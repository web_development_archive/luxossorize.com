<div class="box categories">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content brand-categories">
        <?php foreach ($categories as $category) { ?>
	        <span>
	          <?php if ($category['manufacturer_category_id'] == $category_id) { ?>
	          <a href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>
	          <?php } else { ?>
	          <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
	          <?php } ?>
	        </span>
	        <?php if ($category['children']) { ?>
	        <ul>
	          <?php foreach ($category['children'] as $child) { ?>
	          <li>
	            <?php if ($child['manufacturer_category_id'] == $child_id) { ?>
	            <a href="<?php echo $child['href']; ?>" class="active"><?php echo $child['name']; ?></a>
	            <?php } else { ?>
	            <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
	            <?php } ?>
	          </li>
	          <?php } ?>
	        </ul>
	        <?php } ?>
        <?php } ?>
      </ul>
  </div>
</div>
