<?php
class ControllerPaymentOkpay extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['button_back'] = $this->language->get('button_back');
		$data['action'] = 'https://checkout.okpay.com';
		
		$this->load->model('checkout/order');
		
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		$data['ok_receiver']       = $this->config->get('okpay_receiver');
		$data['ok_invoice']        = $this->session->data['order_id'];
		$data['ok_currency']       = $order_info['currency_code'];
		$data['ok_item_1_name']    = $this->config->get('config_name') . ' - #' . $this->session->data['order_id'];
		$data['ok_item_1_price']   = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], FALSE);
		$data['rbp']               = "okpay-module";
		$data['ok_ipn']            = HTTPS_SERVER . 'index.php?route=payment/okpay/callback';
		$data['ok_return_success'] = HTTPS_SERVER . 'index.php?route=checkout/success';
		
		if ($this->request->get['route'] != 'checkout/guest_step_3') {
			$data['ok_return_fail'] = HTTPS_SERVER . 'index.php?route=checkout/payment';
		} else {
			$data['ok_return_fail'] = HTTPS_SERVER . 'index.php?route=checkout/guest_step_2';
		}
				
		if ($this->request->get['route'] != 'checkout/guest_step_3') {
			$data['back'] = HTTPS_SERVER . 'index.php?route=checkout/payment';
		} else {
			$data['back'] = HTTPS_SERVER . 'index.php?route=checkout/guest_step_2';
		}
		
		$this->id = 'payment';
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/okpay.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/okpay.tpl', $data);
		} else {
			return $this->load->view('payment/okpay.tpl', $data);
		}
	}
	
	public function callback() {
		if (isset($this->request->post['ok_invoice'])) {
			$order_id = $this->request->post['ok_invoice'];
		} else {
			$order_id = 0;
		}
		
		$this->load->model('checkout/order');
		
		$order_info = $this->model_checkout_order->getOrder($order_id);
		
		if ($order_info) {
			$request = 'ok_verify=true';
			foreach ($this->request->post as $key => $value) {
				$request .= '&' . $key . '=' . urlencode(stripslashes(html_entity_decode($value, ENT_QUOTES, 'UTF-8')));
			}
			
			if (extension_loaded('curl')) {
				$ch = curl_init('https://checkout.okpay.com/ipn-verify');
				
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				
				$response = curl_exec($ch);
				
				if (
					strcmp($response, 'VERIFIED') == 0 &&
					$this->request->post['ok_txn_status'] == 'completed' &&
					$this->request->post['ok_txn_gross'] == $this->currency->format($order_info['total'], $order_info['currency_code'], false, false)
					) {
					$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('okpay_order_status_id'), 'OKPAY');
				} else {
					$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'), 'OKPAY');
					header('HTTP/1.0 404 Not Found');die();
				}
				
				curl_close($ch);
			} else {
				$header  = 'POST /ipn-verify HTTP/1.0' . "\r\n";
				$header  = 'Host: checkout.okpay.com'."\r\n";
				$header .= 'Content-Type: application/x-www-form-urlencoded' . "\r\n";
				$header .= 'Content-Length: ' . strlen(utf8_decode($request)) . "\r\n";
				$header .= 'Connection: close'  ."\r\n\r\n";
				
				$fp = fsockopen('checkout.okpay.com', 443, $errno, $errstr, 30);
				
				if ($fp) {
					fputs($fp, $header . $request);
					
					while (!feof($fp)) {
						$response = fgets($fp, 1024);
						
						if (
							strcmp($response, 'VERIFIED') == 0 &&
							$this->request->post['ok_txn_status'] == 'completed' &&
							$this->request->post['ok_txn_gross'] == $this->currency->format($order_info['total'], $order_info['currency_code'], false, false)
							) {
							$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('okpay_order_status_id'), 'OKPAY');
						} else {
							$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'), 'OKPAY');
							header('HTTP/1.0 404 Not Found');die();
						}
					}
					
					fclose($fp);
				}
			}
		}
		else // !$order_info
		{
			header('HTTP/1.0 404 Not Found');
			echo 'ERROR: Order not found!';
			return 0;
			die();
		}
	}
	
}
?>