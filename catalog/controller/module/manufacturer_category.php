<?php
class ControllerModuleManufacturerCategory extends Controller {
	protected function index($setting) {
		$this->language->load('module/manufacturer_category');

		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$this->data['category_id'] = $parts[0];
			$current_category = $parts[0];
		} else {
			$this->data['category_id'] = 0;
			$current_category = 0;
		}

		if (isset($parts[1])) {
			$this->data['category_id'] = $parts[1];
		} else {
			$this->data['category_id'] = 0;
		}

		if (isset($parts[2])) {
			$this->data['child_id'] = $parts[2];
		} else {
			$this->data['child_id'] = 0;
		}
		$this->data['current_category'] = $current_category;

		$this->load->model('catalog/manufacturer_category');

		$current_category_info = $this->model_catalog_manufacturer_category->getCategory($current_category);
		if(isset($current_category_info['name'])){
			$this->data['heading_title'] = $current_category_info['name'];
		}else{
			$this->data['heading_title'] = $this->language->get('heading_title');
		}

		$this->data['categories'] = array();

		$categories = $this->model_catalog_manufacturer_category->getCategories($current_category);

		foreach ($categories as $category) {
			$children_data = array();
			$children = $this->model_catalog_manufacturer_category->getCategories($category['manufacturer_category_id']);
			foreach ($children as $child) {
				$data = array(
					'filter_category_id'  => $child['manufacturer_category_id'],
					'filter_sub_category' => true
				);


				$children_data[] = array(
					'manufacturer_category_id' => $child['manufacturer_category_id'],
					'name'        => $child['name'],
					'href'        => $this->url->link('product/manufacturer', 'manufacturer_category_id='.(($current_category != 0)? $current_category.'_' : '') . $category['manufacturer_category_id'] . '_' . $child['manufacturer_category_id'])
				);
			}

			$this->data['categories'][] = array(
				'manufacturer_category_id' => $category['manufacturer_category_id'],
				'name'        => $category['name'],
				'children'    => $children_data,
				'href'        => $this->url->link('product/manufacturer', 'manufacturer_category_id='.(($current_category != 0)? $current_category.'_' : '') . $category['manufacturer_category_id'])
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/manufacturer_category.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/manufacturer_category.tpl';
		} else {
			$this->template = 'default/template/module/manufacturer_category.tpl';
		}

		$this->render();
  	}

}
?>