-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Host: db695161153.db.1and1.com
-- Generation Time: Aug 27, 2017 at 04:10 AM
-- Server version: 5.5.57-0+deb7u1-log
-- PHP Version: 5.4.45-0+deb7u9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db695161153`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_address`
--

CREATE TABLE IF NOT EXISTS `oc_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `oc_address`
--

INSERT INTO `oc_address` (`address_id`, `customer_id`, `firstname`, `lastname`, `company`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`, `custom_field`) VALUES
(1, 0, 'Ержан', 'Каратаев', '', 'Zhetysu 3 microdistrict, Auezov district', '', 'Almaty', '050063', 109, 1716, ''),
(2, 0, 'Ержан', 'Каратаев', '', 'Zhetysu 3 microdistrict, Auezov district', '', 'Almaty', '050063', 109, 1716, ''),
(3, 0, 'Ержан', 'Каратаев', '', 'Zhetysu 3 microdistrict, Auezov district', '', 'Almaty', '050063', 109, 1716, ''),
(4, 0, 'Ержан', 'Каратаев', '', 'Zhetysu 3 microdistrict, Auezov district', '', 'Almaty', '050063', 109, 1716, ''),
(5, 0, 'Ержан', 'Каратаев', '', 'Алматы', '', 'алматы', '', 109, 1717, ''),
(6, 0, 'Ержан', 'Каратаев', '', 'Алматы', '', 'алматы', '', 109, 1717, ''),
(7, 0, 'Yerzhan', 'Karatayev', '', 'Zhetysu 3 microdistrict, Auezov district', '', 'Almaty', '050063', 109, 1719, '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_affiliate`
--

CREATE TABLE IF NOT EXISTS `oc_affiliate` (
  `affiliate_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`affiliate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_affiliate_activity`
--

CREATE TABLE IF NOT EXISTS `oc_affiliate_activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_affiliate_login`
--

CREATE TABLE IF NOT EXISTS `oc_affiliate_login` (
  `affiliate_login_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`affiliate_login_id`),
  KEY `email` (`email`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_affiliate_transaction`
--

CREATE TABLE IF NOT EXISTS `oc_affiliate_transaction` (
  `affiliate_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliate_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`affiliate_transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api`
--

CREATE TABLE IF NOT EXISTS `oc_api` (
  `api_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`api_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `oc_api`
--

INSERT INTO `oc_api` (`api_id`, `name`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'R29vKJSoW2JYeI06CyiWpQUQfe1oJYj2wgV8Rhi9Xz1uf9PzKMSFNf2ryXTeQVl9lofFJ7OjzXjfEwPrgpp98lOz97YKFbwce7IqSGnwaDrDZIEIsvoifIFAEvSeRnp07pbRZU2AiffBLTVtF2qL4gLwfioE4euZTIg72OjGd6w4gs5fETssU9FQVGygoVEyUNyDCv4RHGGcQ9lvI9JUnxRTIOTN73aTxugrOzBcjovcck4DsNbPsztmSBqVezCt', 1, '2017-08-18 09:22:34', '2017-08-18 09:22:34');

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_ip`
--

CREATE TABLE IF NOT EXISTS `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  PRIMARY KEY (`api_ip_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `oc_api_ip`
--

INSERT INTO `oc_api_ip` (`api_ip_id`, `api_id`, `ip`) VALUES
(1, 1, '::1'),
(2, 1, '206.123.143.195');

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_session`
--

CREATE TABLE IF NOT EXISTS `oc_api_session` (
  `api_session_id` int(11) NOT NULL AUTO_INCREMENT,
  `api_id` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `session_name` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`api_session_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `oc_api_session`
--

INSERT INTO `oc_api_session` (`api_session_id`, `api_id`, `token`, `session_id`, `session_name`, `ip`, `date_added`, `date_modified`) VALUES
(5, 1, '6jv5eCekFct7vxnpQs1SA4UZpgU8f41D', '78ba3cbb343ab5bcc787c1e1dccabedc', 'temp_session_599eb7d9d765e', '206.123.143.195', '2017-08-24 07:26:17', '2017-08-24 07:26:26'),
(4, 1, 'AngUVrAr5FfNAZ2kqojBSigWTUobpxlj', '78ba3cbb343ab5bcc787c1e1dccabedc', 'temp_session_599eb7c71f643', '206.123.143.195', '2017-08-24 07:25:59', '2017-08-24 07:26:16');

-- --------------------------------------------------------

--
-- Table structure for table `oc_article`
--

CREATE TABLE IF NOT EXISTS `oc_article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `date_available` date NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `article_review` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `noindex` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `gstatus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=126 ;

--
-- Dumping data for table `oc_article`
--

INSERT INTO `oc_article` (`article_id`, `image`, `date_available`, `sort_order`, `article_review`, `status`, `noindex`, `date_added`, `date_modified`, `viewed`, `gstatus`) VALUES
(120, 'catalog/cart.png', '0000-00-00', 1, 1, 1, 1, '2014-04-08 04:26:00', '2015-06-29 09:35:55', 8, 0),
(123, 'catalog/demo/canon_eos_5d_2.jpg', '0000-00-00', 1, 1, 1, 1, '2014-03-31 06:55:15', '2015-06-29 09:03:48', 136, 1),
(124, 'catalog/demo/canon_eos_5d_3.jpg', '0000-00-00', 1, 0, 1, 1, '2015-06-29 09:05:38', '2015-06-29 10:11:50', 2, 0),
(125, 'catalog/demo/canon_eos_5d_2.jpg', '0000-00-00', 1, 0, 1, 1, '2015-06-29 09:09:03', '0000-00-00 00:00:00', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_article_description`
--

CREATE TABLE IF NOT EXISTS `oc_article_description` (
  `article_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `tag` text NOT NULL,
  PRIMARY KEY (`article_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_article_description`
--

INSERT INTO `oc_article_description` (`article_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`, `meta_title`, `meta_h1`, `tag`) VALUES
(120, 1, 'CMS для интернет магазинов OPENCART.PRO v2.x', '&lt;p&gt;Рады представить Вашему вниманию OPENCART.PRO v2.x основанную на OPENCART v2.x&lt;/p&gt;\r\n', 'CMS для интернет магазинов OPENCART.PRO v2.x это бесплатный функциональный движок для создания качественных продающих магазинов.', 'cms, opencart, ocshop', 'CMS для интернет магазинов OPENCART.PRO v2.x - Скачать', 'CMS для интернет магазинов OPENCART.PRO v2.x', ''),
(120, 2, 'CMS for online stores OPENCART.PRO v2.x', '&lt;p&gt;&lt;span class=&quot;long_text&quot; id=&quot;result_box&quot; lang=&quot;en&quot;&gt;&lt;span class=&quot;hps&quot;&gt;Are pleased to announce&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;OPENCART.PRO v2.x&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;based on&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;OpenCart v2.x&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'CMS for online stores OPENCART.PRO v2.x is a free functional engine to create high-quality shops selling.', 'cms, opencart, ocshop', 'CMS for online stores OPENCART.PRO v2.x - Download', 'CMS for online stores OPENCART.PRO v2.x', ''),
(123, 1, 'Обзор Первый', '&lt;p&gt;Это первый фото обзор тут можно написать много какого то текста который описывает фото обзор и говорит что и как и почему для чего :-) Это первый фото обзор тут можно написать много какого то текста который описывает фото обзор и говорит что и как и почему для чего :-) Это первый фото обзор тут можно написать много какого то текста который описывает фото обзор и говорит что и как и почему для чего :-) Это первый фото обзор тут можно написать много какого то текста который описывает фото обзор и говорит что и как и почему для чего :-) Это первый фото обзор тут можно написать много какого то текста который описывает фото обзор и говорит что и как и почему для чего :-) Это первый фото обзор тут можно написать много какого то текста который описывает фото обзор и говорит что и как и почему для чего :-) Это первый фото обзор тут можно написать много какого то текста который описывает фото обзор и говорит что и как и почему для чего :-) Это первый фото обзор тут можно написать много какого то текста который описывает фото обзор и говорит что и как и почему для чего :-) Это первый фото обзор тут можно написать много какого то текста который описывает фото обзор и говорит что и как и почему для чего :-)&lt;/p&gt;\r\n', 'Фото Обзор Первый', 'Фото Обзор Первый', 'Фото Обзор Первый', 'Фото Обзор Первый', ''),
(123, 2, 'First Overview', '&lt;p&gt;&lt;span id=&quot;result_box&quot; lang=&quot;en&quot;&gt;&lt;span class=&quot;hps&quot;&gt;This is the first&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review of the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photos&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;here&lt;/span&gt;&lt;span&gt;, you can write&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;a lot of&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what that&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;text&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;that describes the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photo&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review and&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;says&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what and how&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;and why&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;:-) This is the first&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review of the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photos&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;here&lt;/span&gt;&lt;span&gt;, you can write&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;a lot of&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what that&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;text&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;that describes the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photo&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review and&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;says&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what and how&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;and why&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;:-) This is the first&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review of the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photos&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;here&lt;/span&gt;&lt;span&gt;, you can write&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;a lot of&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what that&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;text&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;that describes the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photo&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review and&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;says&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what and how&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;and why&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;:-) This is the first&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review of the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photos&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;here&lt;/span&gt;&lt;span&gt;, you can write&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;a lot of&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what that&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;text&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;that describes the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photo&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review and&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;says&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what and how&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;and why&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;:-) This is the first&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review of the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photos&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;here&lt;/span&gt;&lt;span&gt;, you can write&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;a lot of&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what that&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;text&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;that describes the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photo&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review and&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;says&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what and how&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;and why&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;:-) This is the first&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review of the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photos&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;here&lt;/span&gt;&lt;span&gt;, you can write&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;a lot of&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what that&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;text&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;that describes the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photo&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review and&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;says&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what and how&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;and why&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;:-) This is the first&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review of the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photos&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;here&lt;/span&gt;&lt;span&gt;, you can write&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;a lot of&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what that&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;text&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;that describes the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photo&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review and&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;says&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what and how&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;and why&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;:-) This is the first&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review of the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photos&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;here&lt;/span&gt;&lt;span&gt;, you can write&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;a lot of&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what that&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;text&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;that describes the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photo&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review and&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;says&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what and how&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;and why&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;:-) This is the first&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review of the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photos&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;here&lt;/span&gt;&lt;span&gt;, you can write&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;a lot of&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what that&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;text&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;that describes the&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;photo&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;review and&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;says&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what and how&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;and why&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;what&lt;/span&gt; &lt;span class=&quot;hps&quot;&gt;:-)&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\r\n', 'First Photo Overview', 'First Photo Overview', 'First Photo Overview', 'First Photo Overview', ''),
(124, 2, 'Важная статья', '&lt;p&gt;Это очень важная статья которую нужно прочитать всем важным людям про важные события важных людей :-)&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(124, 1, 'Важная статья', '&lt;p&gt;Это очень важная статья которую нужно прочитать всем важным людям про важные события важных людей :-)&lt;/p&gt;', '', '', '', '', ''),
(125, 1, 'Первая новость', '&lt;p&gt;Это первая новость всем новостям новость :-)&lt;/p&gt;', '', '', '', '', ''),
(125, 2, 'Первая новость', '&lt;p&gt;Это первая новость всем новостям новость :-)&lt;br&gt;&lt;/p&gt;', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_article_image`
--

CREATE TABLE IF NOT EXISTS `oc_article_image` (
  `article_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`article_image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3981 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_article_related`
--

CREATE TABLE IF NOT EXISTS `oc_article_related` (
  `article_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_article_related`
--

INSERT INTO `oc_article_related` (`article_id`, `related_id`) VALUES
(120, 123),
(120, 124),
(123, 120),
(123, 124),
(124, 120),
(124, 123);

-- --------------------------------------------------------

--
-- Table structure for table `oc_article_related_mn`
--

CREATE TABLE IF NOT EXISTS `oc_article_related_mn` (
  `article_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`manufacturer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_article_related_product`
--

CREATE TABLE IF NOT EXISTS `oc_article_related_product` (
  `article_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_article_related_product`
--

INSERT INTO `oc_article_related_product` (`article_id`, `product_id`) VALUES
(30, 123),
(31, 123),
(43, 123),
(45, 123),
(120, 28),
(120, 30),
(120, 41),
(123, 30),
(123, 31),
(123, 43),
(123, 45),
(124, 28),
(124, 30),
(124, 41),
(124, 47);

-- --------------------------------------------------------

--
-- Table structure for table `oc_article_related_wb`
--

CREATE TABLE IF NOT EXISTS `oc_article_related_wb` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_article_to_blog_category`
--

CREATE TABLE IF NOT EXISTS `oc_article_to_blog_category` (
  `article_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  `main_blog_category` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`article_id`,`blog_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_article_to_blog_category`
--

INSERT INTO `oc_article_to_blog_category` (`article_id`, `blog_category_id`, `main_blog_category`) VALUES
(124, 0, 0),
(123, 70, 1),
(120, 0, 0),
(125, 69, 1),
(120, 69, 0),
(120, 71, 1),
(124, 71, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_article_to_download`
--

CREATE TABLE IF NOT EXISTS `oc_article_to_download` (
  `article_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_article_to_layout`
--

CREATE TABLE IF NOT EXISTS `oc_article_to_layout` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_article_to_layout`
--

INSERT INTO `oc_article_to_layout` (`article_id`, `store_id`, `layout_id`) VALUES
(120, 0, 0),
(123, 0, 0),
(124, 0, 0),
(125, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_article_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_article_to_store` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`article_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_article_to_store`
--

INSERT INTO `oc_article_to_store` (`article_id`, `store_id`) VALUES
(120, 0),
(123, 0),
(124, 0),
(125, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute`
--

CREATE TABLE IF NOT EXISTS `oc_attribute` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `oc_attribute`
--

INSERT INTO `oc_attribute` (`attribute_id`, `attribute_group_id`, `sort_order`) VALUES
(13, 11, 3),
(14, 11, 5),
(12, 11, 1),
(22, 7, 3),
(21, 7, 1),
(20, 11, 17),
(19, 11, 15),
(18, 11, 13),
(17, 11, 11),
(16, 11, 9),
(15, 11, 7),
(23, 7, 5),
(24, 8, 1),
(25, 10, 1),
(26, 10, 3),
(27, 10, 5),
(28, 12, 1),
(29, 12, 3),
(30, 12, 5),
(31, 12, 7),
(32, 12, 9),
(33, 12, 13),
(34, 12, 15);

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_description`
--

CREATE TABLE IF NOT EXISTS `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_description`
--

INSERT INTO `oc_attribute_description` (`attribute_id`, `language_id`, `name`) VALUES
(13, 2, 'Body shape'),
(14, 2, 'Water Resistance'),
(15, 2, 'Height'),
(15, 1, 'Высота'),
(12, 2, 'Material'),
(13, 1, 'Форма корпуса'),
(12, 1, 'Материал'),
(17, 2, 'Thickness'),
(16, 2, 'Width'),
(16, 1, 'Ширина'),
(14, 1, 'Водонепроницаемость'),
(17, 1, 'Толщина'),
(18, 1, 'Вес'),
(18, 2, 'Weight'),
(19, 1, 'Скелетон'),
(19, 2, 'Skeleton'),
(20, 1, 'Цифры'),
(20, 2, 'Figures'),
(21, 1, 'Тип механизма'),
(21, 2, 'Type of mechanism'),
(22, 1, 'Автоподзавод'),
(22, 2, 'Self-winding'),
(23, 1, 'Модель механизма'),
(23, 2, 'Mechanism model'),
(24, 1, 'Стрелки'),
(24, 2, 'Arrows'),
(25, 1, 'Браслет/ремешок'),
(25, 2, 'Bracelet/Strap'),
(26, 1, 'Застежка'),
(26, 2, 'Clasp'),
(27, 1, 'Цвет ремня'),
(27, 2, 'Belt Color'),
(28, 1, 'Артикул'),
(28, 2, 'Vendor code'),
(29, 1, 'Пол'),
(29, 2, 'Sex'),
(30, 1, 'Бренд'),
(30, 2, 'Brand'),
(31, 1, 'Коллекция'),
(31, 2, 'Collection'),
(32, 1, 'Страна'),
(32, 2, 'Country'),
(33, 1, 'Механизм'),
(33, 2, 'Mechanism'),
(34, 1, 'Гарантия'),
(34, 2, 'Guarantee');

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group`
--

CREATE TABLE IF NOT EXISTS `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`attribute_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `oc_attribute_group`
--

INSERT INTO `oc_attribute_group` (`attribute_group_id`, `sort_order`) VALUES
(8, 3),
(10, 7),
(9, 5),
(7, 1),
(11, 9),
(12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group_description`
--

CREATE TABLE IF NOT EXISTS `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`attribute_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_group_description`
--

INSERT INTO `oc_attribute_group_description` (`attribute_group_id`, `language_id`, `name`) VALUES
(8, 1, 'Стрелки'),
(7, 2, 'Mechanism'),
(7, 1, 'Механизм'),
(8, 2, 'Arrows'),
(9, 1, 'Циферблат'),
(9, 2, 'Clock face'),
(10, 1, 'Ремешок'),
(10, 2, 'Strap'),
(11, 1, 'Корпус'),
(11, 2, 'Housing'),
(12, 1, 'Технические характеристики часов'),
(12, 2, 'Watches specifications');

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner`
--

CREATE TABLE IF NOT EXISTS `oc_banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `oc_banner`
--

INSERT INTO `oc_banner` (`banner_id`, `name`, `status`) VALUES
(6, 'Банер слева в категориях', 1),
(7, 'Слайдер на главной странице', 1),
(8, 'Производители', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner_image`
--

CREATE TABLE IF NOT EXISTS `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`banner_image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=108 ;

--
-- Dumping data for table `oc_banner_image`
--

INSERT INTO `oc_banner_image` (`banner_image_id`, `banner_id`, `link`, `image`, `sort_order`) VALUES
(100, 7, '', 'catalog/MainSlider/slide4.jpg', 0),
(101, 7, '', 'catalog/MainSlider/slide6.jpg', 0),
(106, 6, '', 'catalog/categories/cruiser_pen.png', 0),
(107, 6, '', 'catalog/categories/MR2713-Simon-G.-white-and-rose-gold-white-and-pink-diamond-weddng-set.png', 0),
(104, 8, '', 'catalog/Manufacturers/595242.png', 0),
(103, 8, '', 'catalog/Manufacturers/1000px-Garmin_logo.svg.png', 0),
(99, 7, '', 'catalog/MainSlider/slide1.jpg', 0),
(102, 8, '', 'catalog/Manufacturers/2000px-Tissot_Logo.svg.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner_image_description`
--

CREATE TABLE IF NOT EXISTS `oc_banner_image_description` (
  `banner_image_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  PRIMARY KEY (`banner_image_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_banner_image_description`
--

INSERT INTO `oc_banner_image_description` (`banner_image_id`, `language_id`, `banner_id`, `title`) VALUES
(106, 2, 6, 'Sale'),
(106, 1, 6, 'Скидки'),
(104, 2, 8, 'CLAUDE BERNARD'),
(99, 1, 7, 'ШВЕЙЦАРСКИЕ ЧАСЫ TISSOT'),
(104, 1, 8, 'CLAUDE BERNARD'),
(103, 2, 8, 'GARMIN'),
(103, 1, 8, 'GARMIN'),
(102, 2, 8, 'TISSOT'),
(102, 1, 8, 'TISSOT'),
(99, 2, 7, 'TISSOT'),
(100, 1, 7, 'ЖЕНСКИЕ ЧАСЫ CLAUDE BERNARD'),
(100, 2, 7, 'CLAUDE BERNARD'),
(101, 1, 7, 'Наручные часы Casio Edifice EQB-600D-1A2'),
(101, 2, 7, 'Casio Edifice EQB-600D-1A2'),
(107, 1, 6, 'Скидки'),
(107, 2, 6, 'Sale');

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog_category`
--

CREATE TABLE IF NOT EXISTS `oc_blog_category` (
  `blog_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`blog_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `oc_blog_category`
--

INSERT INTO `oc_blog_category` (`blog_category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `noindex`, `date_added`, `date_modified`) VALUES
(69, 'catalog/demo/canon_eos_5d_2.jpg', 0, 1, 0, 0, 1, 1, '2014-04-08 03:56:26', '2015-06-18 09:15:42'),
(70, 'catalog/demo/iphone_2.jpg', 0, 1, 0, 0, 1, 1, '2014-04-08 03:58:55', '2015-06-18 09:16:41'),
(71, 'catalog/demo/canon_eos_5d_1.jpg', 69, 1, 1, 0, 1, 1, '2015-06-18 09:13:57', '2015-06-18 09:15:58');

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog_category_description`
--

CREATE TABLE IF NOT EXISTS `oc_blog_category_description` (
  `blog_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  PRIMARY KEY (`blog_category_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_blog_category_description`
--

INSERT INTO `oc_blog_category_description` (`blog_category_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`, `meta_title`, `meta_h1`) VALUES
(69, 2, 'News', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(70, 1, 'Обзоры', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(69, 1, 'Новости', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(71, 2, 'Анонсы', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(71, 1, 'Анонсы', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(70, 2, 'Reviews', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog_category_path`
--

CREATE TABLE IF NOT EXISTS `oc_blog_category_path` (
  `blog_category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`blog_category_id`,`path_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_blog_category_path`
--

INSERT INTO `oc_blog_category_path` (`blog_category_id`, `path_id`, `level`) VALUES
(69, 69, 0),
(71, 71, 1),
(71, 69, 0),
(70, 70, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog_category_to_layout`
--

CREATE TABLE IF NOT EXISTS `oc_blog_category_to_layout` (
  `blog_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`blog_category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_blog_category_to_layout`
--

INSERT INTO `oc_blog_category_to_layout` (`blog_category_id`, `store_id`, `layout_id`) VALUES
(69, 0, 0),
(71, 0, 0),
(70, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_blog_category_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_blog_category_to_store` (
  `blog_category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`blog_category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_blog_category_to_store`
--

INSERT INTO `oc_blog_category_to_store` (`blog_category_id`, `store_id`) VALUES
(69, 0),
(70, 0),
(71, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_cart`
--

CREATE TABLE IF NOT EXISTS `oc_cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `cart_id` (`customer_id`,`session_id`,`product_id`,`recurring_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category`
--

CREATE TABLE IF NOT EXISTS `oc_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`category_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `noindex`, `date_added`, `date_modified`) VALUES
(20, 'catalog/categories/wpid-iwc-portuguese-iw502306_12.png', 0, 1, 1, 1, 1, 1, '2009-01-05 21:49:43', '2017-08-18 17:40:01'),
(24, 'catalog/categories/cruiser_pen.png', 0, 1, 1, 5, 1, 1, '2009-01-20 02:36:26', '2017-08-18 15:02:24'),
(60, '', 0, 1, 1, 5, 1, 1, '2017-08-23 02:13:03', '2017-08-23 02:13:03'),
(61, 'catalog/categories/wpid-iwc-portuguese-iw502306_12.png', 20, 0, 1, 0, 1, 1, '2017-08-23 02:21:34', '2017-08-23 02:21:34'),
(59, 'catalog/categories/RDAW-140522-Izola-Money-Clip-logo-living-well-1-e1472569163411.jpg', 0, 0, 1, 9, 1, 1, '2017-08-22 14:47:15', '2017-08-22 14:48:51'),
(62, 'catalog/categories/head2.png', 20, 0, 1, 0, 1, 1, '2017-08-23 02:23:12', '2017-08-23 02:23:12'),
(63, '', 60, 0, 1, 0, 1, 1, '2017-08-23 02:44:47', '2017-08-23 02:44:47'),
(64, '', 60, 0, 1, 0, 1, 1, '2017-08-23 02:45:03', '2017-08-23 02:45:03'),
(65, '', 60, 0, 1, 0, 1, 1, '2017-08-23 02:45:20', '2017-08-23 02:45:20');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_description`
--

CREATE TABLE IF NOT EXISTS `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(59, 2, 'Money clip', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(24, 2, 'Pens', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'Pens', '', '', ''),
(62, 1, 'Женские часы', '&lt;p&gt;Самые стильные и изящные мужские часы&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(24, 1, 'Ручки', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'Ручки', '', '', ''),
(59, 1, 'Зажим для денег', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(60, 1, 'Аксессуары', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(20, 1, 'Часы', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'Часы', 'Часы Мужские - по доступным ценам в интернет магазине Luxossorize. Купить оригинальные часы Мужские в Казахстане. Принимаем заказы по тел. +77053331349', 'Часы Мужские, казахстан, купить, цена, интернет магазин', ''),
(60, 2, 'Accessories', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(61, 1, 'Мужские часы', '&lt;p&gt;Самые стильные и изящные мужские часы&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(61, 2, 'Men''s Watch', '&lt;p&gt;The most stylish and graceful men''s watch&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(20, 2, 'Watches', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'Watches', '', '', ''),
(62, 2, 'Ladies Watch', '&lt;p&gt;The most stylish and graceful men''s watch&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(63, 1, 'Портмоне', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(63, 2, 'Purse', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(64, 1, 'Сумки', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(64, 2, 'Bags', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(65, 1, 'Ремни', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(65, 2, 'Belts', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_filter`
--

CREATE TABLE IF NOT EXISTS `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_filter`
--

INSERT INTO `oc_category_filter` (`category_id`, `filter_id`) VALUES
(20, 1),
(20, 2),
(20, 3),
(20, 4);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_path`
--

CREATE TABLE IF NOT EXISTS `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`path_id`),
  KEY `path` (`path_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(20, 20, 0),
(24, 24, 0),
(59, 59, 0),
(61, 20, 0),
(60, 60, 0),
(61, 61, 1),
(62, 20, 0),
(62, 62, 1),
(63, 60, 0),
(63, 63, 1),
(64, 60, 0),
(64, 64, 1),
(65, 60, 0),
(65, 65, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_layout`
--

CREATE TABLE IF NOT EXISTS `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_to_layout`
--

INSERT INTO `oc_category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES
(20, 0, 0),
(59, 0, 0),
(24, 0, 0),
(61, 0, 0),
(60, 0, 0),
(62, 0, 0),
(63, 0, 0),
(64, 0, 0),
(65, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(20, 0),
(24, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_country`
--

CREATE TABLE IF NOT EXISTS `oc_country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=258 ;

--
-- Dumping data for table `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Армения', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Азербайджан', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Белоруссия (Беларусь)', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D''Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Грузия', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Казахстан', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Киргизия (Кыргызстан)', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People''s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Молдова', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Российская Федерация', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Таджикистан', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Туркменистан', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Украина', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Узбекистан', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon`
--

CREATE TABLE IF NOT EXISTS `oc_coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `code` varchar(10) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`coupon_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, 'Скидка -10%', '2222', 'P', '10.0000', 0, 0, '0.0000', '2014-01-01', '2020-01-01', 10, '10', 0, '2009-01-27 13:55:03'),
(5, 'Бесплатная доставка', '3333', 'P', '0.0000', 0, 1, '100.0000', '2014-01-01', '2014-02-01', 10, '10', 0, '2009-03-14 21:13:53'),
(6, 'Скидка - 10.00', '1111', 'F', '10.0000', 0, 0, '10.0000', '2014-01-01', '2020-01-01', 100000, '10000', 0, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_category`
--

CREATE TABLE IF NOT EXISTS `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`coupon_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_history`
--

CREATE TABLE IF NOT EXISTS `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`coupon_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_product`
--

CREATE TABLE IF NOT EXISTS `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`coupon_product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_currency`
--

CREATE TABLE IF NOT EXISTS `oc_currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(2, 'US Dollar', 'USD', '$ ', '', '2', 1.00000000, 1, '2017-08-25 14:48:54');

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field`
--

CREATE TABLE IF NOT EXISTS `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `location` varchar(7) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`custom_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_customer_group`
--

CREATE TABLE IF NOT EXISTS `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL,
  PRIMARY KEY (`custom_field_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_description`
--

CREATE TABLE IF NOT EXISTS `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`custom_field_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value`
--

CREATE TABLE IF NOT EXISTS `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`custom_field_value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value_description`
--

CREATE TABLE IF NOT EXISTS `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`custom_field_value_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer`
--

CREATE TABLE IF NOT EXISTS `oc_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_activity`
--

CREATE TABLE IF NOT EXISTS `oc_customer_activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `oc_customer_activity`
--

INSERT INTO `oc_customer_activity` (`activity_id`, `customer_id`, `key`, `data`, `ip`, `date_added`) VALUES
(1, 0, 'order_guest', '{"name":" ","order_id":1}', '5.76.247.22', '2017-08-21 17:14:37'),
(2, 0, 'order_guest', '{"name":" ","order_id":2}', '5.76.247.22', '2017-08-21 17:15:38'),
(3, 0, 'order_guest', '{"name":"Yerzhan ","order_id":5}', '206.123.143.195', '2017-08-23 07:13:01'),
(4, 0, 'order_guest', '{"name":"Yerzhan ","order_id":14}', '206.123.143.195', '2017-08-23 16:12:57'),
(5, 0, 'order_guest', '{"name":"Yerzhan Karatayev","order_id":15}', '5.76.247.22', '2017-08-23 16:29:07'),
(6, 0, 'order_guest', '{"name":"\\u0415\\u0432\\u0433\\u0435\\u043d\\u0438\\u0439 ","order_id":22}', '2.73.6.198', '2017-08-25 09:17:11');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group`
--

CREATE TABLE IF NOT EXISTS `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`customer_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group_description`
--

CREATE TABLE IF NOT EXISTS `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`customer_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Default', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_history`
--

CREATE TABLE IF NOT EXISTS `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_ip`
--

CREATE TABLE IF NOT EXISTS `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_ip_id`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_login`
--

CREATE TABLE IF NOT EXISTS `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`customer_login_id`),
  KEY `email` (`email`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_online`
--

CREATE TABLE IF NOT EXISTS `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_reward`
--

CREATE TABLE IF NOT EXISTS `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_reward_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_transaction`
--

CREATE TABLE IF NOT EXISTS `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_wishlist`
--

CREATE TABLE IF NOT EXISTS `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`customer_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download`
--

CREATE TABLE IF NOT EXISTS `oc_download` (
  `download_id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(128) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download_description`
--

CREATE TABLE IF NOT EXISTS `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`download_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_event`
--

CREATE TABLE IF NOT EXISTS `oc_event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension`
--

CREATE TABLE IF NOT EXISTS `oc_extension` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL,
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(29, 'module', 'manufacturerpercategory'),
(5, 'total', 'total'),
(27, 'captcha', 'google_captcha'),
(9, 'shipping', 'flat'),
(13, 'module', 'category'),
(28, 'module', 'filter'),
(26, 'payment', 'bank_transfer'),
(34, 'module', 'tg_kspro_revolution_slider'),
(20, 'module', 'product_tab'),
(22, 'module', 'blog_featured'),
(33, 'module', 'tg_kspro_settings'),
(24, 'module', 'imgcategory'),
(25, 'module', 'manufacturer'),
(30, 'module', 'manufacturer_category'),
(37, 'module', 'tg_kspro_filter_product'),
(38, 'module', 'information'),
(39, 'module', 'latest'),
(40, 'module', 'category'),
(41, 'module', 'html'),
(50, 'module', 'buy'),
(51, 'shipping', 'pickup'),
(52, 'shipping', 'fedex'),
(53, 'module', 'pim'),
(59, 'payment', 'shoputils_ik');

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter`
--

CREATE TABLE IF NOT EXISTS `oc_filter` (
  `filter_id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`filter_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `oc_filter`
--

INSERT INTO `oc_filter` (`filter_id`, `filter_group_id`, `sort_order`) VALUES
(5, 2, 0),
(3, 1, 2),
(2, 1, 1),
(1, 1, 0),
(6, 2, 0),
(7, 2, 0),
(8, 3, 0),
(9, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_description`
--

CREATE TABLE IF NOT EXISTS `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`filter_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_description`
--

INSERT INTO `oc_filter_description` (`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES
(3, 1, 1, 'Унисекс'),
(2, 2, 1, 'Women''s'),
(2, 1, 1, 'Женские'),
(1, 2, 1, 'Men''s'),
(1, 1, 1, 'Мужские'),
(3, 2, 1, 'Unisex'),
(5, 1, 2, 'Бельгия'),
(5, 2, 2, 'Belgium'),
(6, 1, 2, 'Австрия'),
(6, 2, 2, 'Austria'),
(7, 1, 2, 'Швейцария'),
(7, 2, 2, 'Switzerland'),
(8, 1, 3, 'Механический'),
(8, 2, 3, 'Mechanical'),
(9, 1, 3, 'Кварцевый'),
(9, 2, 3, 'Quartz');

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group`
--

CREATE TABLE IF NOT EXISTS `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`filter_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `oc_filter_group`
--

INSERT INTO `oc_filter_group` (`filter_group_id`, `sort_order`) VALUES
(1, 0),
(2, 0),
(3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group_description`
--

CREATE TABLE IF NOT EXISTS `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`filter_group_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group_description`
--

INSERT INTO `oc_filter_group_description` (`filter_group_id`, `language_id`, `name`) VALUES
(1, 2, 'Sex'),
(1, 1, 'Пол'),
(2, 1, 'Производитель'),
(2, 2, 'Manufacturer'),
(3, 1, 'Тип механизма'),
(3, 2, 'Type of mechanism');

-- --------------------------------------------------------

--
-- Table structure for table `oc_geo_zone`
--

CREATE TABLE IF NOT EXISTS `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`geo_zone_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_modified`, `date_added`) VALUES
(3, 'По Казахстану', 'Весь Казахстан PONY EXPRESS', '2017-08-23 07:10:27', '2009-01-06 23:26:25'),
(4, 'Доставка по Алматы', 'Доставка по Алматы на покупку менее 30 000 тг', '2017-08-23 07:08:07', '2009-06-23 01:14:53');

-- --------------------------------------------------------

--
-- Table structure for table `oc_information`
--

CREATE TABLE IF NOT EXISTS `oc_information` (
  `information_id` int(11) NOT NULL AUTO_INCREMENT,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `noindex` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`information_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`, `noindex`) VALUES
(3, 1, 3, 1, 1),
(4, 1, 1, 1, 1),
(5, 1, 4, 1, 1),
(6, 1, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_description`
--

CREATE TABLE IF NOT EXISTS `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  PRIMARY KEY (`information_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(4, 1, 'О нас', '&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;&amp;nbsp;Приветствуют Вас на сайте нашего магазина наручных часов и люксовых копий акссесуаров. &amp;nbsp;Изначально, наша компания была самой первой из поставщиков, кто занимался оптовыми продажами часов высокого качества. И в связи с этим фактором, мы открыли уже свой магазин в где любой желающий, мог по достоинству оценить.&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;Огромное количество производителей ежегодно, выпускают новые модели своих часов, и ставят на них баснословные цены, включая туда стоимость самого бренда и рекламы. Мы продаём точные копии этих же самых часов по ценам доступных каждому человеку. Наши дубликаты являются самыми достоверными, как визуально, так и технически. У нас огромный ассортимент, который всё время обновляется, ибо мы периодически отслеживаем эти новые модели на мировом рынке часов, чтобы Вы получили не только качественный товар, но и новинку своевременно.&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;У нас всё время проходят разнообразные акции, а также предпраздничные снижения цен. К каждому своему клиенту, мы подходим строго индивидуально, с особой отдачей и вниманием. В наших интересах, чтобы каждый наш покупатель был доволен своей покупкой, и она служила ему очень долгое время. Также, мы гарантируем возврат денежных средств в течение суток, если товар по тем или иным причинам пришлось вернуть или обменять. Только в Luxossorize доставка часов осуществляется в любую точку Мира &amp;nbsp;в самые кротчайшие сроки!&amp;nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;Лучше хорошая копия,чем плохой оригинал!&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;С уважением Ваш Luxossorize!&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: 1; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;ИП &quot;Led Vision&quot;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: 1; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;г. Алматы, Валиханова 83/9&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: 1; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;Казахстан&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: 1; margin-bottom: 0px; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;ИНН 600604401787&lt;/span&gt;&lt;/p&gt;', 'О нас', '', '', 'О нас'),
(4, 2, 'About Us', '&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Welcome to the website of our store of wrist watches and luxury copies of accessories. Initially, our company was the very first supplier, who was engaged in wholesale sales of high quality watches. And in connection with this factor, we have already opened our store in where anyone can, could appreciate.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;A huge number of manufacturers annually, release new models of their watches, and put fabulous prices on them, including the cost of the brand itself and advertising. We sell replicas of the same watches at prices available to each person. Our duplicates are the most reliable, both visually and technically. We have a huge assortment that is constantly updated, because we periodically track these new models in the world clock market so that you have not only a quality product, but also a novelty in a timely manner.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;At us all time pass various actions, and also pre-holiday decrease in the prices. To each of our clients, we come strictly individually, with special feedback and attention. It is in our interests that each of our customers is satisfied with their purchase, and it has served him for a very long time. Also, we guarantee a refund of money within 24 hours, if the goods for one reason or another had to be returned or exchanged. Only in Luxossorize the delivery of the clock is carried out anywhere in the World in the shortest possible time!&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;blockquote&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Better a good copy than a bad original!&lt;/span&gt;&lt;/blockquote&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Sincerely yours Luxossorize&lt;/span&gt;&lt;/p&gt;&lt;div class=&quot;tw-ta-container tw-nfl&quot; id=&quot;tw-target-text-container&quot; style=&quot;position: relative; padding-top: 20px;&quot;&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;margin-bottom: 0px; outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: 1; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;&lt;br class=&quot;Apple-interchange-newline&quot;&gt;ИП &quot;Led Vision&quot;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;margin-bottom: 0px; outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: 1; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;Almaty, Валиханова 83/9&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;margin-bottom: 0px; outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: 1; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;Kazakhstan&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;first_paragraph&quot; style=&quot;margin-bottom: 0px; outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: 1; padding: 0px 0px 15px; vertical-align: baseline; text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(45, 58, 79); font-family: Verdana; font-size: 16px;&quot;&gt;600604401787&lt;/span&gt;&lt;/p&gt;&lt;div style=&quot;&quot;&gt;&lt;span lang=&quot;en&quot; style=&quot;font-family: Verdana;&quot;&gt;&lt;p&gt;&lt;/p&gt;&lt;/span&gt;&lt;/div&gt;&lt;pre class=&quot;tw-data-text tw-ta tw-text-small&quot; lang=&quot;en&quot; style=&quot;color: rgb(33, 33, 33) !important; font-family: inherit; font-size: 16px !important; background-color: transparent; border-width: initial; border-style: none; border-color: initial; padding: 0px 0.14em 0px 0px; position: absolute; margin-bottom: 0px; resize: none; overflow: hidden; text-align: initial; width: 276px; white-space: pre-wrap; visibility: hidden; line-height: 24px !important;&quot;&gt;&lt;/pre&gt;&lt;/div&gt;', 'About Us', '', '', 'About Us'),
(5, 2, 'Terms &amp; Conditions', '&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Cash payment - payment by cash to the courier after receiving the order. .&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;br&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Bank card - Payment by bank card is possible through the Internet system of electronic payments. Visa and MasterCard are not accepted without CVV2 / CVC2. When a paid online order is delivered to the home, the delivery officer may ask for an identity document. .&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Payment cards of international payment systems VISA and MasterCard are accepted. A convenient and secure way to pay&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;To pay for an order with a bank card, you need:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;ol&gt;&lt;li&gt;Choose a product;&lt;br&gt;&lt;/li&gt;&lt;li&gt;Your full name, e-mail and contact phone number&lt;br&gt;&lt;/li&gt;&lt;li&gt;Click &quot;Checkout&quot;&lt;br&gt;&lt;/li&gt;&lt;/ol&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;You need to fill in your credit card details:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;ol&gt;&lt;li&gt;Your name as indicated on the map.&lt;br&gt;&lt;/li&gt;&lt;li&gt;The card number is on the front side of the card, 16-19 digits without spaces.&lt;br&gt;&lt;/li&gt;&lt;li&gt;The month and year of expiry of the card, they are also on the front side of the card.&lt;br&gt;&lt;/li&gt;&lt;li&gt;The code CVC2 / CVV2, are on the reverse side of the card on the signature strip, you must enter the last 3 or 4 digits of the number.&lt;br&gt;&lt;/li&gt;&lt;/ol&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;margin-bottom: 0px; padding: 0px;&quot;&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;After clicking the &quot;Pay&quot; button, you may need to enter your 3-D Secure password. You can get this password from your bank. To enter this code, you will be directed to the page of your bank.&lt;/span&gt;&lt;/p&gt;', 'Terms &amp; Conditions', '', '', ''),
(3, 2, 'Privacy Policy', '&lt;div&gt;Website - http://luxossorize.com/&lt;/div&gt;&lt;div&gt;Internet store - Internet store located on the Internet at: http://www.Luxossorize.com, where the Goods offered by the Seller for the purchase of Buyers are presented, as well as the terms of payment and delivery of the Goods.&lt;/div&gt;&lt;div&gt;The Buyer is the User who placed the Order in the Luxossorize.com online store in order to purchase the Goods.&lt;/div&gt;&lt;div&gt;The seller - IS LED VISION, Republic of Kazakhstan, Almaty, ul.Valikhana 83&lt;/div&gt;&lt;div&gt;User - any individual who visited the Site, registered and accepted the terms of this agreement.&lt;/div&gt;&lt;div&gt;Site visitor - any individual who visited the site http://www.Luxossorize.com&lt;/div&gt;&lt;div&gt;Goods - the object of the agreement of the parties, the list, the name of the assortment, placed in the online store.&lt;/div&gt;&lt;div&gt;Order - a request duly executed by the Buyer for purchase and delivery to the specified Buyer at the address of the Goods selected on the Site.&lt;/div&gt;&lt;div&gt;&lt;div&gt;Selected on the Site.&lt;/div&gt;&lt;div&gt;1. General Provisions&lt;/div&gt;&lt;div&gt;1.1. The seller carries out the sale and promotion of the goods through the online store.&lt;/div&gt;&lt;div&gt;1.2. When ordering Goods through the online store, the User agrees with the terms of sale of the Goods set out below (hereinafter referred to as the Conditions for the Sale of Goods). In case of disagreement with this User Agreement (hereinafter referred to as the Agreement), the User has the right to stop using the service and leave the site http://www.Luxossorize.com&lt;/div&gt;&lt;div&gt;1.3. These Terms of Sale of goods, as well as information on the Goods submitted on the Site, are a public offer in accordance with Cl. 395 of the Civil Code of the Republic of Kazakhstan.&lt;/div&gt;&lt;div&gt;1.4. The Agreement can be changed by the Seller unilaterally without notifying the Buyer / User. The new version of the Agreement comes into force after 10 (Ten) calendar days from the date of its publication on the Site, unless otherwise provided by the terms of this Agreement.&lt;/div&gt;&lt;div&gt;1.5. The Agreement comes into force from the date of sending to the Buyer by the Seller an electronic confirmation of acceptance of the Order (upon execution by the Buyer of the Order without authorization on the Site) or from the moment of registration of the User on the Site.&lt;/div&gt;&lt;div&gt;Informing the Seller of his e-mail and telephone number, Name and Surname, Site Visitor / User / Buyer agrees to use the specified communication facilities by the Seller, as well as by third parties engaged by the Seller for the purpose of fulfilling obligations under this Agreement. In order to carry out promotional and informational newsletters containing information on discounts, upcoming and current promotions and other events of the Seller, on the transfer of the order for delivery, as well as other information directly related to the fulfillment of obligations by the Buyer under this Agreement.&lt;/div&gt;&lt;div&gt;2. Subject of the agreement&lt;/div&gt;&lt;div&gt;2.1. The subject of this Agreement is the provision of the opportunity for the User to purchase for personal, family, home and other use not related to the performance of business activities, Goods presented in the catalog of the Internet store on the site: http://www.Luxossorize.com&lt;/div&gt;&lt;div&gt;2.2. This Agreement applies to all types of Goods and services presented on the Site, as long as such offers are described in the online store catalog.&lt;/div&gt;&lt;div&gt;3. Registration on the Site&lt;/div&gt;&lt;div&gt;3.1. Registration on the Site is carried out via the link for registration &quot;Registration&quot;.&lt;/div&gt;&lt;div&gt;3.2. Registration on the Site is not mandatory for ordering.&lt;/div&gt;&lt;div&gt;3.3. The Seller is not responsible for the accuracy and correctness of the information provided by the Visitor at registration.&lt;/div&gt;&lt;div&gt;3.4. The User undertakes not to disclose to third parties the login and password specified by him at registration. In the event that the User has suspicions regarding the security of his login and password or the possibility of their unauthorized use by third parties, the User agrees to immediately notify the Seller about it by sending an appropriate email to:, and also to change the password specified earlier.&lt;/div&gt;&lt;div&gt;3.5. Communication of the User / Buyer with managers and other representatives of the Seller should be based on the principles of generally accepted morality and communication etiquette. It is strictly forbidden to use obscene words, abuse, offensive language, and threats and blackmail, no matter in what form and to whom they were addressed.&lt;/div&gt;&lt;div&gt;4. The goods and the order of fulfillment of purchase&lt;/div&gt;&lt;div&gt;4.1. The seller ensures the availability in its warehouse of the Goods presented on the Site. Accompanying the Goods the photos placed on the Site are simple illustrations to it and may differ from the actual appearance of the Goods. Accompanying Product descriptions / characteristics do not pretend to be exhaustive information content and may contain typos. To clarify the information on the Goods, the Buyer can contact the Customer Support Service at the contacts indicated on the Site. The information on the Site is updated every 24 hours.&lt;/div&gt;&lt;div&gt;4.2. In the absence of goods ordered by the Buyer in the warehouse of the Seller, the latter has the right to exclude the specified Goods from the Order / cancel the Buyer''s Order by notifying the Buyer thereof by sending the relevant electronic message to the address specified by the Buyer upon registration or by telephone.&lt;/div&gt;&lt;div&gt;4.3. In the event of the cancellation of a fully or partially prepaid Order, the cost of the canceled Goods is returned by the Seller to the Buyer in accordance with the terms of this Agreement.&lt;/div&gt;&lt;div&gt;4.4. The Seller has the right to cancel the Buyer''s Orders, including Goods, from which the Buyer previously refused 5 or more times, specifying the reasons not related to the presence of defects in these Goods.&lt;/div&gt;&lt;div&gt;4.5. The Seller has the right to refuse the execution of the Order if the Buyer already has issued and unpaid orders (3 orders).&lt;/div&gt;&lt;div&gt;4.6. The Buyer''s order is made in accordance with the procedures indicated on the Site in the section &quot;Ordering&quot; at: http://www.luxossorize.com&lt;/div&gt;&lt;div&gt;4.7. The buyer bears full responsibility for providing&lt;/div&gt;&lt;div&gt;Переводчик Google для бизнеса –Инструменты переводчикаПереводчик сайтов&lt;/div&gt;&lt;/div&gt;', 'Privacy Policy', '', '', '');
INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(3, 1, 'Публичная оферта', '&lt;p style=&quot;padding: 0px; margin-bottom: 0px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;b&gt;Сайт&lt;/b&gt;&amp;nbsp;—&amp;nbsp;&lt;a href=&quot;http://luxossorize.com/&quot; target=&quot;_blank&quot;&gt;http://luxossorize.com/&lt;/a&gt;&lt;br&gt;&lt;b&gt;Интернет-магазин&lt;/b&gt;&amp;nbsp;— Интернет-магазин, расположенный в сети Интернет по адресу:&amp;nbsp;&lt;a href=&quot;http://www.fashionpark.kz/&quot; style=&quot;background-color: rgb(255, 255, 255); border: none; outline: none; color: rgb(87, 110, 117);&quot;&gt;http://www.&lt;/a&gt;Luxossorize.com, где представлены Товары, предлагаемые Продавцом для приобретения Покупателями, а также условия оплаты и доставки Товаров.&lt;br&gt;&lt;b&gt;Покупатель&lt;/b&gt;&amp;nbsp;— Пользователь, разместивший Заказ в Интернет-магазине Luxossorize.com с целью приобретения определенного Товара.&lt;br&gt;&lt;b&gt;Продавец&lt;/b&gt;&amp;nbsp;— ИП LED VISION, Республика Казахстан г.Алматы ул.Валихана 83&lt;br&gt;&lt;b&gt;Пользователь&lt;/b&gt;&amp;nbsp;— любое физическое лицо, посетившее Сайт, зарегистрировавшийся и принявший условия настоящего Соглашения.&lt;br&gt;&lt;b&gt;Посетитель Сайта&lt;/b&gt;&amp;nbsp;— любое физическое лицо, посетившее сайт&amp;nbsp;&lt;a href=&quot;http://www.fashionpark.kz/&quot; style=&quot;background-color: rgb(255, 255, 255); border: none; outline: none; color: rgb(87, 110, 117);&quot;&gt;http://www.&lt;/a&gt;Luxossorize.com&lt;br&gt;&lt;b&gt;Товар&lt;/b&gt;&amp;nbsp;—&amp;nbsp;&lt;span style=&quot;background-image: initial;&quot;&gt;объект соглашения сторон, перечень, наименование ассортимента, размещенный в интернет-магазине.&lt;/span&gt;&lt;br&gt;&lt;b&gt;Заказ&lt;/b&gt;&amp;nbsp;— запрос должным образом оформленный Покупателем на приобретение и доставку по указанному Покупателем адресу Товаров, выбранных на Сайте.&lt;br&gt;1. Общие положения&lt;br&gt;1.1. Продавец осуществляет продажу и продвижение Товаров через Интернет-магазин.&lt;br&gt;1.2. Заказывая Товары через Интернет-магазин, Пользователь соглашается с условиями продажи Товаров, изложенными ниже (далее – Условия продажи товаров). В случае несогласия с настоящим Пользовательским соглашением (далее - Соглашение) Пользователь имеет право прекратить использование сервиса и покинуть сайт&amp;nbsp;&lt;a href=&quot;http://www.fashionpark.kz/&quot; style=&quot;background-color: rgb(255, 255, 255); border: none; outline: none; color: rgb(87, 110, 117);&quot;&gt;http://www.&lt;/a&gt;Luxossorize.com&lt;br&gt;1.3. Настоящие Условия продажи товаров, а также информация о Товаре, представленная на Сайте, являются публичной офертой в соответствии с п. 5 ст. 395 Гражданского кодекса Республики Казахстан.&lt;br&gt;1.4. Соглашение может быть изменено Продавцом в одностороннем порядке без уведомления Покупателя/Пользователя. Новая редакция Соглашения вступает в силу по истечении 10 (Десяти) календарных дней с даты ее опубликования на Сайте, если иное не предусмотрено условиями настоящего Соглашения.&lt;br&gt;1.5. Соглашение вступает в силу с даты отправки Покупателю Продавцом электронного подтверждения о принятии Заказа (при оформлении Покупателем Заказа без авторизации на Сайте), или с момента регистрации Пользователя на Сайте.&lt;br&gt;Сообщая Продавцу свой e-mail и номер телефона, Имя и Фамилию, Посетитель Сайта/Пользователь/Покупатель дает согласие на использование указанных средств связи Продавцом, а также третьими лицами, привлекаемыми Продавцом для целей выполнения обязательств по настоящему Соглашению. В целях осуществления рассылок рекламного и информационного характера, содержащих информацию о скидках, предстоящих и действующих акциях и иных мероприятиях Продавца, о передаче заказа в доставку, а также иную информацию, непосредственно связанную с выполнением обязательств Покупателем в рамках настоящего Соглашения.&lt;br&gt;2. Предмет соглашения&lt;br&gt;2.1. Предметом настоящего Соглашения является предоставление возможности Пользователю приобретать для личного, семейного, домашнего и иного пользования, не связанного с осуществлением предпринимательской деятельности, Товары, представленные в каталоге Интернет-магазина на сайте:&amp;nbsp;&lt;a href=&quot;http://www.fashionpark.kz/&quot; style=&quot;background-color: rgb(255, 255, 255); border: none; outline: none; color: rgb(87, 110, 117);&quot;&gt;http://www.&lt;/a&gt;Luxossorize.com&lt;br&gt;2.2. Данное Соглашение распространяется на все виды Товаров и услуг, представленных на Сайте, пока такие предложения с описанием присутствуют в каталоге Интернет-магазина.&lt;br&gt;3. Регистрация на Сайте&lt;br&gt;3.1. Регистрация на Сайте осуществляется с помощью ссылки для регистрации «Регистрация».&lt;br&gt;3.2. Регистрация на Сайте не является обязательной для оформления Заказа.&lt;br&gt;3.3. Продавец не несет ответственности за точность и правильность информации, предоставляемой Посетителем при регистрации.&lt;br&gt;3.4. Пользователь обязуется не сообщать третьим лицам логин и пароль, указанные им при регистрации. В случае возникновения у Пользователя подозрений относительно безопасности его логина и пароля или возможности их несанкционированного использования третьими лицами, Пользователь обязуется незамедлительно уведомить об этом Продавца, направив соответствующее электронное письмо по адресу:, а также самостоятельно сменить указанный ранее пароль.&lt;br&gt;3.5. Общение Пользователя/Покупателя с менеджерами и иными представителями Продавца должно строиться на принципах общепринятой морали и коммуникационного этикета. Строго запрещено использование нецензурных слов, брани, оскорбительных выражений, а также угроз и шантажа, в независимости от того, в каком виде и кому они были адресованы.&lt;br&gt;4. Товар и порядок совершения покупки&lt;br&gt;4.1. Продавец обеспечивает наличие на своем складе Товаров, представленных на Сайте. Сопровождающие Товар фотографии, размещенные на Сайте, являются простыми иллюстрациями к нему и могут отличаться от фактического внешнего вида Товара.&amp;nbsp;&lt;span style=&quot;color: red;&quot;&gt;Сопровождающие Товар описания/характеристики не претендуют на исчерпывающую информативность и могут содержать опечатки. Для уточнения информации по Товару Покупатель может обратиться в Службу поддержки клиентов по указанным на Сайте контактам&lt;/span&gt;.&lt;span style=&quot;color: red;&quot;&gt;&amp;nbsp;Обновление информации, представленной на Сайте, производится каждые 24 часа.&lt;/span&gt;&lt;br&gt;4.2. В случае отсутствия заказанных Покупателем Товаров на складе Продавца, последний вправе исключить указанный Товар из Заказа / аннулировать Заказ Покупателя, уведомив об этом Покупателя путем направления соответствующего электронного сообщения по адресу, указанному Покупателем при регистрации, либо по телефонной связи.&lt;br&gt;4.3. В случае аннуляции полностью либо частично предоплаченного Заказа, стоимость аннулированного Товара возвращается Продавцом Покупателю в соответствии с условиями настоящего Соглашения.&lt;br&gt;4.4. Продавец вправе аннулировать Заказы Покупателя, включающие в себя Товары, от которых ранее Покупатель отказался 5 и более раз, указав причины, не связанные с наличием недостатков в этих Товарах.&lt;br&gt;4.5. Продавец вправе отказать в оформлении Заказа, если у Покупателя уже есть оформленные и не оплаченные заказы (3 заказа).&lt;br&gt;4.6. Заказ Покупателя оформляется в соответствии с процедурами, указанными на Сайте в разделе «Оформление Заказа» по адресу:&amp;nbsp;&lt;a href=&quot;http://www.fashionpark.kz/&quot; style=&quot;background-color: rgb(255, 255, 255); border: none; outline: none; color: rgb(87, 110, 117);&quot;&gt;http://www.&lt;/a&gt;luxossorize.com&lt;br&gt;4.7. Покупатель несет полную ответственность за предоставление неверных сведений, повлекшее за собой невозможность надлежащего исполнения Продавцом своих обязательств перед Покупателем.&lt;br&gt;4.8. После оформления Заказа на Сайте, Покупателю предоставляется информация о предполагаемой дате доставки Товара, путем направления Продавцом электронного сообщения по электронному адресу, указанному Покупателем при регистрации, или по телефону. Менеджер, обслуживающий данный Заказ, уточняет детали Заказа, согласовывает дату доставки, которая зависит от наличия заказанных Товаров на складе Продавца и времени, необходимого для обработки и доставки Заказа.&lt;br&gt;4.9. Ожидаемая дата передачи Заказа в Службу доставки сообщается Покупателю менеджером, обслуживающим Заказ, по электронной почте или при контрольном звонке Покупателю.&lt;br&gt;5. Доставка заказа&lt;br&gt;5.1. Способы доставки Товаров указаны на Сайте в разделе «Доставка товара»&amp;nbsp;&lt;br&gt;5.2.&amp;nbsp;&lt;span style=&quot;color: red;&quot;&gt;Территория доставки Товаров, представленных на Сайте, ограничена пределами Республики Казахстан.&lt;/span&gt;&lt;br&gt;5.3. Задержки в доставке возможны ввиду непредвиденных обстоятельств, произошедших не по вине Продавца.&lt;br&gt;5.4. При доставке, Заказ вручается Покупателю либо третьему лицу, указанному Покупателем в Заказе в качестве получателя (далее Покупатель и третье лицо именуются «Получатель»). При невозможности получения Заказа, оплаченного посредством наличного расчета, указанными выше лицами, Заказ может быть вручен лицу, которое может предоставить сведения о Заказе (номер отправления и/или ФИО Получателя), а также оплатить стоимость Заказа (если Заказ не был оплачен) и стоимость доставки Заказа в полном объеме лицу, осуществляющему доставку Заказа.&lt;br&gt;5.5. Во избежание мошенничества, а также для выполнения взятых на себя обязательств, указанных в пункте 5. настоящего Соглашения, при вручении предоплаченного Заказа, лицо, осуществляющее доставку Заказа, вправе затребовать документ, удостоверяющий личность Получателя, а также указать тип и номер предоставленного Получателем документа, на квитанции к Заказу. Продавец гарантирует конфиденциальность и защиту персональных данных Получателя.&lt;br&gt;5.6. Риск случайного повреждения и гибели Товара переходит к Покупателю с момента передачи ему Заказа и проставления Получателем Заказа подписи в документах, подтверждающих доставку Заказа. В случае недоставки Заказа Продавец возмещает Покупателю стоимость предоплаченного Покупателем Заказа и/или доставки в полном объеме, после получения от Службы доставки подтверждения утраты Заказа.&lt;br&gt;5.7. Стоимость доставки каждого Заказа рассчитывается индивидуально, исходя из веса Товара, способа и региона доставки и указывается на Сайте.&lt;br&gt;Порядок доставки и вскрытия Отправлений, содержащих товар, определяется действующими правилами почтовой связи.&lt;br&gt;5.8. Обязанность Продавца передать товар Покупателю считается исполненной в момент вручения лицом, осуществляющим доставку (представителем/сотрудником Заказчика и/или третьим лицом, привлеченным Заказчиком для доставки Товара Покупателю) товара покупателю или его представителю, или &amp;nbsp;получение Товара Покупателем в отделении почтовой связи.&lt;br&gt;5.9. Покупатель &amp;nbsp;понимает и соглашается с тем, что:&lt;br&gt;доставка Товара - отдельная услуга, не являющаяся неотъемлемой частью приобретаемого Покупателем Товара, выполнение которой заканчивается в момент получения Покупателем Товара и осуществления платежа за него. Претензии к качеству приобретенного Товара, возникшие после получения и оплаты Товара, рассматриваются в соответствии с Законом РК «О защите прав потребителей» и гарантийными обязательствами Продавца. В связи с этим, приобретение Товара с доставкой, не дает Покупателю право требования доставки приобретенного Товара в целях гарантийного обслуживания или замены, не дает возможности осуществлять гарантийное обслуживание или замену Товара посредством выезда представителя Продавца к Покупателю.&lt;br&gt;5.10. При получении Покупателем Заказа, Покупатель обязуется осмотреть упаковку Заказа на предмет ее целостности.&lt;br&gt;5.10.1. В случае целостности упаковки, Покупатель оплачивает Заказ в полном объеме и расписывается в накладной (или ином соответствующем документе), подтверждая получение Заказа.&lt;br&gt;5.10.2. В случае обнаружения Покупателем нарушения целостности упаковки, Покупатель обязан отразить данный факт в письменном виде в накладной (или ином соответствующем документе), предоставленной представителем Продавца, осуществившим доставку Заказа, или сотрудником АО «Казпочта» (если доставка осуществлялась через АО «Казпочта»). После этого Покупатель вправе, в присутствии представителя Продавца/сотрудника АО «Казпочта», вскрыть упаковку Заказа, для проверки содержимого Заказа. В случае соответствия содержимого Заказа (Товара) данным, указанным в накладной, Покупатель оплачивает и принимает Заказ в полном объеме, поставив свою подпись в накладной (или ином соответствующем документе). В случае несоответствия фактически вложенного в Заказ Товара, данным, указанным в накладной (или ином соответствующем документе), Покупатель обязан в присутствии представителя Продавца/сотрудника АО «Казпочта», составить соответствующий Акт, о выявленных несоответствиях (расхождениях). В таком случае Заказ Покупателю не передается, а предоплата (в случае если Покупатель вносил предоплату за Заказ) возвращается Продавцом Покупателю путем перечисления денежных средств на расчетный счет, указанный Покупателем, либо путем почтового перевода, в соответствии с условиями, изложенными на Сайте.&lt;br&gt;5.10.3. Заказ, доставленный Покупателю, может быть принят Покупателем только в полном объеме. Частичная приемка Заказа не допускается.&lt;br&gt;6. Оплата товара&lt;br&gt;6.1. Все расчеты между Сторонами осуществляются в тенге.&lt;br&gt;6.2. Цена Товара указывается на Сайте. В случае неверного указания цены заказанного Покупателем Товара, Продавец информирует об этом Покупателя, для подтверждения последним, Заказа по исправленной цене либо аннулирования Заказа. При невозможности связаться с Покупателем, данный Заказ считается аннулированным. Если Заказ был оплачен, Продавец возвращает Покупателю оплаченную за Заказ сумму, в порядке, предусмотренном настоящим Соглашением.&lt;br&gt;6.3. Цена Товара на Сайте может быть изменена Продавцом в одностороннем порядке. При этом цена на заказанный Покупателем Товар изменению не подлежит.&lt;br&gt;6.4. Особенности оплаты Товара с помощью банковских карт:&lt;br&gt;6.4.1. В соответствии с Правилами выпуска и использования платежных карт в Республике Казахстан, утвержденными Постановлением Правления Национального Банка Республики Казахстан от 24 августа 2000 года № 331, операции по банковским картам совершаются держателем карты либо уполномоченным им лицом.&lt;br&gt;6.4.2. Авторизация операций по банковским картам осуществляется банком. Если у банка есть основания полагать, что операция носит мошеннический характер, то банк вправе отказать в осуществлении данной операции.&lt;br&gt;6.4.3. Во избежание различных случаев неправомерного использования банковских карт при оплате товаров и услуг, все Заказы, оформленные на Сайте с оплатой банковской картой, проверяются Продавцом, Согласно Правилам международных платежных систем, в целях проверки личности владельца и его прав на использование карты. Покупатель, оформивший такой заказ, обязан по запросу, поступившему от Продавца, предоставить копию двух страниц паспорта владельца банковской карты - разворота с фотографией, а также копию банковской карты с обеих сторон (номер карты нужно закрыть, кроме последних четырех цифр). Продавец оставляет за собой право без объяснения причины аннулировать Заказ, в том числе в случае непредставления указанных документов Покупателем (по электронной почте в виде сканированных копий) в течение 14 (четырнадцати) календарных дней с даты оформления Заказа. Стоимость Заказа возвращается на карту владельца.&lt;br&gt;6.5. Продавец вправе предоставлять скидки на Товары и устанавливать программу бонусов. Виды скидок, бонусов, порядок и условия начисления указанные на Сайте могут быть изменены Продавцом в одностороннем порядке.&lt;br&gt;7. Возврат товара и денежных средств&lt;br&gt;7.1. Возврат Товара осуществляется в соответствии с условиями возврата, указанными на Сайте по адресу&amp;nbsp;&lt;a href=&quot;http://www.fashionpark.kz/&quot; style=&quot;background-color: rgb(255, 255, 255); border: none; outline: none; color: rgb(87, 110, 117);&quot;&gt;http://www.fashionpark.kz&lt;/a&gt;&lt;br&gt;7.2.1. Покупатель вправе отказаться от заказанного товара в течение 14 дней, со дня получения товара, за исключением Товаров, указанных в п. 7.2.4. настоящего Соглашения. Возврат Товара надлежащего качества возможен в случае, если он не был в употреблении, сохранены его товарный вид, потребительские свойства, этикетка возврата, а также документ, подтверждающий факт и условия покупки указанного Товара.&lt;br&gt;7.2.2.&amp;nbsp;При отказе Покупателя от Товара согласно п.7.2.1. Продавец возвращает ему стоимость возвращенного Товара, за исключением расходов Продавца, связанных с доставкой возвращенного Покупателем Товара, в течение 10 банковских дней с даты получения Продавцом письменного заявления от Покупателя на возврат Товара, фискального чека, товарной накладной, копии документа, удостоверяющего личность и непосредственно самого товара.&lt;br&gt;7.2.3. Если заказанный Покупателем Товар отсутствует в продаже у Продавца, Покупатель вправе отказаться от исполнения настоящего Соглашения и потребовать возврата уплаченной за указанный Товар денежной суммы. Продавец обязан вернуть уплаченную за товар денежную сумму в течение 14 банковских дней.&lt;br&gt;7.2.4. Не подлежат возврату чулочно-носочные изделия и нательное белье (в соответствии с п.1 ст.30 Закона РК о защите прав потребителей).&lt;br&gt;7.2.5. Единовременно можно вернуть не более 3-х товаров.&lt;br&gt;7.3. Возврат Товара ненадлежащего качества: Под товаром ненадлежащего качества подразумевается товар, который не может обеспечить исполнение своих функций. Полученный Товар должен соответствовать описанию на Сайте. Отличие элементов дизайна или оформления от заявленных в описании на Сайте, не является признаком некачественности или нефункциональности Товара.&lt;br&gt;7.3.1. Если Покупателю был передан Товар ненадлежащего качества, что не было заранее оговорено Продавцом, Покупатель вправе воспользоваться положениями ст. 15 «Права потребителя в случае продажи товара ненадлежащего качества» Закона РК о защите прав потребителей.&lt;br&gt;7.3.2. Требования Покупателя о возврате уплаченной за товар ненадлежащего качества денежной суммы подлежат удовлетворению Продавцом в течение 10 дней со дня предъявления Покупателем соответствующего требования.&lt;br&gt;7.4. Возврат денежных средств осуществляется посредством возврата стоимости оплаченного Товара на банковскую карту или путем почтового перевода. Способ возврата денежных средств должен быть указан Покупателем в соответствующем поле заявления на возврат Товара, которое можно найти здесь.&lt;br&gt;8. Ответственность&lt;br&gt;8.1. Продавец не несет ответственности за ущерб, причиненный Покупателю вследствие ненадлежащего использования Товаров, приобретенных в Интернет-магазине.&lt;br&gt;8.2. Продавец не несет ответственности за содержание и функционирование внешних сайтов.&lt;br&gt;8.3. За неисполнение и/или ненадлежащее исполнение Сторонами принятых на себя по настоящему Договору обязательств, Стороны несут ответственность в соответствии с действующим законодательством РК.&lt;br&gt;9. Конфиденциальность и защита информации, предоставляемой Пользователем/Покупателем&lt;br&gt;9.1. Персональные данные Пользователя/Покупателя обрабатываются в соответствии c Законом РК от 21.05.2013 г. №94-V «О персональных данных и их защите» и Политикой конфиденциальности Продавца.&lt;br&gt;9.2. При регистрации на Сайте, Пользователь предоставляет следующую информацию: Фамилия, Имя, контактный номер телефона, адрес электронной почты, пароль для доступа к Сайту.&lt;br&gt;9.3. Принимая условия настоящего Соглашения, Посетитель/Пользователь/Покупатель дает согласие на сбор и обработку своих персональных данных Продавцом, в том числе и в целях продвижения Продавцом товаров и услуг, а также передачу персональных данных третьим лицам, привлеченным Продавцом для выполнения обязательств перед Посетителем Сайта/Пользователем/ Покупателем в рамках настоящего Соглашения.&lt;br&gt;9.4. Продавец использует персональные данные Пользователя/Покупателя:&lt;br&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;/p&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;для регистрации Пользователя на Сайте;&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;для выполнения своих обязательств перед Пользователем/Покупателем;&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;для оценки и анализа работы Сайта;&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;для определения победителя в акциях, проводимых Продавцом (в случае проведения таковых).&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;9.4.1. Продавец имеет право отправлять информационные, в том числе рекламные сообщения, на электронную почту и мобильный телефон Пользователя/Покупателя с его согласия. Пользователь/Покупатель вправе отказаться от получения рекламной и другой информации без объяснения причин отказа. Сервисные сообщения, информирующие Пользователя/Покупателя о заказе и этапах его обработки, отправляются автоматически и не могут быть отклонены Пользователем/Покупателем.&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;9.5. Продавец вправе использовать технологию &quot;cookies&quot;.&quot;Cookies&quot; не содержат конфиденциальную информацию и не передаются третьим лицам.&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;9.6. Продавец получает информацию об IP-адресе посетителя Сайта&amp;nbsp;&lt;/span&gt;&lt;a href=&quot;http://www.bravo.kz/&quot; style=&quot;text-align: justify; background-color: rgb(255, 255, 255); border: none; outline: none; color: rgb(87, 110, 117);&quot;&gt;www.bravo.kz&lt;/a&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;. Данная информация не используется Продавцом для установления личности посетителя.&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;9.7. Продавец не несет ответственности за сведения, предоставленные Пользователем/Покупателем на Сайте в общедоступной форме.&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;9.8. Продавец вправе осуществлять записи телефонных разговоров с Пользователем/Покупателем. При этом Продавец обязуется: предотвращать попытки несанкционированного доступа к информации, полученной в ходе телефонных переговоров, и/или передачу ее третьим лицам, не имеющим непосредственного отношения к исполнению Заказов.&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;10. Дополнительные условия&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;10.1. Продавец не вправе каким-либо способом передавать свои права и обязанности, вытекающие из его отношений с Покупателем, третьим лицам.&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;10.2. Интернет-магазин и предоставляемые сервисы могут быть временно недоступны, частично или полностью, по причине проведения профилактических или иных работ, а также по любым другим причинам технического характера. Техническая служба Продавца имеет право периодически проводить необходимые профилактические или иные работы с предварительным уведомлением Покупателей или без такового.&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;10.3. К отношениям между Пользователем/Покупателем и Продавцом применяются положения законодательства Республики Казахстан.&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;10.4. В случае возникновения вопросов и претензий со стороны Пользователя/Покупателя он должен обратиться к Продавцу по телефону или иным доступным способом. Все возникающее споры стороны будут стараться решить путем переговоров, при не достижении соглашения, спор будет передан на рассмотрение в судебные органы в соответствии с действующим законодательством РК.&lt;/span&gt;&lt;br&gt;&lt;span style=&quot;color: rgb(0, 0, 0); text-align: justify;&quot;&gt;10.5. Признание судом недействительности какого-либо положения настоящего Соглашения не влечет за собой недействительность остальных положений.&lt;/span&gt;&lt;br&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px; color: rgb(0, 0, 0); text-align: justify;&quot;&gt;&lt;/p&gt;', 'Политика Безопасности', '', '', ''),
(5, 1, 'Способы оплаты', '&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Verdana;&quot;&gt;Наличный расчет – оплата наличными деньгами курьеру после получения заказа. .&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;br&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Verdana;&quot;&gt;Банковская карта - Оплата банковской картой возможна через интернет систему электронных платежей. Не принимаются банковские карты Visa и MasterCard без кода CVV2/CVC2. При доставке на дом оплаченного онлайн заказа сотрудник службы доставки может попросить предъявить документ, удостоверяющий личность.&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Verdana;&quot;&gt;К оплате принимаются платежные карточки международных платёжных систем VISA и MasterCard. Удобный и безопасный способ оплаты&amp;nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Verdana;&quot;&gt;Чтобы оплатить заказ с помощью банковской карты, необходимо:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;ol&gt;&lt;li&gt;Выбрать товар;&lt;br&gt;&lt;/li&gt;&lt;li&gt;Ваши ФИО, e-mail и контактный телефон&lt;br&gt;&lt;/li&gt;&lt;li&gt;Нажать кнопку &quot;Оформить заказ&quot;&lt;br&gt;&lt;/li&gt;&lt;/ol&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Verdana;&quot;&gt;Вам необходимо заполнить данные своей банковской карты:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;ol&gt;&lt;li&gt;Ваше имя, как указано на карте.&lt;br&gt;&lt;/li&gt;&lt;li&gt;Номер карты, он находится с лицевой стороны карты, 16-19 цифр без пробелов.&lt;br&gt;&lt;/li&gt;&lt;li&gt;Месяц и год окончания срока действия карты, они находится так же с лицевой стороны карты.&lt;br&gt;&lt;/li&gt;&lt;li&gt;Код CVC2/CVV2, находятся на обратной стороне карты на полосе для подписи, необходимо ввести последние 3 или 4 цифры номера.&lt;br&gt;&lt;/li&gt;&lt;/ol&gt;&lt;/p&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;span style=&quot;color: rgb(0, 0, 0); font-family: Verdana;&quot;&gt;После нажатия кнопки &quot;Оплатить&quot;, возможна необходимость в вводе Вашего пароля 3-D Secure. Этот пароль Вы можете получить в Вашем банке. Для ввода этого кода вы будете направлены на страницу Вашего банка.&lt;/span&gt;&lt;/p&gt;&lt;ul style=&quot;padding: 0px; margin-right: 0px; margin-bottom: 22px; margin-left: 0px; list-style: none; color: rgb(0, 0, 0); line-height: 1.2;&quot;&gt;&lt;li style=&quot;padding: 0px; margin: 0px;&quot;&gt;&lt;ul style=&quot;padding: 0px; margin-right: 0px; margin-bottom: 22px; margin-left: 0px; list-style: none; line-height: 1.2;&quot;&gt;&lt;li style=&quot;padding: 0px; margin: 0px;&quot;&gt;&lt;p style=&quot;padding: 0px; margin-bottom: 0px;&quot;&gt;&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;/li&gt;&lt;li style=&quot;padding: 0px; margin: 0px;&quot;&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&lt;/ul&gt;', 'Способы оплаты', '', '', ''),
(6, 1, 'Информация о доставке', '&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Срок доставки в крупные города Казахстана до 3х рабочих дней. &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Срок доставки по СНГ &amp;nbsp;- от 7до 10 дней. &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Все подробности доставки Вы можете уточнить по тел. 87052227268&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Для возврата товара необходимо чтобы товар имел оригинальные ярлыки, упаковку и сохранный товарный вид.&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Единовременно можно вернуть не более 3-х товаров &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;В случае частичного возврата товара стоимость доставки будет вычисляться из конечной суммы приобретаемого товара. &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Сроки доставки рассчитывается с даты подтверждения заказа оператором.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;h3&gt;&lt;span style=&quot;font-weight: bold; text-decoration-line: underline; font-family: Verdana;&quot;&gt;ЭКСПРЕСС&lt;/span&gt;&lt;/h3&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Возможна срочная доставка почтовых отправлений по всему миру (включительно) &amp;nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Экспресс доставка осуществляется компанией PONY EXPRESS. &lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Сроки доставки от 1 до 3 рабочих дней. Отдалённые районы (удаленные от областных центров) 2 – 8 рабочих дней.&lt;/span&gt;&lt;/p&gt;', 'Информация о доставке', '', '', '');
INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(6, 2, 'Delivery Information', '&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Delivery time to major cities in Kazakhstan is up to 3 working days.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;The delivery time for the CIS is from 7 to 10 days.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;All details of delivery you can specify by phone. 87052227268&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;At One time you can return no more than 3 products&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;In the case of a partial return of the goods, the cost of delivery will be calculated from the final amount of the purchased goods.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;The delivery time is calculated from the date of confirmation of the order by the operator. EXPRESS&amp;nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Urgent delivery of mail items worldwide is possible (inclusive)&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;In order to return the goods, it is necessary that the goods have original labels, packaging and a preserved presentation.&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;h3&gt;&lt;span style=&quot;font-family: Verdana; text-decoration-line: underline; font-weight: bold;&quot;&gt;Express delivery is carried out by PONY EXPRESS.&lt;/span&gt;&lt;/h3&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Delivery time is from 1 to 3 working days. Remote areas (remote from the regional centers) 2 - 8 working days.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;MAIL The period of storage of the order: 14 days, after 14 days the order is disbanded&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Payment by cash on delivery (commission of Kazpost is charged in the amount of 2% of the order value)&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Schedule the work of mail check in your office COURIER&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-family: Verdana;&quot;&gt;Shelf life: 7 days&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;', 'Delivery Information', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_layout`
--

CREATE TABLE IF NOT EXISTS `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`information_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_to_layout`
--

INSERT INTO `oc_information_to_layout` (`information_id`, `store_id`, `layout_id`) VALUES
(6, 0, 0),
(3, 0, 0),
(5, 0, 0),
(4, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`information_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_language`
--

CREATE TABLE IF NOT EXISTS `oc_language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'Russian', 'ru', 'ru_RU.UTF-8,ru_RU,russian', 'ru.png', 'russian', 0, 1),
(2, 'English', 'en', 'en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout`
--

CREATE TABLE IF NOT EXISTS `oc_layout` (
  `layout_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`layout_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Главная'),
(2, 'Товар'),
(3, 'Категория'),
(4, 'По-умолчанию'),
(5, 'Список Производителей'),
(6, 'Аккаунт'),
(7, 'Оформление заказа'),
(8, 'Контакты'),
(9, 'Карта сайта'),
(10, 'Партнерская программа'),
(11, 'Информация'),
(12, 'Сравнение'),
(13, 'Поиск'),
(14, 'Категории Блога'),
(15, 'Блог'),
(16, 'Статьи Блога'),
(17, 'Производители'),
(18, 'Mega Filter PRO');

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_module`
--

CREATE TABLE IF NOT EXISTS `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`layout_module_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(102, 3, 'category', 'column_left', 1),
(101, 1, 'category', 'header_left', 0),
(100, 1, 'information', 'column_right', 0),
(99, 1, 'html.51', 'content_bottom', 4),
(98, 1, 'html.50', 'content_bottom', 3),
(97, 1, 'html.49', 'column_left', 0),
(96, 1, 'html.48', 'header_right', 1),
(95, 1, 'html.47', 'header_right', 0),
(103, 2, 'category', 'column_left', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_route`
--

CREATE TABLE IF NOT EXISTS `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(255) NOT NULL,
  PRIMARY KEY (`layout_route_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(75, 6, 0, 'account/%'),
(17, 10, 0, 'affiliate/%'),
(86, 3, 0, 'product/category'),
(79, 1, 0, 'common/home'),
(20, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(74, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search'),
(71, 14, 0, 'blog/category'),
(69, 15, 0, 'blog/latest'),
(72, 16, 0, 'blog/article'),
(73, 17, 0, 'product/manufacturer/info'),
(82, 18, 0, 'module/mega_filter/results');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class`
--

CREATE TABLE IF NOT EXISTS `oc_length_class` (
  `length_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) NOT NULL,
  PRIMARY KEY (`length_class_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class_description`
--

CREATE TABLE IF NOT EXISTS `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  PRIMARY KEY (`length_class_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Сантиметр', 'см'),
(2, 2, 'Centimeter', 'cm'),
(3, 1, 'Миллиметр', 'мм'),
(4, 2, 'Millimeter', 'mm'),
(5, 1, 'Дюйм', 'in'),
(6, 2, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `oc_location`
--

CREATE TABLE IF NOT EXISTS `oc_location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer`
--

CREATE TABLE IF NOT EXISTS `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `noindex` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_category`
--

CREATE TABLE IF NOT EXISTS `oc_manufacturer_category` (
  `manufacturer_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`manufacturer_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_category_description`
--

CREATE TABLE IF NOT EXISTS `oc_manufacturer_category_description` (
  `manufacturer_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_description`
--

CREATE TABLE IF NOT EXISTS `oc_manufacturer_description` (
  `manufacturer_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  PRIMARY KEY (`manufacturer_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_manufacturer_description`
--

INSERT INTO `oc_manufacturer_description` (`manufacturer_id`, `language_id`, `description`, `meta_description`, `meta_keyword`, `meta_title`, `meta_h1`) VALUES
(8, 2, '&lt;p&gt;описание en&lt;br&gt;&lt;/p&gt;', 'desc en', 'keyw en', 'title en', 'h1 en'),
(8, 1, '&lt;p&gt;описание ru&lt;br&gt;&lt;/p&gt;', 'desc ru', 'keyw ru', 'длиннный тайтл длиннный тайтлдлиннный тайтлдлиннный тайтлдлиннный тайтл длиннный тайтл длиннный тайтлдлиннный тайтлдлиннный тайтлдлиннный тайтл', 'h1 ru'),
(9, 1, '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(9, 2, '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(7, 1, '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(7, 2, '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(5, 1, '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(5, 2, '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(6, 1, '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(6, 2, '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(10, 1, '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', ''),
(10, 2, '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_to_category`
--

CREATE TABLE IF NOT EXISTS `oc_manufacturer_to_category` (
  `manufacturer_category_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`manufacturer_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_marketing`
--

CREATE TABLE IF NOT EXISTS `oc_marketing` (
  `marketing_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`marketing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_menu`
--

CREATE TABLE IF NOT EXISTS `oc_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort_order` int(3) NOT NULL,
  `columns` int(3) NOT NULL,
  `menu_type` varchar(20) NOT NULL,
  `status` int(3) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=93 ;

--
-- Dumping data for table `oc_menu`
--

INSERT INTO `oc_menu` (`menu_id`, `sort_order`, `columns`, `menu_type`, `status`) VALUES
(70, 2, 1, 'category', 1),
(9, 1, 1, 'product', 1),
(81, 5, 1, 'category', 1),
(17, 1, 1, 'manufacturer', 1),
(30, 1, 1, 'information', 1),
(35, 1, 1, 'manufacturer', 1),
(36, 1, 1, 'manufacturer', 1),
(37, 1, 1, 'manufacturer', 1),
(38, 1, 1, 'manufacturer', 1),
(39, 1, 1, 'manufacturer', 1),
(41, 1, 1, 'custom', 1),
(44, 1, 1, 'custom', 1),
(49, 1, 1, 'custom', 1),
(51, 1, 1, 'custom', 1),
(89, 6, 1, '', 1),
(92, 6, 1, '', 1),
(74, 6, 1, '', 1),
(75, 8, 1, 'custom', 1),
(78, 7, 1, 'information', 1),
(88, 4, 1, 'category', 1),
(86, 3, 1, 'category', 1),
(87, 5, 1, 'information', 1),
(91, 4, 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_menu_child`
--

CREATE TABLE IF NOT EXISTS `oc_menu_child` (
  `menu_child_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `menu_type` varchar(20) NOT NULL,
  `status` int(3) NOT NULL,
  PRIMARY KEY (`menu_child_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=134 ;

--
-- Dumping data for table `oc_menu_child`
--

INSERT INTO `oc_menu_child` (`menu_child_id`, `menu_id`, `sort_order`, `menu_type`, `status`) VALUES
(85, 75, 1, '', 1),
(86, 70, 1, 'category', 1),
(87, 70, 1, '', 1),
(88, 70, 2, 'category', 1),
(89, 70, 1, '', 1),
(90, 70, 2, '', 1),
(91, 70, 1, '', 1),
(92, 70, 2, '', 1),
(93, 77, 1, 'category', 1),
(94, 70, 1, '', 1),
(95, 70, 2, '', 1),
(97, 77, 2, '', 1),
(98, 70, 1, '', 1),
(99, 70, 2, '', 1),
(100, 78, 1, '', 1),
(101, 77, 2, '', 1),
(102, 70, 1, '', 1),
(103, 70, 2, '', 1),
(104, 77, 1, '', 1),
(105, 70, 1, '', 1),
(106, 70, 2, '', 1),
(107, 81, 1, '', 1),
(108, 70, 1, '', 1),
(109, 70, 2, '', 1),
(110, 81, 1, '', 1),
(111, 81, 2, 'category', 1),
(112, 70, 1, '', 1),
(113, 70, 2, '', 1),
(114, 81, 1, '', 1),
(115, 81, 2, '', 1),
(116, 81, 3, 'category', 1),
(117, 70, 1, '', 1),
(118, 70, 2, '', 1),
(119, 81, 1, '', 1),
(120, 81, 2, '', 1),
(121, 81, 3, '', 1),
(122, 70, 1, '', 1),
(123, 70, 2, '', 1),
(133, 81, 1, 'category', 1),
(125, 81, 2, '', 1),
(126, 81, 3, '', 1),
(127, 81, 4, '', 1),
(128, 70, 1, '', 1),
(129, 70, 2, '', 1),
(130, 81, 1, '', 1),
(131, 81, 2, '', 1),
(132, 81, 3, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_menu_child_description`
--

CREATE TABLE IF NOT EXISTS `oc_menu_child_description` (
  `menu_child_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL DEFAULT '',
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`menu_child_id`,`language_id`),
  KEY `menu_id` (`menu_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_menu_child_description`
--

INSERT INTO `oc_menu_child_description` (`menu_child_id`, `menu_id`, `name`, `link`, `language_id`) VALUES
(53, 0, 'Телефоны и PDA', '24', 1),
(53, 0, 'Phones &amp; PDAs', '24', 2),
(86, 70, 'Мужские часы', '61', 1),
(86, 70, 'Men''s Watch', '61', 2),
(88, 70, 'Женские часы', '62', 1),
(88, 70, 'Ladies Watch', '62', 2),
(93, 77, 'Портмоне', '63', 1),
(93, 77, 'Purse', '63', 2),
(133, 81, 'Purse', '63', 2),
(111, 81, 'Ремни', '65', 1),
(111, 81, 'Belts', '65', 2),
(116, 81, 'Сумки', '64', 1),
(116, 81, 'Bags', '64', 2),
(133, 81, 'Портмоне', '63', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_menu_child_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_menu_child_to_store` (
  `menu_child_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_child_id`,`store_id`),
  KEY `store_id` (`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_menu_child_to_store`
--

INSERT INTO `oc_menu_child_to_store` (`menu_child_id`, `store_id`) VALUES
(2, 0),
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(29, 0),
(32, 0),
(33, 0),
(37, 0),
(42, 0),
(45, 0),
(47, 0),
(57, 0),
(58, 0),
(60, 0),
(63, 0),
(67, 0),
(77, 0),
(78, 0),
(86, 0),
(88, 0),
(93, 0),
(111, 0),
(116, 0),
(133, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_menu_description`
--

CREATE TABLE IF NOT EXISTS `oc_menu_description` (
  `menu_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL DEFAULT '',
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`,`language_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_menu_description`
--

INSERT INTO `oc_menu_description` (`menu_id`, `name`, `link`, `language_id`) VALUES
(70, 'Watches', '20', 2),
(88, 'Money clip', '59', 2),
(88, 'Зажим для денег', '59', 1),
(26, 'PC', '', 1),
(26, 'PC', '', 2),
(30, 'О нас', '4', 1),
(30, 'About Us', '4', 2),
(75, 'Контакты', 'information/contact', 1),
(37, 'Apple', '8', 0),
(38, 'Apple', '8', 0),
(70, 'Часы', '20', 1),
(81, 'Accessories', '60', 2),
(75, 'Contact Us', 'information/contact', 2),
(78, 'Информация о доставке', '6', 1),
(86, 'Pens', '24', 2),
(86, 'Ручки', '24', 1),
(78, 'Delivery Information', '6', 2),
(81, 'Аксессуары', '60', 1),
(87, 'Способы оплаты', '5', 1),
(87, 'Terms &amp; Conditions', '5', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_menu_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_menu_to_store` (
  `menu_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`,`store_id`),
  KEY `store_id` (`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_menu_to_store`
--

INSERT INTO `oc_menu_to_store` (`menu_id`, `store_id`) VALUES
(9, 0),
(17, 0),
(30, 0),
(35, 0),
(36, 0),
(37, 0),
(38, 0),
(41, 0),
(44, 0),
(49, 0),
(51, 0),
(70, 0),
(75, 0),
(78, 0),
(81, 0),
(86, 0),
(87, 0),
(88, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_modification`
--

CREATE TABLE IF NOT EXISTS `oc_modification` (
  `modification_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`modification_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `oc_modification`
--

INSERT INTO `oc_modification` (`modification_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`) VALUES
(1, 'Menu Item for the Acute Category Manager', 'Acute-Category-Manager', 'tabook', '1.0', '', '<!-- Created using vQmod XML Generator by UKSB - http://uksb.github.com/vqgen/ //-->\n<modification>\n  <id><![CDATA[Acute Category Manager]]></id>\n <version><![CDATA[1.0]]></version>\n        <name>Menu Item for the Acute Category Manager</name>\n        <code>Acute-Category-Manager</code>\n  <vqmver><![CDATA[2.1.6]]></vqmver>\n  <author><![CDATA[tabook]]></author>\n <file path="admin/view/template/common/menu.tpl">\n   <operation>\n     <search><![CDATA[<li><a href="<?php echo $category; ?>"><?php echo $text_category; ?></a></li>]]></search>\n      <add position="replace"><![CDATA[<li><a href="<?php echo $category; ?>"><?php echo $text_category; ?></a></li>\n<li><a href="<?php echo $category_mgr_lite; ?>"><?php echo $category_mgr_lite_heading_title; ?></a></li>]]></add>\n   </operation>\n  </file>\n <file path="admin/controller/common/menu.php">\n    <operation>\n     <search><![CDATA[$this->load->language(''common/menu'');]]></search>\n      <add position="replace"><![CDATA[\r\n$this->load->language(''common/menu'');\r\n$data[''category_mgr_lite''] = $this->url->link(''catalog/category_mgr_lite'', ''token='' . $this->session->data[''token''], ''SSL'');\r\n$this->load->language(''catalog/category_mgr_lite'');\r\n$data[''category_mgr_lite_heading_title''] = $this->language->get(''category_mgr_heading_title'');]]></add>\n    </operation>\n  </file>\n</modification>', 1, '2017-08-19 04:47:12'),
(5, 'Local copy OCMOD by iSenseLabs', 'isensealabs_quickfix_ocmod', 'iSenseLabs', '1.3', 'http://isenselabs.com', '<modification>\r\n    <name>Local copy OCMOD by iSenseLabs</name>\r\n  <version>1.3</version>\r\n  <link>http://isenselabs.com</link>\r\n  <author>iSenseLabs</author>\r\n <code>isensealabs_quickfix_ocmod</code>\r\n\r\n <file path="admin/controller/extension/installer.php">\r\n    <operation error="skip">\r\n      <search ><![CDATA[''url''  => str_replace(''&amp;'', ''&'', $this->url->link(''extension/installer/ftp'', ''token='' . $this->session->data[''token''],]]></search>\r\n     <add position="replace"><![CDATA[''url''  => str_replace(''&amp;'', ''&'', $this->url->link(''extension/installer/localcopy'', ''token='' . $this->session->data[''token''],]]></add>\r\n   </operation>\r\n\r\n    <operation>\r\n     <search><![CDATA[public function unzip() {]]></search>\r\n      <add position="before"><![CDATA[      \r\n  public function localcopy() {\r\n   $this->load->language(''extension/installer'');\r\n\r\n   $json = array();\r\n\r\n    if (!$this->user->hasPermission(''modify'', ''extension/installer'')) {\r\n     $json[''error''] = $this->language->get(''error_permission'');\r\n    }\r\n\r\n   if (VERSION == ''2.0.0.0'') {\r\n       $directory = DIR_DOWNLOAD  . str_replace(array(''../'', ''..\\\\'', ''..''), '''', $this->request->post[''path'']) . ''/upload/'';\r\n    } else {\r\n        $directory = DIR_UPLOAD  . str_replace(array(''../'', ''..\\\\'', ''..''), '''', $this->request->post[''path'']) . ''/upload/'';\r\n    }\r\n\r\n   if (!is_dir($directory)) {\r\n      $json[''error''] = $this->language->get(''error_directory'');\r\n   }\r\n\r\n   if (!$json) {\r\n     // Get a list of files ready to upload\r\n      $files = array();\r\n\r\n     $path = array($directory . ''*'');\r\n\r\n      while (count($path) != 0) {\r\n       $next = array_shift($path);\r\n\r\n       foreach (glob($next) as $file) {\r\n          if (is_dir($file)) {\r\n            $path[] = $file . ''/*'';\r\n         }\r\n\r\n         $files[] = $file;\r\n       }\r\n     }\r\n\r\n     $root = dirname(DIR_APPLICATION).''/'';\r\n\r\n     foreach ($files as $file) {\r\n       // Upload everything in the upload directory\r\n        $destination = substr($file, strlen($directory));\r\n\r\n       // Update from newer OpenCart versions:\r\n       if (substr($destination, 0, 5) == ''admin'') {\r\n          $destination = DIR_APPLICATION . substr($destination, 5);\r\n       } else if (substr($destination, 0, 7) == ''catalog'') {\r\n         $destination = DIR_CATALOG . substr($destination, 7);\r\n       } else if (substr($destination, 0, 5) == ''image'') {\r\n         $destination = DIR_IMAGE . substr($destination, 5);\r\n       } else if (substr($destination, 0, 6) == ''system'') {\r\n          $destination = DIR_SYSTEM . substr($destination, 6);\r\n        } else {\r\n          $destination = $root.$destination;\r\n        }\r\n\r\n       if (is_dir($file)) {\r\n          if (!file_exists($destination)) {\r\n           if (!mkdir($destination)) {\r\n             $json[''error''] = sprintf($this->language->get(''error_ftp_directory''), $destination);\r\n            }\r\n         }\r\n       }\r\n\r\n       if (is_file($file)) {\r\n         if (!copy($file, $destination)) {\r\n           $json[''error''] = sprintf($this->language->get(''error_ftp_file''), $file);\r\n          }\r\n       }\r\n     }\r\n   }\r\n\r\n   $this->response->addHeader(''Content-Type: application/json'');\r\n   $this->response->setOutput(json_encode($json));\r\n }]]></add>\r\n    </operation>\r\n  </file> \r\n</modification>\r\n', 1, '2017-08-23 16:20:45'),
(6, 'Power Image Manager 2.0', '1vHShWu', 'Sharley''s Web Development', '3.1.2', 'http://bit.ly/1vHShWu', ' <modification>\r\n  <name>Power Image Manager 2.0</name>\r\n  <version>3.1.2</version>\r\n  <link>http://bit.ly/1vHShWu</link>\r\n  <author>Sharley''s Web Development</author>\r\n  <code>1vHShWu</code>\r\n\r\n  <file path="admin/controller/common/header.php">\r\n    <operation>\r\n        <search><![CDATA[return $this->load->view(''common/header]]></search>\r\n        <add position="before"><![CDATA[\r\n              $data[''width''] = 800;\r\n              $data[''height''] = 600;\r\n              $data[''lang''] = ''en'';\r\n              if ($this->config->get(''pim_status'')) {\r\n                $data[''width''] = $this->config->get(''pim_width'');\r\n                $data[''height''] = $this->config->get(''pim_height'');\r\n\r\n                if ($this->config->get(''pim_language'')) {\r\n                $data[''lang''] = $this->config->get(''pim_language'');\r\n                }\r\n              }\r\n          $data[''pim_status''] = $this->config->get(''pim_status'');                    \r\n        ]]></add>\r\n    </operation>\r\n<!--\r\n    <operation>\r\n        <search><![CDATA[$this->response->setOutput($this->load->view(''common/filemanager.tpl'', $data));]]></search>\r\n        <add  position="replace"><![CDATA[$this->response->setOutput($this->load->view(''common/modalpim.tpl'', $data));]]></add>\r\n    </operation> \r\n-->\r\n  </file>\r\n  <file path="admin/controller/common/footer.php">\r\n   <operation>\r\n     <search><![CDATA[return $this->load->view(''common/footer]]></search>\r\n     <add  position="before"><![CDATA[\r\n              $data[''width''] = 800;\r\n              $data[''height''] = 600;\r\n              $data[''lang''] = ''en'';\r\n              if ($this->config->get(''pim_status'')) {\r\n                $data[''width''] = $this->config->get(''pim_width'');\r\n                $data[''height''] = $this->config->get(''pim_height'');\r\n\r\n                if ($this->config->get(''pim_language'')) {\r\n                $data[''lang''] = $this->config->get(''pim_language'');\r\n                }\r\n              }\r\n          $data[''pim_status''] = $this->config->get(''pim_status'');                         \r\n     ]]></add>\r\n   </operation>    \r\n  </file>  \r\n  <file path="admin/view/template/common/header.tpl">\r\n    <operation>\r\n        <search><![CDATA[<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>]]></search>\r\n        <add position="before"><![CDATA[\r\n          <?php if ($pim_status) {?>\r\n          <!-- Power Image Manager -->\r\n          <link rel="stylesheet" href="view/javascript/jquery/jquery-ui-1.11.4.custom/jquery-ui.css" />\r\n          <script src="view/javascript/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>\r\n          <script type="text/javascript" src="view/javascript/pim/pim.min.js"></script>          \r\n          <link rel="stylesheet" type="text/css" media="screen" href="view/stylesheet/pim/pim.min.css">\r\n          <link rel="stylesheet" type="text/css" media="screen" href="view/stylesheet/pim/theme.css">\r\n            <?php if ($lang) { ?>\r\n             <script type="text/javascript" src="view/javascript/pim/i18n/<?php echo $lang;?>.js"></script>  \r\n            <?php } ?>        	\r\n          <!-- Power Image Manager -->        \r\n          <?php } ?>\r\n        ]]></add>\r\n    </operation> \r\n  </file>  \r\n  <file path="admin/controller/common/filemanager.php">\r\n<!--\r\n    <operation>\r\n        <search><![CDATA[$this->response->setOutput($this->load->view(''common/filemanager.tpl'', $data));]]></search>\r\n        <add  position="replace"><![CDATA[\r\n          $data[''width''] = $this->config->get(''pim_width'');\r\n          $data[''height''] = $this->config->get(''pim_height'');\r\n          $this->response->setOutput($this->load->view(''common/modalpim.tpl'', $data));]]></add>\r\n    </operation> \r\n-->\r\n    <operation>\r\n        <search><![CDATA[public function index() {]]></search>\r\n        <add position="before"><![CDATA[\r\n  public function pim() {\r\n  // Power Image Manager\r\n    if ($this->config->get(''pim_status'')) {\r\n      $this->load->language(''common/filemanager'');\r\n      $data[''heading_title''] = $this->language->get(''heading_title'');\r\n      $this->document->setTitle($this->language->get(''heading_title''));\r\n      $data[''token''] = $this->session->data[''token''];\r\n      $data[''lang''] = ''en'';\r\n      $data[''width''] = $this->config->get(''pim_width'');\r\n      $data[''height''] = $this->config->get(''pim_height'');\r\n      if ($this->config->get(''pim_language'')) {\r\n        $data[''lang''] = $this->config->get(''pim_language'');\r\n      }\r\n      $data[''header''] = $this->load->controller(''common/header'');\r\n      $data[''column_left''] = $this->load->controller(''common/column_left'');\r\n      $data[''footer''] = $this->load->controller(''common/footer'');\r\n      \r\n      \r\n      $this->response->setOutput($this->load->view(''common/pim.tpl'', $data));\r\n      return;\r\n    } else {\r\n      die(''Power Image Manager is not installed. Please go back, install and configure the module in Extension > Modules.'');\r\n    }\r\n   // Power Image Manager    \r\n   }\r\n        ]]></add>\r\n    </operation> \r\n    <operation>\r\n      <search><![CDATA[public function index() {]]></search>\r\n      <add position="after"><![CDATA[\r\n    if ($this->config->get(''pim_status'')) {\r\n      $this->load->language(''common/filemanager'');\r\n      $data[''heading_title''] = $this->language->get(''heading_title'');\r\n      $this->document->setTitle($this->language->get(''heading_title''));\r\n      $data[''token''] = $this->session->data[''token''];\r\n      $data[''lang''] = ''en'';\r\n      $data[''width''] = $this->config->get(''pim_width'');\r\n      $data[''height''] = $this->config->get(''pim_height'');\r\n      if ($this->config->get(''pim_language'')) {\r\n        $data[''lang''] = $this->config->get(''pim_language'');\r\n      }\r\n      $data[''header''] = '''';//$this->load->controller(''common/header'');\r\n      $data[''footer''] = '''';//$this->load->controller(''common/footer'');\r\n      $data[''cke''] = '''';\r\n      $data[''target''] = '''';\r\n      $data[''thumb'']  = '''';\r\n      if (isset($this->request->get[''CKEditor''])) {\r\n        $data[''cke''] = $this->request->get[''CKEditor''];\r\n      }\r\n      if ($this->request->server[''HTTPS'']) {\r\n        $data[''base''] = HTTPS_SERVER;\r\n      } else {\r\n        $data[''base''] = HTTP_SERVER;\r\n      }\r\n      \r\n      if (isset($this->request->get[''target''])) {\r\n        $data[''target''] = $this->request->get[''target''];\r\n      }\r\n      if (isset($this->request->get[''thumb''])) {\r\n        $data[''thumb''] = $this->request->get[''thumb''];\r\n      }\r\n      \r\n      $this->response->setOutput($this->load->view(''common/pim.tpl'', $data));\r\n      return;\r\n    }       \r\n      ]]></add>\r\n    </operation>    \r\n    <operation>\r\n        <search><![CDATA[public function delete() {]]></search>\r\n        <add position="before"><![CDATA[\r\n        // Power Image Manager\r\n    public function connector() {\r\n      include_once DIR_SYSTEM.''library/filemanager/elFinderConnector.class.php'';\r\n      include_once DIR_SYSTEM.''library/filemanager/elFinder.class.php'';\r\n      include_once DIR_SYSTEM.''library/filemanager/elFinderVolumeDriver.class.php'';\r\n      $volumes = $this->config->get(''pim_volumes'');\r\n\r\n      include_once DIR_SYSTEM.''library/filemanager/volumes/LocalFileSystem.class.php'';\r\n      \r\n      $volumes = $this->config->get(''pim_volumes'');\r\n      if (empty($volumes)) { $volumes = array(); }\r\n      foreach ($volumes as $driver => $something) {\r\n        if ($driver == ''LocalFileSystem'') {continue;}\r\n        if (file_exists(DIR_SYSTEM.''library/filemanager/volumes/''.$driver.''.class.php'')) {\r\n          include_once DIR_SYSTEM.''library/filemanager/volumes/''.$driver.''.class.php'';\r\n        }\r\n      }\r\n\r\n      if (isset($this->request->server[''HTTPS'']) && (($this->request->server[''HTTPS''] == ''on'') || ($this->request->server[''HTTPS''] == ''1''))) {\r\n                        $base = HTTPS_CATALOG."image/";\r\n                } else {\r\n                        $base = HTTP_CATALOG."image/";\r\n                }\r\n\r\n       $this->config->set(''config_error_display'', 0);\r\n       $this->config->set(''config_error_log'', 0);\r\n        function access($attr, $path, $data, $volume) {\r\n                return strpos(basename($path), ''.'') === 0       // if file/folder begins with ''.'' (dot)\r\n                        ? !($attr == ''read'' || $attr == ''write'')    // set read+write to false, other (locked+hidden) set to true\r\n                        :  null;                                    // else elFinder decide it itself\r\n        }\r\n        $tmbURL = DIR_IMAGE.''tmb/'';\r\n\r\n        $plugins =  $this->config->get(''pim_modules'');\r\n        $binds = array ();\r\n        $plugin_options = array();\r\n        if (!empty($plugins)) {\r\n          foreach ($plugins as $key=>$val) {\r\n            $binds[''upload.presave''][] =  ''Plugin.''.$key.''.onUpLoadPreSave'';\r\n            $plugin_data =  $this->config->get(''pim_''.$key);\r\n            if (!empty($plugin_data)) {\r\n              foreach ($plugin_data as $data_key=>$data_val){\r\n                  if ($data_key == ''source'') { // watermark source\r\n                    $data_val = DIR_IMAGE.$data_val;\r\n                  }\r\n                  $plugin_options[$key][$data_key] = $data_val;\r\n              }\r\n            }\r\n          }\r\n        }\r\n\r\n        if (empty($volumes)) { // default settings\r\n          $bits = array(\r\n           array(\r\n              ''driver''          => ''LocalFileSystem'',         // driver for accessing file system (REQUIRED)\r\n              ''path''            => DIR_IMAGE.''catalog/'',      // path to files (REQUIRED)\r\n              ''URL''             => $base.''catalog/'',          // URL to files (REQUIRED)\r\n              ''accessControl''   => ''access'',                  // disable and hide dot starting files (OPTIONAL)\r\n              ''fileMode''        => 0644,                       // new files mode\r\n              ''dirMode''         => 0755,                       // new folders mode\r\n              ''uploadAllow''     => array(''image'', ''application/pdf''),\r\n              ''uploadDeny''      => array(''all''),\r\n              ''uploadOrder''     => ''deny,allow'',            	     			        			\r\n              ''tmbBgColor''      => ''transparent'',             // transparent background\r\n              ''tmbCrop''         => ''false'',                   // do not crop\r\n              ''tmbSize''         => ''59'',                      // default tmb size.\r\n              ''copyOverwrite''   => $this->config->get(''pim_copyOverwrite''),\r\n              ''uploadOverwrite'' => $this->config->get(''pim_uploadOverwrite''),\r\n              ''uploadMaxSize''   => ''''.$this->config->get(''pim_uploadMaxSize'').''''.$this->config->get(''pim_uploadMaxType''),\r\n            ),\r\n         );\r\n        } else {\r\n          $bits = array();\r\n          $i=0;\r\n          foreach ($volumes as $driver => $volume) { \r\n            foreach ($volume as $vkey => $vdata) {\r\n              if ($vdata[''status'']) {\r\n                $bits[$i][''driver''] = $driver;\r\n                $bits[$i][''uploadAllow'']     = array(''image'', ''application/pdf'');\r\n                $bits[$i][''uploadDeny'']      = array(''all'');\r\n                $bits[$i][''uploadOrder'']     = ''deny,allow'';                     \r\n                $bits[$i][''accessControl'']   = ''access'';\r\n                $bits[$i][''fileMode'']        = 0644;\r\n                $bits[$i][''dirMode'']         = 0755;\r\n                $bits[$i][''tmpPath'']         = ''.tmp'';\r\n                $bits[$i][''tmbBgColor'']      = ''transparent'';\r\n                $bits[$i][''tmbCrop'']         = ''false'';\r\n                $bits[$i][''tmbSize'']         = ''59'';\r\n                $bits[$i][''copyOverwrite'']   = $this->config->get(''pim_copyOverwrite'');\r\n                $bits[$i][''uploadOverwrite''] = $this->config->get(''pim_uploadOverwrite'');\r\n                $bits[$i][''uploadMaxSize'']   = ''''.$this->config->get(''pim_uploadMaxSize'').''''.$this->config->get(''pim_uploadMaxType'');\r\n               \r\n                foreach ($vdata as $var_key => $var_val) {\r\n\r\n                  $bits[$i][$var_key] = $var_val;\r\n                  if ($driver == ''FTP'') { \r\n                       $bits[$i][''tmbPath'']      = ''tmb'';\r\n                       $bits[$i][''tmbURL'']       = dirname($_SERVER[''PHP_SELF'']).''/tmb'';\r\n                       $bits[$i][''tmpPath'']      = ''/tmp'';                 \r\n                  }\r\n                }     \r\n                $i++;\r\n              }\r\n            }   \r\n          }          \r\n        }\r\n\r\n        $roots[''roots''] = $bits;\r\n        $opts = $roots;\r\n\r\n\r\n        if (!empty($binds)) {\r\n          $opts[''bind''] = $binds;\r\n        }\r\n        if (!empty($plugin_options)) {\r\n          $opts[''plugin''] = $plugin_options;\r\n        }\r\n\r\n        $connector = new elFinderConnector(new elFinder($opts));\r\n        $connector->run();\r\n    }\r\n        // Power Image Manager\r\n        ]]></add>\r\n    </operation>\r\n  </file>\r\n<file path="admin/controller/catalog/product.php">\r\n  \r\n    <operation>\r\n        <search><![CDATA[private $error = array();]]></search>\r\n        <add position="after"><![CDATA[\r\n  // multi image uploader\r\n        public function pimage() {\r\n          $this->load->model(''tool/image'');\r\n          $json = array();\r\n          if (isset($this->request->get[''image''])) {\r\n            $this->request->get[''image''] = urldecode($this->request->get[''image'']);\r\n          } else {\r\n            return;\r\n          }\r\n      		if (!is_file(DIR_IMAGE . $this->request->get[''image''])) {\r\n      			if (is_file(DIR_IMAGE ."catalog/". $this->request->get[''image''])) {\r\n        			$this->request->get[''image''] = ''catalog/''.$this->request->get[''image''];\r\n        		} else {\r\n          		return;\r\n        		}\r\n      		}                \r\n          \r\n          if (isset($this->request->get[''image''])) {\r\n              $json[''abs'']  = $this->request->get[''image''];\r\n              $json[''file''] = $this->model_tool_image->resize(html_entity_decode($this->request->get[''image''], ENT_QUOTES, ''UTF-8''), 100, 100);\r\n\r\n          		$this->response->addHeader(''Content-Type: application/json'');\r\n              $this->response->setOutput(json_encode($json));\r\n          }\r\n        }\r\n        // multi image uploader\r\n\r\n        ]]></add>\r\n    </operation>\r\n    <operation>\r\n        <search><![CDATA[function getForm() {]]></search>\r\n        <add  position="after"><![CDATA[\r\n          $data[''pim_miu''] = false;\r\n          $data[''pim_status''] = false;\r\n          $data[''pim_miu'']  = $this->config->get(''pim_miu'');\r\n          $data[''pim_status''] = $this->config->get(''pim_status'');\r\n        ]]></add>\r\n    </operation>\r\n  </file>\r\n  <file path="admin/view/template/catalog/product_form.tpl">\r\n        <operation>\r\n            <search><![CDATA[<td class="text-right"><?php echo $entry_sort_order; ?></td>]]></search>\r\n            <add position="before"><![CDATA[\r\n            <?php if ($pim_status && !$pim_miu) {?>\r\n            <td class="text-left"><?php echo ''Default Image''; ?></td>\r\n      <?php } ?>\r\n            ]]></add>\r\n        </operation>\r\n        <operation>\r\n      <search>\r\n        <![CDATA[<td class="text-right"><input type="text" name="product_image[<?php echo $image_row; ?>][sort_order]"]]>\r\n      </search>\r\n      <add position="before">\r\n        <![CDATA[\r\n        <?php if ($pim_status && !$pim_miu) {?>\r\n          <td class="text-right"><input type="radio" name="def_img" value="<?php  if (isset($product_image[''image''])) { echo $product_image[''image'']; } ?>" class="form-control" /></td>\r\n        <?php } ?>\r\n        ]]>\r\n      </add>\r\n    </operation>\r\n    <operation>\r\n        <search><![CDATA[html += ''  <td class="text-right"><input type="text" name="product_image['' + image_row + ''][sort_order]"]]></search>\r\n        <add position="before"><![CDATA[\r\n         <?php if ($pim_status && !$pim_miu) {?>\r\n          html += ''<td class="text-right"><input type="radio" name="def_img" id="radio-input-image'' + image_row + ''" value="" disabled="disabled" class="form-control"></td>'';   \r\n         <?php } ?>\r\n        ]]></add>\r\n    </operation>\r\n    <operation>\r\n        <search><![CDATA[<?php echo $footer; ?> ]]></search>\r\n        <add position="before"><![CDATA[\r\n        <?php if ($pim_status) {?>\r\n        <script type="text/javascript">\r\n            if (typeof addMultiImage != ''function'') {\r\n\r\n              function addMultiImage(img) {\r\n                    if (image_row>0) {\r\n                        var k = (image_row-1);\r\n\r\n                        if ($(''#input-image''+k).val() == "") {\r\n                            $(''#image-row''+k).remove();\r\n                        }\r\n                    }\r\n                      $.ajax({\r\n                        url: ''index.php?route=catalog/product/pimage&token=<?php echo $token; ?>&image='' + encodeURIComponent(img),\r\n                        dataType: ''json'',\r\n                        success: function(json) {\r\n\r\n                          if (json.file) {\r\n                          html  = ''<tr id="image-row'' + image_row + ''">'';\r\n                          html += ''  <td class="text-left"><a href="" id="thumb-image'' + image_row + ''"data-toggle="image" class="img-thumbnail">'';\r\n                          html += ''<img src="''+json.file+''" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />'';\r\n                          html+=''<input type="hidden" name="product_image['' + image_row + ''][image]" value="'' + decodeURIComponent(json.abs) + ''" id="input-image'' + image_row + ''" /></td>'';\r\n                          html += ''<td class="text-right"><input type="radio" name="def_img" value="''+json.abs+''" class="form-control"></td>'';\r\n  \r\n                          html += ''  <td class="text-right"><input type="text" name="product_image['' + image_row + ''][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>'';\r\n                          html += ''  <td class="text-left"><button type="button" onclick="$(\\''#image-row'' + image_row  + ''\\'').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>'';\r\n                          html += ''</tr>'';\r\n  \r\n  \r\n                          $(''#images tbody'').append(html);\r\n                          image_row++;\r\n                        }\r\n                      }\r\n                  });\r\n              }\r\n            }\r\n        </script>\r\n      <?php } ?>        \r\n        ]]></add>\r\n    </operation>\r\n    <operation>\r\n        <search><![CDATA[<button type="button" onclick="addImage();"]]></search>\r\n        <add  position="replace"><![CDATA[<?php if ($pim_status){?><a type="button" data-toggle="manager" title="<?php echo ''Power Image Manager''; ?>" class="btn btn-success"><i class="fa fa-upload"></i> <?php echo ''Power Image Manager''; ?></a>&nbsp;<?php } ?><button type="button" onclick="addImage();"]]></add>\r\n    </operation>\r\n  </file>\r\n\r\n  <file path="admin/view/template/common/footer.tpl">\r\n    <operation>\r\n        <search><![CDATA[<footer id="footer">]]></search>\r\n        <add position="before"><![CDATA[\r\n          <?php if ($pim_status) {?>\r\n<script type="text/javascript">\r\n// Power Image Manager\r\n$(document).ready(function() {\r\n  $(document).undelegate(''a[data-toggle=\\''image\\'']'', ''click'');\r\n  // Power Image Manager\r\n  $(document).delegate(''a[data-toggle=\\''image\\'']'', ''click'', function(e) {\r\n    e.preventDefault();    \r\n    var element = this;\r\n    $(element).popover({\r\n      html: true,\r\n      placement: ''right'',\r\n      trigger: ''manual'',\r\n      content: function() {\r\n        return ''<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>'';\r\n      }\r\n    });\r\n    location.hash = ''pim'';\r\n    $(element).popover(''toggle'');\r\n\r\n    $(''#button-image'').on(''click'', function() {\r\n      $(element).popover(''hide'');\r\n      var target = $(element).parent().find(''input'').attr(''id'');\r\n      var thumb = $(element).attr(''id'');\r\n      var fm = $(''<div/>'').dialogelfinder({\r\n        url : ''index.php?route=common/filemanager/connector&token='' + getURLVar(''token''),\r\n        lang : ''<?php echo $lang;?>'',\r\n        width : <?php echo $width;?>,\r\n        height: <?php echo $height;?>,\r\n        destroyOnClose : true,\r\n        \r\n        uiOptions : {toolbar : [[''home'', ''back'', ''forward''],[''reload''],[''mkdir'', ''upload''],[''open'', ''download'', ''getfile''],[''info''],[''quicklook''],[''copy'', ''cut'', ''paste''],[''rm''],[''duplicate'', ''rename'', ''edit'', ''resize''],[''extract'', ''archive'',''multiupload''],[''search''],[''view''],[''help'']]},\r\n  \r\n        contextmenu: {navbar: ["open", "|", "copy", "cut", "paste", "duplicate", "|", "rm", "|", "info"],cwd: ["reload", "back", "|", "upload", "mkdir", "mkfile", "paste", "|", "sort", "|", "info"],files: ["getfile", "|", "open", "quicklook", "|", "download", "|", "copy", "cut", "paste", "duplicate", "|", "rm", "|", "edit", "rename", "resize", "|", "archive","multiupload", "extract", "|", "info"]},\r\n        \r\n        getFileCallback : function(files, fm) {\r\n          a = files.url;\r\n\r\n					b = a.replace(''<?php echo HTTPS_CATALOG."image/";?>'','''');	\r\n					b = b.replace(''<?php echo HTTP_CATALOG."image/";?>'','''');	\r\n          \r\n          \r\n          $(''#''+thumb).find(''img'').attr(''src'', files.tmb);\r\n          $(''#''+target).val(decodeURIComponent(b));\r\n          $(''#radio-''+target).removeAttr(''disabled'');\r\n          $(''#radio-''+target).val(b);\r\n        },\r\n        commandsOptions : {\r\n          getfile : {\r\n            oncomplete : ''close'',\r\n          }\r\n        }\r\n      }).dialogelfinder(''instance'');\r\n      return;\r\n    });\r\n\r\n    $(''#button-clear'').on(''click'', function() {\r\n      $(element).find(''img'').attr(''src'', $(element).find(''img'').attr(''data-placeholder''));\r\n      $(element).parent().find(''input'').attr(''value'', '''');\r\n      $(element).popover(''hide'');\r\n    });\r\n  });\r\n\r\n  $(document).delegate(''a[data-toggle=\\''manager\\'']'', ''click'', function(e) {\r\n    e.preventDefault();\r\n    var fm = $(''<div/>'').dialogelfinder({\r\n      url : ''index.php?route=common/filemanager/connector&token='' + getURLVar(''token''),\r\n      lang : ''<?php echo $lang;?>'',\r\n      width : <?php echo $width;?>,\r\n      height: <?php echo $height;?>,\r\n      destroyOnClose : true,\r\n      \r\n      uiOptions : {toolbar : [[''home'', ''back'', ''forward''],[''reload''],[''mkdir'', ''upload''],[''open'', ''download'', ''getfile''],[''info''],[''quicklook''],[''copy'', ''cut'', ''paste''],[''rm''],[''duplicate'', ''rename'', ''edit'', ''resize''],[''extract'', ''archive'',''multiupload'', ''sort''],[''search''],[''view''],[''help'']]},\r\n\r\n      contextmenu: {navbar: ["open", "|", "copy", "cut", "paste", "duplicate", "|", "rm", "|", "info"],cwd: ["reload", "back", "|", "upload", "mkdir", "mkfile", "paste", "|", "sort", "|", "info"],files: ["getfile", "|", "open", "quicklook", "|", "download", "|", "copy", "cut", "paste", "duplicate", "|", "rm", "|", "edit", "rename", "resize", "|", "archive","multiupload", "extract", "|", "info"]},\r\n      \r\n      getFileCallback : function(files, fm) {\r\n        a = files.url;\r\n					b = a.replace(''<?php echo HTTPS_CATALOG."image/";?>'','''');	\r\n					b = b.replace(''<?php echo HTTP_CATALOG."image/";?>'','''');	\r\n        addMultiImage(decodeURIComponent(b));\r\n      },\r\n      commandsOptions : {\r\n        getfile : {\r\n          oncomplete : ''close'',\r\n          folders : false\r\n        }\r\n      }\r\n    }).dialogelfinder(''instance'');\r\n  });\r\n\r\n $(document).undelegate(''button[data-toggle=\\''image\\'']'', ''click'');\r\n \r\n    $(document).delegate(''button[data-toggle=\\''image\\'']'', ''click'', function() {\r\n      location.hash = '''';\r\n      var fm = $(''<div/>'').dialogelfinder({\r\n        url : ''index.php?route=common/filemanager/connector&token='' + getURLVar(''token''),\r\n        lang : ''<?php echo $lang;?>'',\r\n        width : <?php echo $width;?>,\r\n        height: <?php echo $height;?>,\r\n        destroyOnClose : true,\r\n        getFileCallback : function(files, fm) {\r\n          var range, sel = window.getSelection();  \r\n          if (sel.rangeCount) {\r\n            var img = document.createElement(''img'');\r\n            a = files.url;\r\n            b = a.replace(files.baseUrl,'''');\r\n            img.src = files.baseUrl+''''+b;\r\n            range = sel.getRangeAt(0);\r\n            range.insertNode(img);\r\n          }\r\n        },\r\n        commandsOptions : {\r\n          getfile : {\r\n            oncomplete : ''close'',\r\n            folders : false\r\n          }\r\n        }\r\n      }).dialogelfinder(''instance'');\r\n    });\r\n});\r\n// Power Image Manager\r\n<?php } ?>\r\n</script>\r\n        ]]></add>\r\n    </operation>\r\n  </file>\r\n  <file path="admin/model/catalog/product.php">\r\n    <operation>\r\n      <search index="0"><![CDATA[foreach ($data[''product_description''] as $language_id => $value) {]]></search>\r\n      <add position="before"><![CDATA[\r\n        if (isset($data[''def_img'']) && $data[''def_img''] != "" && !$this->config->get(''pim_miu'')) {\r\n           $q="UPDATE " . DB_PREFIX . "product SET image = ''" . $this->db->escape($data[''def_img'']) . "'' WHERE product_id = ''" . (int)$product_id . "''";\r\n           $this->db->query($q);\r\n        }\r\n        ]]>\r\n      </add>\r\n    </operation>\r\n    <operation>\r\n      <search index="1"><![CDATA[foreach ($data[''product_description''] as $language_id => $value) {]]></search>\r\n      <add position="before"><![CDATA[\r\n        if (isset($data[''def_img'']) && $data[''def_img''] != "" && !$this->config->get(''pim_miu'')) {\r\n          $q="UPDATE " . DB_PREFIX . "product SET image = ''" . $this->db->escape($data[''def_img'']) . "'' WHERE product_id = ''" . (int)$product_id . "''";\r\n          $this->db->query($q);\r\n        }\r\n        ]]>\r\n      </add>\r\n    </operation>\r\n    <operation>\r\n        <search index="0"><![CDATA[foreach ($data[''product_image''] as $product_image) {]]></search>\r\n        <add  position="after"><![CDATA[\r\n              if ($this->config->get(''pim_deletedef'') && isset($data[''def_img'']) && $data[''def_img''] == $product_image[''image'']) { continue;}\r\n        ]]></add>\r\n    </operation>\r\n    <operation>\r\n        <search index="1"><![CDATA[$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = ''" . (int)$product_id . "''");]]></search>\r\n        <add  position="before"><![CDATA[\r\n              if ($this->config->get(''pim_deletedef'') && isset($data[''def_img''])) {\r\n                $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = ''" . (int)$product_id . "'' AND image = ''" . $this->db->escape($data[''image'']) . "''");\r\n              }\r\n        ]]></add>\r\n    </operation>\r\n  </file>\r\n  <file path="admin/controller/common/menu.php">\r\n    <operation>\r\n      <search><![CDATA[return $this->load->view(''common/menu]]></search>\r\n      <add position="before"><![CDATA[\r\n        $data[''text_pim''] = $this->language->get(''text_pim'');\r\n        $data[''pim_status''] = $this->config->get(''pim_status'');\r\n        $data[''pim''] = $this->url->link(''common/filemanager/pim'', ''token='' . $this->session->data[''token''], ''SSL'');           \r\n      ]]></add>\r\n    </operation>\r\n  </file>     \r\n  <file path="admin/view/template/common/menu.tpl">\r\n    <operation>\r\n      <search><![CDATA[<li><a href="<?php echo $information; ?>"><?php echo $text_information; ?></a></li>]]></search>\r\n      <add position="after"><![CDATA[\r\n        <?php if (isset($pim_status) && $pim_status){?><li><a href="<?php echo $pim; ?>"><?php echo $text_pim; ?></a></li> <?php } ?>\r\n      ]]></add>\r\n    </operation>\r\n  </file>  \r\n  <file path="admin/language/*/common/menu.php">\r\n    <operation>\r\n      <search><![CDATA[<?php]]></search>\r\n      <add position="after"><![CDATA[\r\n       $_[''text_pim'']   = ''Power Image Manager'';\r\n      ]]></add>\r\n    </operation>\r\n    </file>       \r\n</modification> ', 1, '2017-08-23 07:31:42');

-- --------------------------------------------------------

--
-- Table structure for table `oc_module`
--

CREATE TABLE IF NOT EXISTS `oc_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `oc_module`
--

INSERT INTO `oc_module` (`module_id`, `name`, `code`, `setting`) VALUES
(47, 'Верхний правый банер 1', 'html', '{"name":"\\u0412\\u0435\\u0440\\u0445\\u043d\\u0438\\u0439 \\u043f\\u0440\\u0430\\u0432\\u044b\\u0439 \\u0431\\u0430\\u043d\\u0435\\u0440 1","module_description":{"1":{"title":"","description":"&lt;div style=&quot;margin-bottom: 16px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image\\/catalog\\/MainSlider\\/right1.png&quot; class=&quot;hidden-xs&quot;&gt;&lt;\\/div&gt;"},"2":{"title":"","description":"&lt;div style=&quot;margin-bottom: 16px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image\\/catalog\\/MainSlider\\/right1.png&quot; class=&quot;hidden-xs&quot;&gt;&lt;\\/div&gt;"}},"status":"1"}'),
(48, 'Верхний правый банер 1', 'html', '{"name":"\\u0412\\u0435\\u0440\\u0445\\u043d\\u0438\\u0439 \\u043f\\u0440\\u0430\\u0432\\u044b\\u0439 \\u0431\\u0430\\u043d\\u0435\\u0440 1","module_description":{"1":{"title":"","description":"&lt;div style=&quot;margin-bottom: 16px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image\\/catalog\\/MainSlider\\/right2.png&quot; class=&quot;hidden-xs&quot;&gt;&lt;\\/div&gt;"},"2":{"title":"","description":"&lt;div style=&quot;margin-bottom: 16px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image\\/catalog\\/MainSlider\\/right2.png&quot; class=&quot;hidden-xs&quot;&gt;&lt;\\/div&gt;"}},"status":"1"}'),
(49, 'Левый банер ', 'html', '{"name":"\\u041b\\u0435\\u0432\\u044b\\u0439 \\u0431\\u0430\\u043d\\u0435\\u0440 ","module_description":{"1":{"title":"","description":"&lt;img alt=&quot;&quot; src=&quot;image\\/catalog\\/MainSlider\\/BOT1.png&quot; class=&quot;hidden-xs&quot;&gt;"},"2":{"title":"","description":"&lt;img alt=&quot;&quot; src=&quot;image\\/catalog\\/MainSlider\\/BOT1.png&quot; class=&quot;hidden-xs&quot;&gt;"}},"status":"1"}'),
(50, 'Нижний баннер рекламный', 'html', '{"name":"\\u041d\\u0438\\u0436\\u043d\\u0438\\u0439 \\u0431\\u0430\\u043d\\u043d\\u0435\\u0440 \\u0440\\u0435\\u043a\\u043b\\u0430\\u043c\\u043d\\u044b\\u0439","module_description":{"1":{"title":"","description":"&lt;div style=&quot;margin-bottom: 20px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image\\/catalog\\/MainSlider\\/bot2.png&quot;&gt;&lt;\\/div&gt;"},"2":{"title":"","description":"&lt;div style=&quot;margin-bottom: 20px;&quot;&gt;&lt;img alt=&quot;&quot; src=&quot;image\\/catalog\\/MainSlider\\/bot2.png&quot;&gt;&lt;\\/div&gt;"}},"status":"1"}'),
(51, 'Главная страница преимущества ', 'html', '{"name":"\\u0413\\u043b\\u0430\\u0432\\u043d\\u0430\\u044f \\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0446\\u0430 \\u043f\\u0440\\u0435\\u0438\\u043c\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u0430 ","module_description":{"1":{"title":"","description":"&lt;div style=&quot;background:#f8f8f8;overflow: auto; padding: 30px;&quot;&gt;&lt;div class=&quot;col-lg-3 col-md-3 col-sm-3 col-xs-12&quot; style=&quot;text-align: center; margin-top: 10px; margin-bottom: 10px;&quot;&gt;&lt;i class=&quot;fa fa-check-square-o&quot; style=&quot;font-size: 30px;margin-right: 20px;color:#000000; vertical-align: middle;&quot;&gt;&lt;\\/i&gt;&lt;span style=&quot;color:#000000; font-family: Roboto Condensed,sans-serif; font-weight: normal;text-transform:uppercase;&quot;&gt;\\u0422\\u043e\\u0432\\u0430\\u0440 \\u043f\\u0440\\u0435\\u043c\\u0438\\u0443\\u043c \\u043a\\u043b\\u0430\\u0441\\u0441\\u0430&lt;\\/span&gt;&lt;\\/div&gt;&lt;div class=&quot;col-lg-3 col-md-3 col-sm-3 col-xs-12&quot; style=&quot;text-align: center; margin-top: 10px; margin-bottom: 10px;&quot;&gt;&lt;i class=&quot;fa fa-truck&quot; style=&quot;font-size: 30px;margin-right: 20px; color:#000000; vertical-align: middle;&quot;&gt;&lt;\\/i&gt;&lt;span style=&quot;color:#000000; font-family: Roboto Condensed,sans-serif; font-weight: normal;text-transform:uppercase;&quot;&gt;\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043f\\u043e \\u043c\\u0438\\u0440\\u0443&lt;\\/span&gt;&lt;\\/div&gt;&lt;div class=&quot;col-lg-3 col-md-3 col-sm-3 col-xs-12&quot; style=&quot;text-align: center; margin-top: 10px; margin-bottom: 10px;&quot;&gt;&lt;i class=&quot;fa fa-user&quot; style=&quot;font-size: 30px;margin-right: 20px; color:#000000; vertical-align: middle;&quot;&gt;&lt;\\/i&gt;&lt;span style=&quot;color:#000000; font-family: Roboto Condensed,sans-serif; font-weight: normal;text-transform:uppercase;&quot;&gt;\\u0441\\u0435\\u0437\\u043e\\u043d\\u043d\\u044b\\u0435 \\u0441\\u043a\\u0438\\u0434\\u043a\\u0438&lt;\\/span&gt;&lt;\\/div&gt;&lt;div class=&quot;col-lg-3 col-md-3 col-sm-3 col-xs-12&quot; style=&quot;text-align: center; margin-top: 10px; margin-bottom: 10px;&quot;&gt;&lt;i class=&quot;fa fa-gift&quot; style=&quot;font-size: 30px;margin-right: 20px; color:#000000; vertical-align: middle;&quot;&gt;&lt;\\/i&gt;&lt;span style=&quot;color:#000000; font-family: Roboto Condensed,sans-serif; font-weight: normal;text-transform:uppercase;&quot;&gt;\\u043f\\u043e\\u0434\\u0430\\u0440\\u043e\\u0447\\u043d\\u044b\\u0435 \\u043a\\u0430\\u0440\\u0442\\u044b&lt;\\/span&gt;&lt;\\/div&gt;&lt;\\/div&gt;"},"2":{"title":"","description":"&lt;div style=&quot;background:#f8f8f8;overflow: auto; padding: 30px;&quot;&gt;&lt;div class=&quot;col-lg-3 col-md-3 col-sm-3 col-xs-12&quot; style=&quot;text-align: center; margin-top: 10px; margin-bottom: 10px;&quot;&gt;&lt;i class=&quot;fa fa-check-square-o&quot; style=&quot;font-size: 30px;margin-right: 20px;color:#000000; vertical-align: middle;&quot;&gt;&lt;\\/i&gt;&lt;span style=&quot;color:#000000; font-family: Roboto Condensed,sans-serif; font-weight: normal;text-transform:uppercase;&quot;&gt;Luxury&lt;\\/span&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;col-lg-3 col-md-3 col-sm-3 col-xs-12&quot; style=&quot;text-align: center; margin-top: 10px; margin-bottom: 10px;&quot;&gt;&lt;i class=&quot;fa fa-truck&quot; style=&quot;font-size: 30px;margin-right: 20px; color:#000000; vertical-align: middle;&quot;&gt;&lt;\\/i&gt;&lt;span style=&quot;color:#000000; font-family: Roboto Condensed,sans-serif; font-weight: normal;text-transform:uppercase;&quot;&gt;DELIVERY OPTIONS&lt;\\/span&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;col-lg-3 col-md-3 col-sm-3 col-xs-12&quot; style=&quot;text-align: center; margin-top: 10px; margin-bottom: 10px;&quot;&gt;&lt;i class=&quot;fa fa-user&quot; style=&quot;font-size: 30px;margin-right: 20px; color:#000000; vertical-align: middle;&quot;&gt;&lt;\\/i&gt;&lt;span style=&quot;color:#000000; font-family: Roboto Condensed,sans-serif; font-weight: normal;text-transform:uppercase;&quot;&gt;SALES&lt;\\/span&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;col-lg-3 col-md-3 col-sm-3 col-xs-12&quot; style=&quot;text-align: center; margin-top: 10px; margin-bottom: 10px;&quot;&gt;&lt;i class=&quot;fa fa-gift&quot; style=&quot;font-size: 30px;margin-right: 20px; color:#000000; vertical-align: middle;&quot;&gt;&lt;\\/i&gt;&lt;span style=&quot;color:#000000; font-family: Roboto Condensed,sans-serif; font-weight: normal;text-transform:uppercase;&quot;&gt;GIFT CARDS&lt;\\/span&gt;&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;"}},"status":"1"}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option`
--

CREATE TABLE IF NOT EXISTS `oc_option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(5, 'select', 4),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 10),
(12, 'date', 11);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_description`
--

CREATE TABLE IF NOT EXISTS `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`option_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Переключатель'),
(4, 1, 'Текст'),
(6, 1, 'Текстовая область'),
(8, 1, 'Дата'),
(7, 1, 'Файл'),
(5, 1, 'Список'),
(9, 1, 'Время'),
(10, 1, 'Дата и время'),
(12, 1, 'Дата доставки'),
(11, 1, 'Размер'),
(2, 1, 'Флажок'),
(8, 2, 'Date'),
(10, 2, 'Date &amp; Time'),
(12, 2, 'Delivery Date'),
(7, 2, 'File'),
(1, 2, 'Radio'),
(5, 2, 'Select'),
(11, 2, 'Size'),
(4, 2, 'Text'),
(6, 2, 'Textarea'),
(9, 2, 'Time'),
(2, 2, 'Checkbox');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value`
--

CREATE TABLE IF NOT EXISTS `oc_option_value` (
  `option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`option_value_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(46, 11, '', 1),
(47, 11, '', 2),
(48, 11, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value_description`
--

CREATE TABLE IF NOT EXISTS `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`option_value_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(32, 2, 1, 'Small'),
(44, 1, 2, 'Флажок 3'),
(32, 1, 1, 'Маленький'),
(40, 1, 5, 'Голубой'),
(39, 2, 5, 'Red'),
(39, 1, 5, 'Красный'),
(24, 2, 2, 'Checkbox 2'),
(46, 2, 11, 'Small'),
(46, 1, 11, 'Маленький'),
(24, 1, 2, 'Флажок 2'),
(23, 2, 2, 'Checkbox 1'),
(23, 1, 2, 'Флажок 1'),
(31, 1, 1, 'Средний'),
(31, 2, 1, 'Medium'),
(43, 1, 1, 'Большой'),
(43, 2, 1, 'Large'),
(40, 2, 5, 'Blue'),
(41, 1, 5, 'Зеленый'),
(41, 2, 5, 'Green'),
(42, 1, 5, 'Желтый'),
(42, 2, 5, 'Yellow'),
(47, 1, 11, 'Средний'),
(47, 2, 11, 'Medium'),
(48, 1, 11, 'Большой'),
(48, 2, 11, 'Large'),
(44, 2, 2, 'Checkbox 3'),
(45, 1, 2, 'Флажок 4'),
(45, 2, 2, 'Checkbox 4');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order`
--

CREATE TABLE IF NOT EXISTS `oc_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(40) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `oc_order`
--

INSERT INTO `oc_order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `custom_field`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_custom_field`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_custom_field`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `marketing_id`, `tracking`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(4, 0, 'INV-2013-00', 0, 'Luxossorize', 'http://luxossorize.com/', 0, 1, 'Yerzhan', '', 'erzhan.kst@gmail.com', '87752963994', '', '[]', 'Yerzhan', '', '', '', '', '', '', 'Казахстан', 109, 'Алматы - город республ-го значения', 1717, '', '[]', 'Банковский перевод', 'bank_transfer', 'Yerzhan', '', '', '', '', '', '', 'Казахстан', 109, 'Алматы - город республ-го значения', 1717, '', '[]', 'Доставка с фиксированной стоимостью', 'flat.flat', '', '51.0000', 0, 0, '0.0000', 0, '', 1, 2, 'USD', '1.00000000', '206.123.143.195', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36', 'en-US,en;q=0.8,ru;q=0.6', '2017-08-23 07:12:40', '2017-08-23 07:12:40'),
(22, 0, 'INV-2013-00', 0, 'Luxossorize', 'http://luxossorize.com/', 0, 1, 'Евгений', '', 'Gasam@mail.ru', '+79262315467', '', '[]', 'Евгений', '', '', '', '', '', '', 'Казахстан', 109, 'Алматы - город республ-го значения', 1717, '', '[]', 'Банковский перевод', 'bank_transfer', 'Евгений', '', '', '', '', '', '', 'Казахстан', 109, 'Алматы - город республ-го значения', 1717, '', '[]', 'Доставка с фиксированной стоимостью', 'flat.flat', '', '1920.0000', 16, 0, '0.0000', 0, '', 1, 2, 'USD', '1.00000000', '2.73.6.198', '', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0 Mobile/14C92 Safari/602.1', 'ru', '2017-08-25 09:16:54', '2017-08-25 09:17:10'),
(16, 0, 'INV-2013-00', 0, 'Luxossorize', 'http://luxossorize.com/', 0, 1, 'Yerzhan', '', 'erzhan.kst@gmail.com', '+77752963994', '', '[]', 'Yerzhan', '', '', '', '', '', '', 'Казахстан', 109, 'Алматы - город республ-го значения', 1717, '', '[]', 'OKPAY', 'okpay', 'Yerzhan', '', '', '', '', '', '', 'Казахстан', 109, 'Алматы - город республ-го значения', 1717, '', '[]', 'Самовывоз из магазина', 'pickup.pickup', '', '1.0000', 0, 0, '0.0000', 0, '', 1, 2, 'USD', '1.00000000', '206.123.143.195', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36', 'en-US,en;q=0.8,ru;q=0.6', '2017-08-23 16:46:41', '2017-08-23 16:46:41'),
(17, 0, 'INV-2013-00', 0, 'Luxossorize', 'http://luxossorize.com/', 0, 1, 'Yerzhan', '', 'erzhan.kst@gmail.com', '+77752963994', '', '[]', 'Yerzhan', '', '', '', '', '', '', 'Казахстан', 109, 'Алматы - город республ-го значения', 1717, '', '[]', 'OKPAY', 'okpay', 'Yerzhan', '', '', '', '', '', '', 'Казахстан', 109, 'Алматы - город республ-го значения', 1717, '', '[]', 'Самовывоз из магазина', 'pickup.pickup', '', '1.0000', 0, 0, '0.0000', 0, '', 1, 2, 'USD', '1.00000000', '5.76.247.22', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36', 'en-US,en;q=0.8,ru;q=0.6', '2017-08-23 16:48:18', '2017-08-23 16:48:18'),
(18, 0, 'INV-2013-00', 0, 'Luxossorize', 'http://luxossorize.com/', 0, 1, 'Yerzhan', '', 'erzhan.kst@gmail.com', '+77752963994', '', '[]', 'Yerzhan', '', '', '', '', '', '', 'Казахстан', 109, 'Алматы - город республ-го значения', 1717, '', '[]', 'OKPAY', 'okpay', 'Yerzhan', '', '', '', '', '', '', 'Казахстан', 109, 'Алматы - город республ-го значения', 1717, '', '[]', 'Самовывоз из магазина', 'pickup.pickup', '', '1.0000', 0, 0, '0.0000', 0, '', 1, 2, 'USD', '1.00000000', '5.76.247.22', '', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36', 'en-US,en;q=0.8,ru;q=0.6', '2017-08-23 16:54:07', '2017-08-23 16:54:07');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_custom_field`
--

CREATE TABLE IF NOT EXISTS `oc_order_custom_field` (
  `order_custom_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `custom_field_value_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL,
  `location` varchar(16) NOT NULL,
  PRIMARY KEY (`order_custom_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_history`
--

CREATE TABLE IF NOT EXISTS `oc_order_history` (
  `order_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_history_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `oc_order_history`
--

INSERT INTO `oc_order_history` (`order_history_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_added`) VALUES
(37, 22, 16, 1, 'Инструкции по банковскому переводу\n\nтест\n\nЗаказ не будет обработан, пока деньги не поступят на наш расчетный счёт.', '2017-08-25 09:17:10');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_option`
--

CREATE TABLE IF NOT EXISTS `oc_order_option` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`order_option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_product`
--

CREATE TABLE IF NOT EXISTS `oc_order_product` (
  `order_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL,
  PRIMARY KEY (`order_product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `oc_order_product`
--

INSERT INTO `oc_order_product` (`order_product_id`, `order_id`, `product_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`) VALUES
(6, 4, 60, 'Rolex Day-Date', 'Rolex Day-Date', 1, '1.0000', '1.0000', '0.0000', 0),
(24, 22, 58, 'Zenith Pilot', 'Zenith Pilot', 2, '950.0000', '1900.0000', '0.0000', 0),
(18, 16, 60, 'Rolex Day-Date', 'Rolex Day-Date', 1, '1.0000', '1.0000', '0.0000', 0),
(19, 17, 60, 'Rolex Day-Date', 'Rolex Day-Date', 1, '1.0000', '1.0000', '0.0000', 0),
(20, 18, 60, 'Rolex Day-Date', 'Rolex Day-Date', 1, '1.0000', '1.0000', '0.0000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring`
--

CREATE TABLE IF NOT EXISTS `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_recurring_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring_transaction`
--

CREATE TABLE IF NOT EXISTS `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_recurring_transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_status`
--

CREATE TABLE IF NOT EXISTS `oc_order_status` (
  `order_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`order_status_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 1, 'В обработке'),
(3, 1, 'Доставлено'),
(7, 1, 'Отменено'),
(5, 1, 'Сделка завершена'),
(8, 1, 'Возврат'),
(9, 1, 'Отмена и аннулирование'),
(10, 1, 'Неудавшийся'),
(11, 1, 'Возмещенный'),
(12, 1, 'Полностью измененный'),
(13, 1, 'Полный возврат'),
(1, 1, 'Ожидание'),
(15, 1, 'Обработано'),
(14, 1, 'Истекло'),
(2, 2, 'Processing'),
(8, 2, 'Denied'),
(11, 2, 'Refunded'),
(3, 2, 'Shipped'),
(10, 2, 'Failed'),
(1, 2, 'Pending'),
(9, 2, 'Canceled Reversal'),
(7, 2, 'Canceled'),
(12, 2, 'Reversed'),
(13, 2, 'Chargeback'),
(5, 2, 'Complete'),
(14, 2, 'Expired'),
(16, 1, 'Анулированный'),
(16, 2, 'Voided'),
(15, 2, 'Processed');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_total`
--

CREATE TABLE IF NOT EXISTS `oc_order_total` (
  `order_total_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL,
  PRIMARY KEY (`order_total_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `oc_order_total`
--

INSERT INTO `oc_order_total` (`order_total_id`, `order_id`, `code`, `title`, `value`, `sort_order`) VALUES
(16, 4, 'sub_total', 'Итого', '1.0000', 1),
(17, 4, 'shipping', 'Доставка с фиксированной стоимостью', '50.0000', 3),
(18, 4, 'total', 'Всего', '51.0000', 9),
(72, 22, 'total', 'Всего', '1920.0000', 9),
(71, 22, 'shipping', 'Доставка с фиксированной стоимостью', '20.0000', 3),
(70, 22, 'sub_total', 'Итого', '1900.0000', 1),
(52, 16, 'sub_total', 'Итого', '1.0000', 1),
(53, 16, 'shipping', 'Самовывоз из магазина', '0.0000', 3),
(54, 16, 'total', 'Всего', '1.0000', 9),
(55, 17, 'sub_total', 'Итого', '1.0000', 1),
(56, 17, 'shipping', 'Самовывоз из магазина', '0.0000', 3),
(57, 17, 'total', 'Всего', '1.0000', 9),
(58, 18, 'sub_total', 'Итого', '1.0000', 1),
(59, 18, 'shipping', 'Самовывоз из магазина', '0.0000', 3),
(60, 18, 'total', 'Всего', '1.0000', 9);

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_voucher`
--

CREATE TABLE IF NOT EXISTS `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  PRIMARY KEY (`order_voucher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product`
--

CREATE TABLE IF NOT EXISTS `oc_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `noindex` tinyint(1) NOT NULL DEFAULT '1',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `oc_product`
--

INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `noindex`, `viewed`, `date_added`, `date_modified`) VALUES
(50, '7670171', '', '', '', '', '', '', '', 1000, 7, 'catalog/watches/Richard Mille Rm 038/0.jpg', 0, 1, '900.0000', 0, 0, '2017-08-20', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 33, '2017-08-20 15:40:45', '2017-08-25 06:24:26'),
(52, 'RolexExplorer', '', '', '', '', '', '', '', 1000, 7, 'catalog/watches/Rolex Explorer/1.jpeg', 0, 1, '850.0000', 0, 0, '2017-08-22', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 21, '2017-08-23 03:15:46', '2017-08-25 06:02:48'),
(53, 'RichardMilleWatchesRM058', '', '', '', '', '', '', '', 1000, 7, 'catalog/watches/RM 058/RM_58-01_FRONT.jpg', 0, 1, '750.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 16, '2017-08-23 09:56:48', '2017-08-25 09:43:13'),
(54, 'U-BoatPrecious', '', '', '', '', '', '', '', 1000, 7, 'catalog/watches/uboatprecious/1.jpg', 0, 1, '930.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 0, 1, 1, 14, '2017-08-23 10:00:08', '2017-08-25 05:42:58'),
(55, 'audemarspiguetroyaloak', '', '', '', '', '', '', '', 1000, 7, 'catalog/watches/Audemars Piguet Royal Oak/1.jpg', 0, 1, '1000.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 5, 1, 1, 22, '2017-08-23 10:11:18', '2017-08-25 01:56:45'),
(56, 'Cartier Ballon Bleu De Cartier', '', '', '', '', '', '', '', 1000, 7, 'catalog/watches/Cartier Ballon Bleu De Cartier/1.jpg', 0, 1, '2000.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 6, 1, 1, 15, '2017-08-23 10:18:18', '2017-08-25 01:57:00'),
(57, 'Breguet Classique Grande', '', '', '', '', '', '', '', 1000, 7, 'catalog/7040131.jpg', 0, 1, '1650.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 8, 1, 1, 24, '2017-08-23 10:20:34', '2017-08-25 01:56:50'),
(58, 'Zenith Pilot', '', '', '', '', '', '', '', 1000, 7, 'catalog/watches/zenith pilot/1.jpg', 0, 1, '950.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 8, '2017-08-23 10:27:14', '2017-08-25 05:52:46'),
(59, 'Franck Muller Double Mystery', '', '', '', '', '', '', '', 1000, 7, 'catalog/DoubleMystery_2X.jpg', 0, 1, '900.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 16, '2017-08-23 10:28:59', '2017-08-25 09:27:22'),
(60, 'Rolex Day-Date', '', '', '', '', '', '', '', 1001, 7, 'catalog/watches/Rolex Day-Date/1.jpeg', 0, 1, '1000.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 20, '2017-08-23 10:30:28', '2017-08-24 18:17:57'),
(61, '1', '', '', '', '', '', '', '', 1000, 7, 'catalog/moneyclip/versace/1.jpg', 0, 1, '185.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 7, '2017-08-23 10:43:38', '2017-08-25 11:06:49'),
(62, '2', '', '', '', '', '', '', '', 1000, 7, 'catalog/moneyclip/montblanc/1.jpg', 0, 1, '230.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 4, '2017-08-23 10:44:16', '2017-08-25 01:59:02'),
(63, '3', '', '', '', '', '', '', '', 1000, 7, 'catalog/moneyclip/montblanc/2.jpg', 0, 1, '230.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 4, '2017-08-23 10:45:07', '2017-08-25 01:59:07'),
(64, 'Louis Vuitton 1', '', '', '', '', '', '', '', 1000, 7, 'catalog/moneyclip/Louis Vuitton /1.jpg', 0, 1, '165.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 6, '2017-08-23 10:45:26', '2017-08-25 01:58:52'),
(65, 'Louis Vuitton 2', '', '', '', '', '', '', '', 1000, 7, 'catalog/moneyclip/Louis Vuitton /2.jpg', 0, 1, '190.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 3, '2017-08-23 10:45:49', '2017-08-25 01:58:57'),
(66, 'Gucci 1', '', '', '', '', '', '', '', 1000, 7, 'catalog/moneyclip/Gucci /1.jpg', 0, 1, '130.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 2, '2017-08-23 10:46:13', '2017-08-25 01:58:36'),
(67, 'Gucci 2', '', '', '', '', '', '', '', 1000, 7, 'catalog/moneyclip/Gucci /2.jpg', 0, 1, '130.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 4, '2017-08-23 10:46:31', '2017-08-25 01:58:41'),
(68, 'Bulgari Serpenti', '', '', '', '', '', '', '', 1000, 7, 'catalog/watches/Bvlgari WLGQB02017/serpenti_quartz_pink_gold_diamonds_bvlgari_wlgqb02017.jpg', 0, 1, '500.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 15, '2017-08-23 10:59:22', '2017-08-25 01:58:19'),
(69, 'Chopard', '', '', '', '', '', '', '', 1000, 7, 'catalog/Chopard-Womens-288525-3005-Happy-Sport-Round-Black-Rubber-Strap-Diamond-Luxury-Watch-L13524334.jpg', 0, 1, '500.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 9, 1, 1, 18, '2017-08-23 11:00:09', '2017-08-25 08:47:43'),
(70, 'Gucci ', '', '', '', '', '', '', '', 1000, 7, 'catalog/1251272.jpg', 0, 1, '200.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 9, '2017-08-23 11:00:49', '2017-08-25 07:42:02'),
(71, 'Chopard2', '', '', '', '', '', '', '', 1000, 7, 'catalog/chopard-la-strada-diamond-18kt-white-gold-ladies-watch-416547-416547.jpg', 0, 1, '500.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 13, '2017-08-23 11:01:22', '2017-08-25 09:11:14'),
(72, 'Omega', '', '', '', '', '', '', '', 1000, 7, 'catalog/omega-deville-ladymatic-black-diamond-dial-stainless-steel-black-leather-ladies-watch-425.38.34.20.51.001.jpg', 0, 1, '930.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 17, '2017-08-23 11:01:45', '2017-08-25 07:02:07'),
(73, 'Montblanc1', '', '', '', '', '', '', '', 1000, 7, 'catalog/pen/montblanc/1.jpg', 0, 1, '90.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 5, '2017-08-23 11:10:10', '2017-08-25 01:59:13'),
(74, 'penMontblanc2', '', '', '', '', '', '', '', 1000, 7, 'catalog/pen/montblanc/2.jpg', 0, 1, '100.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 6, '2017-08-23 11:10:29', '2017-08-25 01:59:20'),
(75, 'Montblanc3', '', '', '', '', '', '', '', 1000, 7, 'catalog/pen/montblanc/3.jpg', 0, 1, '120.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 5, '2017-08-23 11:10:48', '2017-08-25 01:59:26'),
(76, 'Montblanc4', '', '', '', '', '', '', '', 1000, 7, 'catalog/pen/montblanc/4.jpg', 0, 1, '150.0000', 0, 0, '2017-08-23', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 1, 6, '2017-08-23 11:11:05', '2017-08-25 01:59:31');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_attribute`
--

CREATE TABLE IF NOT EXISTS `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`product_id`,`attribute_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_attribute`
--

INSERT INTO `oc_product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES
(72, 26, 1, 'Раскладывающаяся'),
(72, 25, 2, ''),
(72, 25, 1, 'Кожаный'),
(72, 24, 2, ''),
(72, 24, 1, 'Часы, минуты, секунды'),
(72, 23, 2, 'ETA 4104'),
(72, 22, 1, 'Да'),
(72, 22, 2, ''),
(72, 23, 1, 'ETA 4104'),
(50, 12, 1, 'Часовая сталь, сапфировое стекло'),
(50, 12, 2, ''),
(50, 13, 1, 'Бочкообразные'),
(50, 13, 2, ''),
(50, 14, 1, 'Бытовая'),
(50, 14, 2, ''),
(50, 15, 1, '50 мм'),
(50, 15, 2, ''),
(50, 16, 1, '43 мм'),
(50, 16, 2, ''),
(50, 17, 1, '18 мм'),
(50, 17, 2, ''),
(50, 18, 1, '150 г'),
(50, 18, 2, ''),
(50, 19, 1, 'Да'),
(50, 19, 2, ''),
(50, 20, 1, 'Метки'),
(50, 20, 2, ''),
(50, 22, 1, 'Да'),
(50, 22, 2, 'Yes'),
(50, 23, 1, 'ETA 2824'),
(50, 23, 2, 'ETA 2824'),
(50, 24, 1, 'Часы, Минуты'),
(50, 24, 2, 'Ч'),
(50, 25, 1, 'Каучуковый'),
(50, 25, 2, ''),
(50, 26, 1, 'Раскладывающаяся'),
(50, 26, 2, 'Раскладывающаяся'),
(57, 27, 1, 'Черный'),
(57, 26, 2, ''),
(57, 26, 1, 'Раскладывающаяся'),
(57, 25, 2, ''),
(57, 25, 1, 'Кожаный'),
(57, 24, 2, ''),
(57, 24, 1, 'Часы, минуты, секунды'),
(57, 23, 2, 'Frederic Piguet'),
(57, 23, 1, 'Frederic Piguet'),
(57, 21, 2, ''),
(57, 21, 1, 'Механический'),
(57, 20, 2, ''),
(57, 20, 1, 'Римские'),
(56, 13, 1, 'Круглые'),
(55, 26, 2, ' Folding'),
(55, 26, 1, ' Раскладывающаяся'),
(55, 25, 2, ' Steel'),
(55, 25, 1, ' Стальной'),
(55, 24, 2, ' Hours, minutes, seconds'),
(55, 24, 1, ' Часы, минуты, секунды'),
(55, 23, 2, ' ETA 7750'),
(55, 23, 1, ' ETA 7750'),
(55, 22, 2, ' Yes'),
(55, 22, 1, ' Да'),
(55, 21, 2, ' Mechanical'),
(55, 21, 1, ' Механический'),
(55, 20, 2, ' Tags'),
(55, 20, 1, ' Метки'),
(55, 17, 2, ' 14 mm'),
(55, 17, 1, ' 14 мм'),
(55, 16, 1, ' 44 мм'),
(55, 16, 2, ' 44 mm'),
(55, 14, 2, ' Household'),
(55, 14, 1, ' Бытовая'),
(55, 13, 2, ' Round'),
(55, 13, 1, ' Круглая'),
(55, 12, 2, ' Hour Steel'),
(55, 12, 1, ' Часовая сталь'),
(56, 14, 2, ''),
(56, 16, 1, '46 мм'),
(56, 14, 1, 'Бытовая'),
(56, 13, 2, ''),
(57, 18, 1, '96 г'),
(57, 18, 2, ''),
(57, 17, 2, ''),
(57, 17, 1, '13 мм'),
(57, 16, 2, ''),
(57, 14, 2, ''),
(57, 16, 1, '43 мм'),
(57, 14, 1, 'Бытовая'),
(57, 12, 2, ''),
(57, 13, 1, 'Круглые'),
(57, 13, 2, ''),
(68, 13, 1, 'Овальные'),
(68, 12, 2, ''),
(68, 12, 1, 'Часовая сталь, сапфировое стекло'),
(68, 21, 1, 'Кварцевый'),
(68, 20, 2, ''),
(68, 15, 2, ''),
(68, 16, 1, '25 мм'),
(68, 16, 2, ''),
(68, 17, 1, '9 мм'),
(68, 17, 2, ''),
(68, 20, 1, 'Римские'),
(68, 25, 1, 'Стальной'),
(68, 24, 2, ''),
(68, 24, 1, 'Часы, минуты'),
(68, 23, 2, ''),
(68, 23, 1, 'ETA Quartz'),
(68, 21, 2, ''),
(68, 15, 1, '38 мм'),
(68, 14, 2, ''),
(68, 14, 1, 'Бытовая'),
(68, 13, 2, ''),
(56, 27, 1, 'Коричневый'),
(56, 27, 2, ''),
(56, 29, 1, 'Мужские'),
(56, 29, 2, ''),
(56, 30, 1, 'Cartier'),
(56, 24, 1, 'Часы, минуты'),
(56, 24, 2, ''),
(56, 25, 1, 'Кожаный'),
(56, 25, 2, ''),
(56, 26, 1, 'Раскладывающаяся'),
(56, 26, 2, ''),
(56, 16, 2, ''),
(56, 17, 1, '16 мм'),
(56, 17, 2, ''),
(56, 18, 1, '109 г'),
(56, 18, 2, ''),
(56, 20, 1, 'Римские'),
(56, 20, 2, ''),
(56, 21, 1, 'Механический'),
(56, 21, 2, ''),
(56, 23, 1, 'Frederic Piguet'),
(56, 23, 2, ''),
(60, 26, 2, ''),
(60, 27, 1, 'Золотистый'),
(60, 27, 2, ''),
(60, 23, 1, 'ETA 2836'),
(60, 23, 2, ''),
(60, 24, 1, 'Часы, минуты, секунды'),
(60, 24, 2, ''),
(60, 25, 1, 'Стальной'),
(60, 25, 2, ''),
(60, 26, 1, 'Раскладывающаяся'),
(60, 16, 1, '36 мм'),
(60, 16, 2, ''),
(60, 18, 1, '118 г'),
(60, 18, 2, ''),
(60, 20, 1, 'Метки'),
(60, 20, 2, ''),
(60, 21, 1, 'Механический'),
(60, 21, 2, ''),
(60, 22, 1, 'Да'),
(60, 22, 2, ''),
(60, 14, 2, ''),
(60, 14, 1, 'Бытовая'),
(60, 13, 2, ''),
(60, 13, 1, 'Круглые'),
(60, 12, 2, ''),
(60, 12, 1, 'Часовая сталь, сапфировое стекло'),
(60, 30, 1, 'Rolex'),
(60, 30, 2, ''),
(60, 31, 1, 'Day-date'),
(60, 31, 2, ''),
(60, 29, 1, 'Мужские часы'),
(60, 29, 2, ''),
(72, 31, 1, 'Ladymatic'),
(72, 32, 1, 'Швейцария'),
(72, 31, 2, ''),
(50, 31, 2, 'Rm 038'),
(50, 31, 1, 'Rm 038'),
(50, 30, 2, 'Richard Mille'),
(50, 30, 1, 'Richard Mille'),
(50, 29, 2, 'Men''s'),
(50, 29, 1, 'Мужские'),
(50, 27, 2, ''),
(50, 27, 1, 'Серебристый'),
(57, 12, 1, 'Корпус ..... часовая сталь, Стекло ..... сапфировое'),
(56, 12, 2, ''),
(56, 12, 1, 'Часовая сталь, сапфировое стекло'),
(55, 27, 1, ' Серебристый'),
(55, 27, 2, ' Silver'),
(57, 27, 2, ''),
(56, 30, 2, ''),
(56, 31, 1, 'Ballon Blue De Cartier'),
(56, 31, 2, ''),
(68, 25, 2, ''),
(68, 27, 1, 'Золотистый'),
(68, 27, 2, ''),
(68, 29, 1, 'Женские'),
(68, 29, 2, ''),
(68, 30, 1, 'Bvlgari'),
(68, 30, 2, ''),
(68, 31, 1, 'Serpenti'),
(68, 31, 2, ''),
(54, 29, 2, ''),
(54, 30, 1, 'U-boat'),
(54, 30, 2, ''),
(54, 31, 1, 'Precious'),
(54, 31, 2, ''),
(54, 21, 1, 'Механический'),
(54, 29, 1, 'Мужские'),
(54, 21, 2, ''),
(54, 22, 1, 'Да'),
(54, 22, 2, ''),
(54, 23, 1, 'ETA 2836'),
(54, 23, 2, ''),
(54, 24, 1, 'Часы, минуты, секунды'),
(54, 24, 2, ''),
(54, 12, 1, 'Часовая сталь, сапфировое стекло'),
(54, 12, 2, ''),
(54, 13, 1, 'Круглые'),
(54, 13, 2, ''),
(54, 14, 1, 'Бытовая'),
(54, 14, 2, ''),
(54, 16, 1, '48 мм'),
(54, 16, 2, ''),
(54, 17, 1, '20 мм'),
(54, 17, 2, ''),
(54, 18, 1, '180 г'),
(54, 18, 2, ''),
(54, 20, 1, 'Метки'),
(54, 20, 2, ''),
(54, 25, 1, 'Кожаный'),
(54, 25, 2, ''),
(54, 26, 1, 'Классическая'),
(54, 26, 2, ''),
(54, 27, 1, 'Черный'),
(54, 27, 2, ''),
(58, 29, 1, 'Мужские'),
(58, 29, 2, ''),
(58, 30, 1, 'Zenith'),
(58, 30, 2, ''),
(58, 31, 1, 'Pilot'),
(58, 31, 2, ''),
(58, 21, 1, 'Механический'),
(58, 21, 2, ''),
(58, 22, 1, 'Да'),
(58, 22, 2, ''),
(58, 23, 1, 'ETA 2836'),
(58, 23, 2, ''),
(58, 24, 1, 'Часы, минуты, Секунды'),
(58, 24, 2, ''),
(58, 12, 1, 'Часовая сталь, сапфировое стекло'),
(58, 12, 2, ''),
(58, 13, 1, 'Круглые'),
(58, 13, 2, ''),
(58, 14, 1, 'Бытовая'),
(58, 14, 2, ''),
(58, 16, 1, '48 мм'),
(58, 16, 2, ''),
(58, 17, 1, '14 мм'),
(58, 17, 2, ''),
(58, 18, 1, '123 г'),
(58, 18, 2, ''),
(58, 20, 1, 'Арабские'),
(58, 20, 2, ''),
(58, 25, 1, 'Кожаный'),
(58, 25, 2, ''),
(58, 26, 1, 'Классичекская'),
(58, 26, 2, ''),
(58, 27, 1, 'Коричневый'),
(58, 27, 2, ''),
(52, 29, 1, 'Мужские'),
(52, 29, 2, ''),
(52, 30, 1, 'Rolex'),
(52, 30, 2, ''),
(52, 31, 1, 'Explorer'),
(52, 31, 2, ''),
(52, 21, 1, 'Механический'),
(52, 21, 2, ''),
(52, 22, 1, 'Да'),
(52, 22, 2, ''),
(52, 23, 1, 'ETA 2824'),
(52, 23, 2, 'ETA 2824'),
(52, 24, 1, 'Часы, минуты, секунды'),
(52, 24, 2, ''),
(52, 12, 1, 'Часовая сталь, сапфировое стекло'),
(52, 12, 2, ''),
(52, 13, 1, 'Круглые'),
(52, 13, 2, ''),
(52, 14, 1, 'Бытовая'),
(52, 14, 2, ''),
(52, 16, 1, '39 мм'),
(52, 16, 2, ''),
(52, 17, 1, '12 мм'),
(52, 17, 2, ''),
(52, 18, 1, '132 г'),
(52, 18, 2, ''),
(52, 20, 1, 'Арабские'),
(52, 20, 2, ''),
(52, 25, 1, 'Стальной'),
(52, 25, 2, ''),
(52, 26, 1, 'Раскладывающаяся'),
(52, 26, 2, ''),
(52, 27, 1, 'Серебристый'),
(52, 27, 2, ''),
(72, 26, 2, ''),
(72, 27, 1, 'Черный'),
(72, 27, 2, ''),
(72, 29, 1, 'Женские'),
(72, 29, 2, ''),
(72, 30, 1, 'Omega'),
(72, 30, 2, ''),
(72, 13, 1, 'Круглые'),
(72, 13, 2, ''),
(72, 14, 1, 'Бытовая'),
(72, 14, 2, ''),
(72, 16, 1, '34 мм'),
(72, 16, 2, ''),
(72, 17, 1, '13 мм'),
(72, 17, 2, ''),
(72, 18, 1, '65 г'),
(72, 18, 2, ''),
(72, 20, 1, 'Метки'),
(72, 20, 2, ''),
(72, 21, 1, 'Механический'),
(72, 21, 2, ''),
(72, 12, 2, ''),
(72, 12, 1, 'Часовая сталь, сапфировое стекло'),
(72, 32, 2, ''),
(72, 33, 1, 'ETA 4104'),
(72, 33, 2, 'ETA 4104'),
(72, 34, 1, '10 лет'),
(72, 34, 2, ''),
(70, 15, 2, '38 мм'),
(70, 15, 1, '38 мм'),
(70, 14, 2, ''),
(70, 14, 1, 'Бытовая'),
(70, 13, 2, ''),
(70, 12, 2, ''),
(70, 13, 1, 'Прямоугольные'),
(70, 12, 1, 'Керамика, сапфировое стекло'),
(70, 21, 2, ''),
(70, 21, 1, 'Кварцевый'),
(70, 20, 2, ''),
(70, 17, 2, '8 мм'),
(70, 20, 1, 'Без меток'),
(70, 26, 1, 'Раскладывающаяся'),
(70, 25, 2, ''),
(70, 25, 1, 'Керамический'),
(70, 24, 2, ''),
(70, 24, 1, 'Часы, минуты'),
(70, 23, 2, 'Ronda Quartz'),
(70, 23, 1, 'Ronda Quartz'),
(70, 17, 1, '8 мм'),
(70, 16, 2, '15 мм'),
(70, 16, 1, '15 мм'),
(70, 26, 2, ''),
(70, 27, 1, 'Черный'),
(70, 27, 2, ''),
(70, 29, 1, 'Женские'),
(70, 29, 2, ''),
(70, 30, 1, 'Gucci'),
(70, 30, 2, 'Gucci'),
(70, 31, 1, 'Ladies Classic'),
(70, 31, 2, 'Ladies Classic'),
(70, 32, 1, 'Бельгия'),
(70, 32, 2, ''),
(70, 33, 1, 'Ronda Quartz'),
(70, 33, 2, 'Ronda Quartz'),
(70, 34, 1, '6 лет'),
(70, 34, 2, ''),
(69, 29, 1, 'Женские'),
(69, 29, 2, ''),
(69, 30, 1, 'Chopard'),
(69, 30, 2, 'Chopard'),
(69, 31, 1, 'Happy sport'),
(69, 31, 2, 'Happy sport'),
(69, 33, 1, 'ETA Quartz'),
(69, 33, 2, 'ETA Quartz'),
(69, 34, 1, '6 лет'),
(69, 34, 2, ''),
(69, 32, 1, 'Бельгия'),
(69, 32, 2, ''),
(69, 21, 1, 'Кварцевый'),
(69, 21, 2, ''),
(69, 23, 1, 'ETA Quartz'),
(69, 23, 2, 'ETA Quartz'),
(69, 24, 1, 'Часы, минуты, секунды'),
(69, 24, 2, ''),
(69, 13, 1, 'Круглые'),
(69, 13, 2, ''),
(69, 14, 1, 'Бытовая'),
(69, 14, 2, ''),
(69, 17, 1, '12 мм'),
(69, 17, 2, ''),
(69, 20, 1, 'Метки'),
(69, 20, 2, ''),
(69, 25, 1, 'Кожаный'),
(69, 25, 2, ''),
(69, 26, 1, 'Классическая'),
(69, 26, 2, ''),
(69, 27, 1, 'Черный'),
(69, 27, 2, ''),
(71, 29, 1, 'Женские'),
(71, 29, 2, ''),
(71, 30, 1, 'Chopard'),
(71, 30, 2, 'Chopard'),
(71, 31, 1, 'La Strada'),
(71, 31, 2, ''),
(71, 32, 1, 'Бельгия'),
(71, 32, 2, ''),
(71, 33, 1, 'ETA Quartz'),
(71, 33, 2, 'ETA Quartz'),
(71, 34, 1, '6 лет'),
(71, 34, 2, ''),
(71, 21, 1, 'Кварцевый'),
(71, 21, 2, ''),
(71, 23, 1, 'ETA Quartz'),
(71, 23, 2, 'ETA Quartz'),
(71, 24, 1, 'Часы, минуты, секунды'),
(71, 24, 2, ''),
(71, 12, 1, 'Часовая сталь, сапфировое стекло'),
(71, 12, 2, ''),
(71, 13, 1, 'Прямоугольные'),
(71, 13, 2, ''),
(71, 14, 1, 'Бытовая'),
(71, 14, 2, ''),
(71, 15, 1, '48 мм'),
(71, 15, 2, '48 мм'),
(71, 17, 1, '11 мм'),
(71, 17, 2, '11 мм'),
(71, 18, 1, '118 г'),
(71, 18, 2, ''),
(71, 20, 1, 'Римские'),
(71, 20, 2, ''),
(71, 25, 1, 'Стальной'),
(71, 25, 2, ''),
(71, 26, 1, 'Раскладывающаяся'),
(71, 26, 2, ''),
(71, 27, 1, 'Серебристый'),
(71, 27, 2, ''),
(59, 29, 1, 'Женские'),
(59, 29, 2, ''),
(59, 30, 1, 'Franck Muller'),
(59, 30, 2, 'Franck Muller'),
(59, 31, 1, 'Double Mystery'),
(59, 31, 2, 'Double Mystery'),
(59, 32, 1, 'Швейцария'),
(59, 32, 2, ''),
(59, 33, 1, 'ETA 2836'),
(59, 33, 2, 'ETA 2836'),
(59, 21, 1, 'Механический'),
(59, 21, 2, ''),
(59, 22, 1, 'Да'),
(59, 22, 2, ''),
(59, 23, 1, 'ETA 2836'),
(59, 23, 2, 'ETA 2836'),
(59, 24, 1, 'Часы, минуты'),
(59, 24, 2, ''),
(59, 12, 1, 'Часовая сталь, сапфировое стекло'),
(59, 12, 2, ''),
(59, 13, 1, 'Круглые'),
(59, 13, 2, ''),
(59, 14, 1, 'Бытовая'),
(59, 14, 2, ''),
(59, 16, 1, '36 мм'),
(59, 16, 2, '36 мм'),
(59, 17, 1, '11 мм'),
(59, 17, 2, '11 мм'),
(59, 18, 1, '65 г'),
(59, 18, 2, ''),
(59, 20, 1, 'Метки'),
(59, 20, 2, ''),
(59, 25, 1, 'Кожаный'),
(59, 25, 2, ''),
(59, 26, 1, 'Классическая'),
(59, 26, 2, ''),
(59, 27, 1, 'Синий'),
(59, 27, 2, ''),
(53, 29, 1, 'Мужские'),
(53, 29, 2, ''),
(53, 30, 1, 'Richard Mille'),
(53, 30, 2, 'Richard Mille'),
(53, 31, 1, 'Rm 058'),
(53, 31, 2, 'Rm 058'),
(53, 32, 1, 'Швейцария'),
(53, 32, 2, ''),
(53, 33, 1, 'ETA 2824'),
(53, 33, 2, 'ETA 2824'),
(53, 34, 1, '10 лет'),
(53, 34, 2, ''),
(53, 21, 1, 'Механический'),
(53, 21, 2, ''),
(53, 22, 1, 'Да'),
(53, 22, 2, ''),
(53, 23, 1, 'ETA 2824'),
(53, 23, 2, 'ETA 2824'),
(53, 24, 1, 'Часы, минуты'),
(53, 24, 2, ''),
(53, 12, 1, 'Часовая сталь, сапфировое стекло'),
(53, 12, 2, ''),
(53, 13, 1, 'Круглые'),
(53, 13, 2, ''),
(53, 14, 1, 'Бытовая'),
(53, 14, 2, ''),
(53, 16, 1, '50 мм'),
(53, 16, 2, '50 мм'),
(53, 17, 1, '13.5 мм'),
(53, 17, 2, '13.5 мм'),
(53, 18, 1, '180 г'),
(53, 18, 2, ''),
(53, 20, 1, 'Арабские'),
(53, 20, 2, ''),
(53, 19, 1, 'Да'),
(53, 19, 2, ''),
(53, 25, 1, 'Каучуковый'),
(53, 25, 2, ''),
(53, 26, 1, 'Раскладывающаяся'),
(53, 26, 2, ''),
(53, 27, 1, 'Черный'),
(53, 27, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_description`
--

CREATE TABLE IF NOT EXISTS `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  PRIMARY KEY (`product_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_description`
--

INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`, `meta_h1`) VALUES
(50, 2, 'Richard Mille Rm 038', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(52, 2, 'Rolex Explorer', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(53, 2, 'Richard Mille Watches RM 058', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(54, 1, 'U-Boat Precious', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(54, 2, 'U-Boat Precious', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(55, 2, 'Audemars Piguet Royal Oak', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(56, 2, 'Cartier Ballon Bleu De Cartier', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(57, 2, 'Breguet Classique Grande', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(58, 2, 'Zenith Pilot', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(59, 2, 'Franck Muller Double Mystery', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(60, 2, 'Rolex Day-Date', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(61, 2, 'Versace', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(62, 1, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(62, 2, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(63, 1, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(63, 2, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(64, 1, 'Louis Vuitton ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(64, 2, 'Louis Vuitton ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(65, 1, 'Louis Vuitton ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(65, 2, 'Louis Vuitton ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(66, 2, 'Gucci ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(67, 2, 'Gucci ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(68, 2, 'Bulgari Serpenti', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(69, 1, 'Chopard', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(69, 2, 'Chopard', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(70, 1, 'Gucci ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(70, 2, 'Gucci ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(71, 2, 'Chopard', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(72, 2, 'Omega', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(73, 1, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(73, 2, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(74, 2, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(75, 2, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(76, 2, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(56, 1, 'Cartier Ballon Bleu De Cartier', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(60, 1, 'Rolex Day-Date', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(61, 1, 'Cartier', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(74, 1, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(75, 1, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(76, 1, 'Montblanc', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(66, 1, 'Gucci ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(67, 1, 'Gucci ', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(55, 1, 'Audemars Piguet Royal Oak', '&lt;div class=&quot;item-description__content-section item-description__content-section--centered&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; line-height: inherit; font-family: &amp;quot;PT Sans&amp;quot;, Verdana; margin: 0px; padding: 0px 70px 0px 0px; vertical-align: baseline; float: none; width: 343.328px; display: inline-block;&quot;&gt;&lt;div class=&quot;item-description__content-block&quot; style=&quot;outline: none; border: 0px none; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; font-family: inherit; margin: 0px; padding: 0px; vertical-align: baseline;&quot;&gt;&lt;div style=&quot;color: rgb(160, 116, 23); font-size: 20px;&quot;&gt;&lt;br&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;table class=&quot;table table-bordered&quot;&gt;&lt;tbody&gt;&lt;tr&gt;&lt;td&gt;&lt;br&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/tbody&gt;&lt;/table&gt;&lt;div class=&quot;item-description__content-section item-description__content-section--centered&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: &amp;quot;PT Sans&amp;quot;, Verdana; margin: 0px; padding: 0px 70px 0px 0px; vertical-align: baseline; float: none; width: 343.328px; display: inline-block; color: rgb(45, 58, 79);&quot;&gt;&lt;div class=&quot;item-description__content-block&quot; style=&quot;outline: none; border: 0px none; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; margin: 0px; padding: 0px; vertical-align: baseline;&quot;&gt;&lt;ul class=&quot;item-description__content-list&quot; style=&quot;outline: none; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px none; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: none outside none;&quot;&gt;&lt;li class=&quot;item-description__content-item&quot; style=&quot;outline: none; border: 0px none; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; margin: 0px; padding: 0px 0px 10px; vertical-align: baseline; position: relative; z-index: 0;&quot;&gt;&lt;span class=&quot;item-description__content-item-text&quot; style=&quot;outline: none; border: 0px none; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.4; font-family: inherit; margin: 0px; padding: 0px 0px 0px 8px; vertical-align: baseline; float: right; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(91, 93, 96); max-width: 59%;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/div&gt;&lt;/div&gt;&lt;br&gt;&lt;div class=&quot;item-description__content-section item-description__content-section--centered&quot; style=&quot;outline: none; border: 0px none; font-variant-numeric: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: &amp;quot;PT Sans&amp;quot;, Verdana; margin: 0px; padding: 0px 70px 0px 0px; vertical-align: baseline; float: none; width: 343.328px; display: inline-block; color: rgb(45, 58, 79);&quot;&gt;&lt;div class=&quot;item-description__content-block&quot; style=&quot;outline: none; border: 0px none; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; margin: 0px; padding: 0px; vertical-align: baseline;&quot;&gt;&lt;ul class=&quot;item-description__content-list&quot; style=&quot;outline: none; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px none; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; list-style: none outside none;&quot;&gt;&lt;li class=&quot;item-description__content-item&quot; style=&quot;outline: none; border: 0px none; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; margin: 0px; padding: 0px 0px 10px; vertical-align: baseline; position: relative; z-index: 0;&quot;&gt;&lt;span class=&quot;item-description__content-item-text&quot; style=&quot;outline: none; border: 0px none; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.4; font-family: inherit; margin: 0px; padding: 0px 0px 0px 8px; vertical-align: baseline; float: right; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(91, 93, 96); max-width: 59%;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/div&gt;&lt;/div&gt;', '', '', '', '', ''),
(57, 1, 'Breguet Classique Grande', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(68, 1, 'Bvlgari Serpenti', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(59, 1, 'Franck Muller Double Mystery', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(50, 1, 'Richard Mille Rm 038', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(58, 1, 'Zenith Pilot', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(71, 1, 'Chopard', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(72, 1, 'Omega', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(53, 1, 'Richard Mille Watches RM 058', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(52, 1, 'Rolex Explorer', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_discount`
--

CREATE TABLE IF NOT EXISTS `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`product_discount_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=447 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_filter`
--

CREATE TABLE IF NOT EXISTS `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`filter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_filter`
--

INSERT INTO `oc_product_filter` (`product_id`, `filter_id`) VALUES
(50, 1),
(50, 7);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_image`
--

CREATE TABLE IF NOT EXISTS `oc_product_image` (
  `product_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_image_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2458 ;

--
-- Dumping data for table `oc_product_image`
--

INSERT INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `sort_order`) VALUES
(2450, 50, 'catalog/watches/Richard Mille Rm 038/4.jpg', 4),
(2449, 50, 'catalog/watches/Richard Mille Rm 038/3.jpg', 3),
(2448, 50, 'catalog/watches/Richard Mille Rm 038/2.jpg', 2),
(2447, 50, 'catalog/watches/Richard Mille Rm 038/2.jpg', 1),
(2446, 50, 'catalog/watches/Richard Mille Rm 038/1.jpg', 0),
(2457, 53, 'catalog/watches/RM 058/CLOSE_UP_back.jpg', 0),
(2456, 53, 'catalog/watches/RM 058/CLOSE_UP_crown.jpg', 0),
(2455, 53, 'catalog/watches/RM 058/CLOSE_UP_world_timer.jpg', 0),
(2454, 53, 'catalog/watches/RM 058/RM_58-01_3-4.jpg', 0),
(2453, 53, 'catalog/watches/RM 058/RM_58-01_BACK.jpg', 0),
(2452, 72, 'catalog/omega-de-ville-ladymatic-black-diamonds-42533342051001-2738-4.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option`
--

CREATE TABLE IF NOT EXISTS `oc_product_option` (
  `product_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  PRIMARY KEY (`product_option_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=227 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option_value`
--

CREATE TABLE IF NOT EXISTS `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL,
  PRIMARY KEY (`product_option_value_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_recurring`
--

CREATE TABLE IF NOT EXISTS `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related`
--

CREATE TABLE IF NOT EXISTS `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related_article`
--

CREATE TABLE IF NOT EXISTS `oc_product_related_article` (
  `article_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related_mn`
--

CREATE TABLE IF NOT EXISTS `oc_product_related_mn` (
  `product_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related_wb`
--

CREATE TABLE IF NOT EXISTS `oc_product_related_wb` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_reward`
--

CREATE TABLE IF NOT EXISTS `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_reward_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=550 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_special`
--

CREATE TABLE IF NOT EXISTS `oc_product_special` (
  `product_special_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`product_special_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=457 ;

--
-- Dumping data for table `oc_product_special`
--

INSERT INTO `oc_product_special` (`product_special_id`, `product_id`, `customer_group_id`, `priority`, `price`, `date_start`, `date_end`) VALUES
(455, 50, 1, 0, '800.0000', '2017-07-31', '2017-10-26'),
(450, 57, 1, 0, '1500.0000', '0000-00-00', '2017-10-26'),
(449, 55, 1, 0, '860.0000', '0000-00-00', '2017-12-01'),
(451, 56, 1, 0, '1700.0000', '0000-00-00', '2017-11-04'),
(456, 69, 1, 0, '420.0000', '0000-00-00', '2017-11-17');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_tab`
--

CREATE TABLE IF NOT EXISTS `oc_product_tab` (
  `product_tab_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `sort_order` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`product_tab_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_tab_desc`
--

CREATE TABLE IF NOT EXISTS `oc_product_tab_desc` (
  `product_tab_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_category`
--

CREATE TABLE IF NOT EXISTS `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `main_category` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`category_id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`product_id`, `category_id`, `main_category`) VALUES
(50, 61, 1),
(50, 20, 0),
(52, 20, 0),
(52, 61, 1),
(53, 61, 1),
(53, 20, 0),
(54, 20, 0),
(54, 61, 1),
(55, 61, 1),
(55, 20, 0),
(56, 20, 0),
(56, 61, 1),
(57, 61, 1),
(57, 20, 0),
(58, 61, 1),
(58, 20, 0),
(59, 62, 1),
(61, 59, 1),
(62, 59, 1),
(60, 20, 0),
(60, 61, 1),
(63, 59, 1),
(64, 59, 1),
(65, 59, 1),
(68, 62, 1),
(68, 20, 0),
(69, 20, 0),
(69, 62, 1),
(70, 20, 0),
(70, 62, 1),
(71, 62, 1),
(71, 20, 0),
(72, 62, 1),
(72, 20, 0),
(73, 24, 1),
(74, 24, 1),
(75, 24, 1),
(76, 24, 1),
(66, 59, 1),
(67, 59, 1),
(59, 20, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_download`
--

CREATE TABLE IF NOT EXISTS `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`download_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_layout`
--

CREATE TABLE IF NOT EXISTS `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_layout`
--

INSERT INTO `oc_product_to_layout` (`product_id`, `store_id`, `layout_id`) VALUES
(50, 0, 0),
(52, 0, 0),
(53, 0, 0),
(54, 0, 0),
(55, 0, 0),
(56, 0, 0),
(57, 0, 0),
(58, 0, 0),
(59, 0, 0),
(60, 0, 0),
(61, 0, 0),
(62, 0, 0),
(63, 0, 0),
(64, 0, 0),
(65, 0, 0),
(66, 0, 0),
(67, 0, 0),
(68, 0, 0),
(69, 0, 0),
(70, 0, 0),
(71, 0, 0),
(72, 0, 0),
(73, 0, 0),
(74, 0, 0),
(75, 0, 0),
(76, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(50, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring`
--

CREATE TABLE IF NOT EXISTS `oc_recurring` (
  `recurring_id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) unsigned NOT NULL,
  `cycle` int(10) unsigned NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) unsigned NOT NULL,
  `trial_cycle` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`recurring_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring_description`
--

CREATE TABLE IF NOT EXISTS `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`recurring_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return`
--

CREATE TABLE IF NOT EXISTS `oc_return` (
  `return_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`return_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_action`
--

CREATE TABLE IF NOT EXISTS `oc_return_action` (
  `return_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`return_action_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Возвращено'),
(3, 1, 'Отправка отправлена'),
(1, 2, 'Refunded'),
(3, 2, 'Replacement Sent'),
(2, 1, 'Залог возвращен'),
(2, 2, 'Credit Issued');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_history`
--

CREATE TABLE IF NOT EXISTS `oc_return_history` (
  `return_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`return_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_reason`
--

CREATE TABLE IF NOT EXISTS `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`return_reason_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Получен неисправным (сломанным)'),
(1, 2, 'Dead On Arrival'),
(2, 1, 'Получен не тот (ошибочный) товар'),
(2, 2, 'Received Wrong Item'),
(3, 1, 'Заказан по ошибке'),
(3, 2, 'Order Error'),
(4, 1, 'Неисправен, пожалуйста укажите/приложите подробности'),
(4, 2, 'Faulty, please supply details'),
(5, 1, 'Другое (другая причина), пожалуйста укажите/приложите подробности'),
(5, 2, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_status`
--

CREATE TABLE IF NOT EXISTS `oc_return_status` (
  `return_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`return_status_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'В ожидании'),
(3, 1, 'Готов'),
(2, 1, 'Ожидаемые Продукты'),
(2, 2, 'Awaiting Products'),
(3, 2, 'Complete'),
(1, 2, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `oc_review`
--

CREATE TABLE IF NOT EXISTS `oc_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`review_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_review_article`
--

CREATE TABLE IF NOT EXISTS `oc_review_article` (
  `review_article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`review_article_id`),
  KEY `article_id` (`article_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `oc_review_article`
--

INSERT INTO `oc_review_article` (`review_article_id`, `article_id`, `customer_id`, `author`, `text`, `rating`, `status`, `date_added`, `date_modified`) VALUES
(11, 123, 0, 'Василий Покупайкин', 'Спасибо за отличный фото обзор, обязательно в ближайшее время приобрету себе такую тушку и напишу дополнение к Вашей статье.', 5, 1, '2014-04-08 05:59:25', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_nauthor`
--

CREATE TABLE IF NOT EXISTS `oc_sb_nauthor` (
  `nauthor_id` int(11) NOT NULL AUTO_INCREMENT,
  `adminid` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`nauthor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_nauthor_description`
--

CREATE TABLE IF NOT EXISTS `oc_sb_nauthor_description` (
  `nauthor_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `ctitle` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`nauthor_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_ncategory`
--

CREATE TABLE IF NOT EXISTS `oc_sb_ncategory` (
  `ncategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ncategory_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=59 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_ncategory_description`
--

CREATE TABLE IF NOT EXISTS `oc_sb_ncategory_description` (
  `ncategory_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `description` text COLLATE utf8_bin NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ncategory_id`,`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_ncategory_to_layout`
--

CREATE TABLE IF NOT EXISTS `oc_sb_ncategory_to_layout` (
  `ncategory_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`ncategory_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_ncategory_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_sb_ncategory_to_store` (
  `ncategory_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`ncategory_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_ncomments`
--

CREATE TABLE IF NOT EXISTS `oc_sb_ncomments` (
  `ncomment_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `language_id` int(2) NOT NULL,
  `reply_id` int(11) NOT NULL DEFAULT '0',
  `author` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `text` text COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ncomment_id`),
  KEY `news_id` (`news_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_news`
--

CREATE TABLE IF NOT EXISTS `oc_sb_news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `nauthor_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `acom` int(1) NOT NULL DEFAULT '0',
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `image2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `sort_order` int(11) DEFAULT NULL,
  `gal_thumb_w` int(5) NOT NULL,
  `gal_thumb_h` int(5) NOT NULL,
  `gal_popup_w` int(5) NOT NULL,
  `gal_popup_h` int(5) NOT NULL,
  `gal_slider_h` int(4) NOT NULL,
  `gal_slider_t` int(1) NOT NULL,
  `date_pub` datetime DEFAULT NULL,
  `gal_slider_w` int(4) NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_news_archive`
--

CREATE TABLE IF NOT EXISTS `oc_sb_news_archive` (
  `news_archive_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(3) NOT NULL,
  `year` int(4) NOT NULL,
  `months` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`news_archive_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_news_description`
--

CREATE TABLE IF NOT EXISTS `oc_sb_news_description` (
  `news_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ctitle` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description2` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_desc` varchar(255) COLLATE utf8_bin NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_bin NOT NULL,
  `ntags` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cfield1` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cfield2` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cfield3` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cfield4` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`news_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_news_gallery`
--

CREATE TABLE IF NOT EXISTS `oc_sb_news_gallery` (
  `news_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `image` varchar(512) DEFAULT NULL,
  `text` text NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`news_image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_news_related`
--

CREATE TABLE IF NOT EXISTS `oc_sb_news_related` (
  `news_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`news_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_news_to_layout`
--

CREATE TABLE IF NOT EXISTS `oc_sb_news_to_layout` (
  `news_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  PRIMARY KEY (`news_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_news_to_ncategory`
--

CREATE TABLE IF NOT EXISTS `oc_sb_news_to_ncategory` (
  `news_id` int(11) NOT NULL,
  `ncategory_id` int(11) NOT NULL,
  PRIMARY KEY (`news_id`,`ncategory_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_news_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_sb_news_to_store` (
  `news_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`news_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `oc_sb_news_video`
--

CREATE TABLE IF NOT EXISTS `oc_sb_news_video` (
  `news_video_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `text` text COLLATE utf8_bin NOT NULL,
  `video` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`news_video_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_setting`
--

CREATE TABLE IF NOT EXISTS `oc_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` longtext NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4745 ;

--
-- Dumping data for table `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(1, 0, 'shipping', 'shipping_sort_order', '3', 0),
(2, 0, 'sub_total', 'sub_total_sort_order', '1', 0),
(3, 0, 'sub_total', 'sub_total_status', '1', 0),
(5, 0, 'total', 'total_sort_order', '9', 0),
(6, 0, 'total', 'total_status', '1', 0),
(915, 0, 'google_captcha', 'google_captcha_status', '1', 0),
(9, 0, 'cod', 'cod_sort_order', '5', 0),
(10, 0, 'cod', 'cod_total', '0.01', 0),
(11, 0, 'cod', 'cod_order_status_id', '1', 0),
(12, 0, 'cod', 'cod_geo_zone_id', '0', 0),
(13, 0, 'cod', 'cod_status', '1', 0),
(14, 0, 'shipping', 'shipping_status', '1', 0),
(15, 0, 'shipping', 'shipping_estimator', '1', 0),
(4350, 0, 'shoputils_ik', 'shoputils_ik_geo_zone_id', '0', 0),
(4265, 0, 'flat', 'flat_sort_order', '1', 0),
(4263, 0, 'flat', 'flat_geo_zone_id', '3', 0),
(914, 0, 'google_captcha', 'google_captcha_secret', '6LduWi0UAAAAALhmeiFX-Q4uQlOD1xPVo4seOG-P', 0),
(1839, 0, 'category', 'category_status', '1', 0),
(159, 0, 'affiliate', 'affiliate_status', '1', 0),
(1416, 0, 'filter', 'filter_status', '1', 0),
(4264, 0, 'flat', 'flat_status', '1', 0),
(4262, 0, 'flat', 'flat_tax_class_id', '9', 0),
(913, 0, 'google_captcha', 'google_captcha_key', '6LduWi0UAAAAAO5_fhATFmhbmNv7QjzS25wqVJXW', 0),
(1838, 0, 'information', 'information_status', '1', 0),
(301, 0, 'configblog', 'configblog_image_related_height', '200', 0),
(302, 0, 'configblog', 'configblog_image_related_width', '200', 0),
(303, 0, 'configblog', 'configblog_image_article_height', '150', 0),
(304, 0, 'configblog', 'configblog_image_article_width', '150', 0),
(305, 0, 'configblog', 'configblog_image_category_height', '50', 0),
(306, 0, 'configblog', 'configblog_image_category_width', '50', 0),
(307, 0, 'configblog', 'configblog_review_mail', '1', 0),
(308, 0, 'configblog', 'configblog_review_guest', '1', 0),
(309, 0, 'configblog', 'configblog_review_status', '1', 0),
(400, 0, 'configblog', 'configblog_article_download', '1', 0),
(401, 0, 'configblog', 'configblog_blog_menu', '1', 0),
(402, 0, 'configblog', 'configblog_limit_admin', '20', 0),
(403, 0, 'configblog', 'configblog_article_description_length', '200', 0),
(404, 0, 'configblog', 'configblog_article_limit', '20', 0),
(405, 0, 'configblog', 'configblog_article_count', '1', 0),
(2654, 0, 'configmenu', 'configmenu_menu', '1', 0),
(407, 0, 'manufacturer', 'manufacturer_status', '1', 0),
(4734, 0, 'config', 'config_compression', '0', 0),
(4735, 0, 'config', 'config_secure', '1', 0),
(4736, 0, 'config', 'config_password', '1', 0),
(4737, 0, 'config', 'config_shared', '0', 0),
(4738, 0, 'config', 'config_encryption', 'NjectGz4DASXmLdV4mjwkUvXBNz3QHvtMEH09IOx16YhJD0Zo8t6xTca9HhojRoieMnpZZW3vvOCiVDQmDP2wBRbWgWO6YGyQRmbB1FQMAZWBjr02J4N04L6R3rmXScrVcSiyPfR4Hz9UqCJrfKa7ySFg7PPWiXELIvkA2C0AHC2MfMBp0vNNUl6FOQZOrhcg7kUx2sRwAyqBqp3GmrcRFnlELocNuaFr8nMnUEEttKy8lxQ4iCeVTtO7zO8fDGVML6Zdpfwxxc9hoyKKPOvD5oXRHrEJ1W8sxgCLwhUZUDYrOwW9vBLZfxmPP4D4n9errqP5yQGIcFzzcSJhz38E8Ok06sbAo3oJX6GyvDsr7rUzJX4ly6GVcYdlxioCi6D3R2mHsYrfJrI7EqSlFODtocydwE0OJEP5X9O6iVt23McV1DpmJORjeTGu9P2MN826EPkRcVMKKM3Tt43klt6VEkGACWXio5KxSzSsqsfrobXxfjAI4oXiEYZqLIr5cPXue3FuNMsBswnOiDLzKqLoTyhnFUmGVDmfbNn3AdHEHWbwyKDQRx9ZejsSK3iB3STLnEF20f1Is6Zdof7VLFCzabgdzMR9cajsXxlqdzg4LHxs1MTzjpNfNHa7m2B024ujG51GKWkFtyOjvWbBPxSLfef6hZalhLyhyVW2I7e6HmLuDWio6BH3wYOTDmM2MyPJKNUddIp0q5YXSN61cCDxJ8NPre0RhrghRbPB95eGMmTP7LXOdVEwxMX0ebNR83LzQ2WRwwy6yXAuvuF0WNpVpUvjs26PHaQFUIvnKR7B6WD6QXbvBqGY2BZfwqXzQTSAteQXRvCWNqXsyEIarEggGteWeAjOLepK2SGibSfZ3pWCj0lJZfbXij9EHnIXF4otY9ea8JTaZNhnIgUMYJ0H9dGN2TbkvVe77qzFAUQceYOVgBrcVkgwpilJUhpoxrDakbZTEiOCJxNbpp6KEcrpCX8DV1MMskTaKGZ66okQhhijYgEF2AmNlLi8VOLaDrS', 0),
(4739, 0, 'config', 'config_file_max_size', '300000', 0),
(4740, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(4741, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(4742, 0, 'config', 'config_error_display', '1', 0),
(4743, 0, 'config', 'config_error_log', '1', 0),
(4744, 0, 'config', 'config_error_filename', 'error.log', 0),
(1832, 0, 'kingstorepro_skin', 'kingstorepro_skin', 'kingstorelitedefault', 0),
(1837, 0, 'tg_kspro_revolution_slider', 'tg_kspro_revolution_slider_module', '[{"slider_id":"1","layout_id":"1","position":"header_top","sort_order":"","status":"1"}]', 1),
(4369, 0, 'tg_kspro_filter_product', 'tg_kspro_filter_product_module', '{"1":{"tabs":{"1":{"heading":{"1":"\\u041c\\u0443\\u0436\\u0441\\u043a\\u0438\\u0435 \\u0447\\u0430\\u0441\\u044b","2":"Men''s Watch"},"title":"category","product":"","products":"","category":"","categories":"61"},"2":{"heading":{"1":"\\u0416\\u0435\\u043d\\u0441\\u043a\\u0438\\u0435 \\u0447\\u0430\\u0441\\u044b","2":"Ladies Watch"},"title":"category","product":"","products":"42,30,47,28,41,40,48,36,34","category":"","categories":"62"}},"carousel":"1","width":"170","height":"170","itemsperpage":"6","cols":"1","limit":"12","layout_id":"1","position":"top_content","status":"1","sort_order":"0"},"2":{"tabs":{"3":{"heading":{"1":"\\u0421\\u043a\\u0438\\u0434\\u043a\\u0438 \\u0434\\u043d\\u044f","2":"Daily deals"},"title":"products","product":"","products":"50,51,57,55,56,69","category":"","categories":""}},"carousel":"1","width":"170","height":"170","itemsperpage":"4","cols":"1","limit":"8","layout_id":"1","position":"top_content","status":"1","sort_order":"1"},"3":{"tabs":{"4":{"heading":{"1":"\\u041d\\u041e\\u0412\\u041e\\u0415 \\u043f\\u043e\\u0441\\u0442\\u0443\\u043f\\u043b\\u0435\\u043d\\u0438\\u0435","2":"NEW ARRIVALS"},"title":"latest","product":"","products":"","category":"","categories":""}},"carousel":"1","width":"170","height":"170","itemsperpage":"3","cols":"1","limit":"9","layout_id":"1","position":"content_top","status":"1","sort_order":"0"}}', 1),
(4733, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai''hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(4732, 0, 'config', 'config_seo_url_postfix', '', 0),
(4731, 0, 'config', 'config_seo_url_include_path', '0', 0),
(4730, 0, 'config', 'config_seo_url_type', 'seo_url', 0),
(4729, 0, 'config', 'config_seo_url', '0', 0),
(4728, 0, 'config', 'config_maintenance', '0', 0),
(4727, 0, 'config', 'config_sms_gate_password', '', 0),
(4726, 0, 'config', 'config_sms_gate_username', '', 0),
(4725, 0, 'config', 'config_sms_message', '', 0),
(4724, 0, 'config', 'config_sms_copy', '', 0),
(4723, 0, 'config', 'config_sms_to', '+77053331349', 0),
(4721, 0, 'config', 'config_sms_gatename', 'smsc', 0),
(4722, 0, 'config', 'config_sms_from', '', 0),
(4720, 0, 'config', 'config_sms_alert', '1', 0),
(4719, 0, 'config', 'config_mail_alert', '', 0),
(4718, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(4717, 0, 'config', 'config_mail_smtp_port', '25', 0),
(4716, 0, 'config', 'config_mail_smtp_password', '', 0),
(4715, 0, 'config', 'config_mail_smtp_username', '', 0),
(4714, 0, 'config', 'config_mail_smtp_hostname', '', 0),
(4713, 0, 'config', 'config_mail_parameter', '', 0),
(4712, 0, 'config', 'config_mail_protocol', 'mail', 0),
(4711, 0, 'config', 'config_ftp_status', '1', 0),
(4710, 0, 'config', 'config_ftp_root', '', 0),
(4707, 0, 'config', 'config_ftp_port', '21', 0),
(4708, 0, 'config', 'config_ftp_username', 'u90259579', 0),
(4709, 0, 'config', 'config_ftp_password', 'woF0qSKW7oy1!', 0),
(4706, 0, 'config', 'config_ftp_hostname', 'home695159009.1and1-data.host', 0),
(4705, 0, 'config', 'config_image_location_height', '50', 0),
(4704, 0, 'config', 'config_image_location_width', '268', 0),
(4703, 0, 'config', 'config_image_cart_height', '47', 0),
(4700, 0, 'config', 'config_image_wishlist_width', '47', 0),
(4702, 0, 'config', 'config_image_cart_width', '47', 0),
(4701, 0, 'config', 'config_image_wishlist_height', '47', 0),
(4699, 0, 'config', 'config_image_compare_height', '90', 0),
(4698, 0, 'config', 'config_image_compare_width', '90', 0),
(4697, 0, 'config', 'config_image_related_height', '170', 0),
(4696, 0, 'config', 'config_image_related_width', '170', 0),
(4695, 0, 'config', 'config_image_additional_height', '74', 0),
(4694, 0, 'config', 'config_image_additional_width', '74', 0),
(4693, 0, 'config', 'config_image_product_height', '170', 0),
(4692, 0, 'config', 'config_image_product_width', '170', 0),
(4691, 0, 'config', 'config_image_popup_height', '500', 0),
(4689, 0, 'config', 'config_image_thumb_height', '400', 0),
(4690, 0, 'config', 'config_image_popup_width', '500', 0),
(4688, 0, 'config', 'config_image_thumb_width', '400', 0),
(4687, 0, 'config', 'config_image_category_height', '170', 0),
(4686, 0, 'config', 'config_image_category_width', '170', 0),
(4685, 0, 'config', 'config_icon', 'catalog/logo_new.png', 0),
(4682, 0, 'config', 'config_captcha', 'google_captcha', 0),
(4683, 0, 'config', 'config_captcha_page', '["review","return","contact"]', 1),
(4684, 0, 'config', 'config_logo', 'catalog/logo_new_2.png', 0),
(4680, 0, 'config', 'config_return_id', '3', 0),
(4681, 0, 'config', 'config_return_status_id', '2', 0),
(4679, 0, 'config', 'config_affiliate_mail', '0', 0),
(4675, 0, 'config', 'config_affiliate_approval', '0', 0),
(4676, 0, 'config', 'config_affiliate_auto', '0', 0),
(4677, 0, 'config', 'config_affiliate_commission', '5', 0),
(4678, 0, 'config', 'config_affiliate_id', '4', 0),
(4674, 0, 'config', 'config_stock_checkout', '0', 0),
(4673, 0, 'config', 'config_stock_warning', '0', 0),
(4672, 0, 'config', 'config_stock_display', '0', 0),
(4671, 0, 'config', 'config_api_id', '1', 0),
(4670, 0, 'config', 'config_order_mail', '1', 0),
(4669, 0, 'config', 'config_fraud_status_id', '16', 0),
(4668, 0, 'config', 'config_complete_status', '["3","5"]', 1),
(4667, 0, 'config', 'config_processing_status', '["2","3","1","12","5"]', 1),
(4666, 0, 'config', 'config_order_status_id', '1', 0),
(4662, 0, 'config', 'config_invoice_prefix', 'INV-2013-00', 0),
(4663, 0, 'config', 'config_cart_weight', '0', 0),
(4664, 0, 'config', 'config_checkout_guest', '1', 0),
(4665, 0, 'config', 'config_checkout_id', '3', 0),
(4312, 0, 'buy', 'buy_h2_ru', 'Оформить заказ ↓', 0),
(4311, 0, 'buy', 'buy_h1_ru', 'Корзина / Оформление заказа', 0),
(4310, 0, 'buy', 'buy_meta_title_ru', 'Корзина / Оформление заказа', 0),
(4309, 0, 'buy', 'buy_comment_required', '0', 0),
(4308, 0, 'buy', 'buy_comment_status', '0', 0),
(4307, 0, 'buy', 'buy_country_required', '0', 0),
(4306, 0, 'buy', 'buy_country_status', '0', 0),
(4305, 0, 'buy', 'buy_postcode_required', '0', 0),
(4304, 0, 'buy', 'buy_postcode_status', '0', 0),
(4303, 0, 'buy', 'buy_city_required', '0', 0),
(4302, 0, 'buy', 'buy_city_status', '0', 0),
(4301, 0, 'buy', 'buy_zone_required', '0', 0),
(4300, 0, 'buy', 'buy_zone_status', '0', 0),
(4299, 0, 'buy', 'buy_address_2_required', '0', 0),
(4298, 0, 'buy', 'buy_address_2_status', '0', 0),
(4297, 0, 'buy', 'buy_address_1_required', '0', 0),
(4296, 0, 'buy', 'buy_address_1_status', '0', 0),
(4295, 0, 'buy', 'buy_company_required', '0', 0),
(4294, 0, 'buy', 'buy_company_status', '0', 0),
(4293, 0, 'buy', 'buy_fax_required', '0', 0),
(4292, 0, 'buy', 'buy_fax_status', '0', 0),
(4291, 0, 'buy', 'buy_telephone_required', '1', 0),
(4289, 0, 'buy', 'buy_email_required', '1', 0),
(4290, 0, 'buy', 'buy_telephone_status', '1', 0),
(4288, 0, 'buy', 'buy_email_status', '1', 0),
(4287, 0, 'buy', 'buy_lastname_required', '0', 0),
(4286, 0, 'buy', 'buy_lastname_status', '1', 0),
(4285, 0, 'buy', 'buy_firstname_required', '1', 0),
(4284, 0, 'buy', 'buy_firstname_status', '1', 0),
(4282, 0, 'buy', 'buy_payment_select', '1', 0),
(4283, 0, 'buy', 'buy_telephone_mask', '', 0),
(4281, 0, 'buy', 'buy_shipping_select', '1', 0),
(4280, 0, 'buy', 'buy_form_design', '0', 0),
(4279, 0, 'buy', 'buy_cart_modules', '0', 0),
(4278, 0, 'buy', 'buy_min_order_sum', '', 0),
(4277, 0, 'buy', 'buy_status', '1', 0),
(4089, 0, 'bank_transfer', 'bank_transfer_bank1', 'тест', 0),
(4090, 0, 'bank_transfer', 'bank_transfer_bank2', 'test', 0),
(4091, 0, 'bank_transfer', 'bank_transfer_total', '', 0),
(4092, 0, 'bank_transfer', 'bank_transfer_order_status_id', '16', 0),
(4093, 0, 'bank_transfer', 'bank_transfer_geo_zone_id', '0', 0),
(4094, 0, 'bank_transfer', 'bank_transfer_status', '1', 0),
(4095, 0, 'bank_transfer', 'bank_transfer_sort_order', '', 0),
(4261, 0, 'flat', 'flat_cost', '20.00', 0),
(4101, 0, 'pickup', 'pickup_geo_zone_id', '0', 0),
(4102, 0, 'pickup', 'pickup_status', '1', 0),
(4103, 0, 'pickup', 'pickup_sort_order', '', 0),
(4655, 0, 'config', 'config_customer_online', '0', 0),
(4656, 0, 'config', 'config_customer_group_id', '1', 0),
(4657, 0, 'config', 'config_customer_group_display', '["1"]', 1),
(4658, 0, 'config', 'config_customer_price', '0', 0),
(4659, 0, 'config', 'config_login_attempts', '5', 0),
(4660, 0, 'config', 'config_account_id', '3', 0),
(4661, 0, 'config', 'config_account_mail', '1', 0),
(4654, 0, 'config', 'config_tax_customer', 'shipping', 0),
(4653, 0, 'config', 'config_tax_default', 'shipping', 0),
(4652, 0, 'config', 'config_tax', '0', 0),
(4651, 0, 'config', 'config_voucher_max', '1000', 0),
(4650, 0, 'config', 'config_voucher_min', '1', 0),
(4649, 0, 'config', 'config_review_mail', '1', 0),
(4648, 0, 'config', 'config_review_guest', '1', 0),
(4647, 0, 'config', 'config_review_status', '1', 0),
(4646, 0, 'config', 'config_limit_admin', '20', 0),
(4645, 0, 'config', 'config_product_description_length', '100', 0),
(4644, 0, 'config', 'config_product_limit', '15', 0),
(4642, 0, 'config', 'config_weight_class_id', '1', 0),
(4643, 0, 'config', 'config_product_count', '0', 0),
(4641, 0, 'config', 'config_length_class_id', '1', 0),
(4638, 0, 'config', 'config_admin_language', 'ru', 0),
(4639, 0, 'config', 'config_currency', 'USD', 0),
(4640, 0, 'config', 'config_currency_auto', '1', 0),
(4635, 0, 'config', 'config_country_id', '109', 0),
(4636, 0, 'config', 'config_zone_id', '1717', 0),
(4637, 0, 'config', 'config_language', 'ru', 0),
(4633, 0, 'config', 'config_open', 'Каждый день\r\n10:00 - 20:00', 0),
(4634, 0, 'config', 'config_comment', '', 0),
(4631, 0, 'config', 'config_fax', '', 0),
(4632, 0, 'config', 'config_image', 'catalog/logo_new.png', 0),
(4259, 0, 'pim', 'pim_width', '800', 0),
(4258, 0, 'pim', 'pim_language', 'ru', 0),
(4257, 0, 'pim', 'pim_uploadMaxType', 'M', 0),
(4256, 0, 'pim', 'pim_uploadMaxSize', '999', 0),
(4255, 0, 'pim', 'pim_uploadOverwrite', '0', 0),
(4254, 0, 'pim', 'pim_copyOverwrite', '0', 0),
(4253, 0, 'pim', 'pim_deletedef', '0', 0),
(4252, 0, 'pim', 'pim_miu', '0', 0),
(4251, 0, 'pim', 'pim_status', '1', 0),
(4239, 0, 'pim_volumes', 'pim_volumes', '{"FTP":{"FTP":{"alias":"FTP","host":"home695159009.1and1-data.host","port":"21","user":"u90259579","pass":"woF0qSKW7oy1!","path":"\\/image\\/catalog\\/","URL":"","status":"1"}}}', 1),
(4357, 0, 'shoputils_ik', 'shoputils_ik_order_fail_status_id', '10', 0),
(4354, 0, 'shoputils_ik', 'shoputils_ik_maximal_order', '', 0),
(4355, 0, 'shoputils_ik', 'shoputils_ik_order_confirm_status_id', '2', 0),
(4356, 0, 'shoputils_ik', 'shoputils_ik_order_status_id', '2', 0),
(4353, 0, 'shoputils_ik', 'shoputils_ik_minimal_order', '', 0),
(4352, 0, 'shoputils_ik', 'shoputils_ik_sort_order', '', 0),
(4260, 0, 'pim', 'pim_height', '400', 0),
(4351, 0, 'shoputils_ik', 'shoputils_ik_status', '1', 0),
(4313, 0, 'buy', 'buy_form_headings', '0', 0),
(4314, 0, 'buy', 'buy_heading_1_ru', 'Информация', 0),
(4315, 0, 'buy', 'buy_heading_2_ru', 'Адрес', 0),
(4316, 0, 'buy', 'buy_heading_3_ru', 'Доставка и оплата', 0),
(4317, 0, 'buy', 'buy_meta_title_en', 'Корзина / Оформление заказа', 0),
(4318, 0, 'buy', 'buy_h1_en', 'Корзина / Оформление заказа', 0),
(4319, 0, 'buy', 'buy_h2_en', 'Оформить заказ ↓', 0),
(4320, 0, 'buy', 'buy_heading_1_en', 'Информация', 0),
(4321, 0, 'buy', 'buy_heading_2_en', 'Адрес', 0),
(4322, 0, 'buy', 'buy_heading_3_en', 'Доставка и оплата', 0),
(4630, 0, 'config', 'config_telephone', '+7 705 333 13 49', 0),
(4629, 0, 'config', 'config_email', 'sadykovkayrat81@gmail.com', 0),
(4628, 0, 'config', 'config_geocode', '', 0),
(4358, 0, 'shoputils_ik', 'shoputils_ik_langdata', '{"1":{"title":"\\u0418\\u043d\\u0442\\u0435\\u0440\\u043a\\u0430\\u0441\\u0441\\u0430","instruction":""},"2":{"title":"\\u0418\\u043d\\u0442\\u0435\\u0440\\u043a\\u0430\\u0441\\u0441\\u0430","instruction":""}}', 1),
(4359, 0, 'shoputils_ik', 'shoputils_ik_shop_id', '599eb0123b1eafd37d8b456b', 0),
(4360, 0, 'shoputils_ik', 'shoputils_ik_sign_hash', 'fjrEazRrfdkkalTt', 0),
(4361, 0, 'shoputils_ik', 'shoputils_ik_sign_test_key', 'rNQuUPJNCsnifETI', 0),
(4362, 0, 'shoputils_ik', 'shoputils_ik_test_mode', '1', 0),
(4363, 0, 'shoputils_ik', 'shoputils_ik_currency', 'USD', 0),
(4364, 0, 'shoputils_ik', 'shoputils_ik_lifetime', '5', 0),
(4365, 0, 'shoputils_ik', 'shoputils_ik_log_filename', 'shoputils_ik.log', 0),
(4366, 0, 'shoputils_ik', 'shoputils_ik_version', '3.3.ik', 0),
(4367, 0, 'shoputils_ik', 'shoputils_ik_log', '2', 0),
(4627, 0, 'config', 'config_address', 'ИП &quot;Led Vision&quot;\r\nг. Алматы, Валиханова 83/9\r\nКазахстан\r\nИНН 600604401787', 0),
(4621, 0, 'config', 'config_meta_description', '', 0),
(4622, 0, 'config', 'config_meta_keyword', '', 0),
(4623, 0, 'config', 'config_template', 'kingstorepro', 0),
(4624, 0, 'config', 'config_layout_id', '4', 0),
(4625, 0, 'config', 'config_name', 'Luxossorize', 0),
(4626, 0, 'config', 'config_owner', 'Динара', 0),
(4620, 0, 'config', 'config_meta_title', 'Luxossorize - интернет магазин', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_stock_status`
--

CREATE TABLE IF NOT EXISTS `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`stock_status_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'В наличии'),
(8, 1, 'Предзаказ'),
(5, 1, 'Нет в наличии'),
(6, 1, 'Ожидание 2-3 дня'),
(7, 2, 'In Stock'),
(8, 2, 'Pre-Order'),
(5, 2, 'Out Of Stock'),
(6, 2, '2-3 Days');

-- --------------------------------------------------------

--
-- Table structure for table `oc_store`
--

CREATE TABLE IF NOT EXISTS `oc_store` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_class`
--

CREATE TABLE IF NOT EXISTS `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`tax_class_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate`
--

CREATE TABLE IF NOT EXISTS `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`tax_rate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate_to_customer_group`
--

CREATE TABLE IF NOT EXISTS `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  PRIMARY KEY (`tax_rate_id`,`customer_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rule`
--

CREATE TABLE IF NOT EXISTS `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tax_rule_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_tg_kspro_revolution_slider`
--

CREATE TABLE IF NOT EXISTS `oc_tg_kspro_revolution_slider` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text,
  `settings` text,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `oc_tg_kspro_revolution_slider`
--

INSERT INTO `oc_tg_kspro_revolution_slider` (`id`, `name`, `settings`, `content`) VALUES
(1, 'Главный слайдер', 'a:4:{s:12:"slider_width";s:3:"620";s:13:"slider_height";s:3:"516";s:12:"slider_speed";s:1:"9";s:11:"layout_type";s:1:"0";}', 'a:3:{i:1;a:2:{i:1;a:4:{s:6:"status";s:1:"1";s:6:"slider";s:102:"catalog/MainSlider/cartier-ballon-bleu-Most-luxurious-Watch-Brands-at-BaselWorld-2014-mydesignweek.jpg";s:17:"transition_effect";s:6:"random";s:4:"link";s:0:"";}i:2;a:4:{s:6:"status";s:1:"1";s:6:"slider";s:102:"catalog/MainSlider/cartier-ballon-bleu-Most-luxurious-Watch-Brands-at-BaselWorld-2014-mydesignweek.jpg";s:17:"transition_effect";s:6:"random";s:4:"link";s:0:"";}}i:2;a:2:{i:1;a:4:{s:6:"status";s:1:"1";s:6:"slider";s:53:"catalog/MainSlider/Blancpain-X-Fathoms-Dive-Watch.jpg";s:17:"transition_effect";s:6:"random";s:4:"link";s:0:"";}i:2;a:4:{s:6:"status";s:1:"1";s:6:"slider";s:53:"catalog/MainSlider/Blancpain-X-Fathoms-Dive-Watch.jpg";s:17:"transition_effect";s:6:"random";s:4:"link";s:0:"";}}i:3;a:2:{i:1;a:4:{s:6:"status";s:1:"1";s:6:"slider";s:53:"catalog/MainSlider/Top-5-of-Luxury-Watch-Brands-5.jpg";s:17:"transition_effect";s:6:"random";s:4:"link";s:0:"";}i:2;a:4:{s:6:"status";s:1:"1";s:6:"slider";s:53:"catalog/MainSlider/Top-5-of-Luxury-Watch-Brands-5.jpg";s:17:"transition_effect";s:6:"random";s:4:"link";s:0:"";}}}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_upload`
--

CREATE TABLE IF NOT EXISTS `oc_upload` (
  `upload_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`upload_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_url_alias`
--

CREATE TABLE IF NOT EXISTS `oc_url_alias` (
  `url_alias_id` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `seomanager` int(1) DEFAULT '0',
  PRIMARY KEY (`url_alias_id`),
  KEY `query` (`query`),
  KEY `keyword` (`keyword`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1108 ;

--
-- Dumping data for table `oc_url_alias`
--

INSERT INTO `oc_url_alias` (`url_alias_id`, `query`, `keyword`, `seomanager`) VALUES
(1105, 'information_id=4', 'about_us', 0),
(934, 'category_id=60', 'aksesuari', 0),
(933, 'category_id=59', 'zazhim-dlja-deneg', 0),
(913, 'category_id=24', 'pens', 0),
(935, 'category_id=61', 'muzhskie-chasi', 0),
(925, 'news/headlines', 'blogspage', 0),
(1087, 'product_id=50', 'richard-mille-rm-038-7670171', 0),
(1099, 'information_id=6', 'delivery', 0),
(981, 'information_id=3', 'privacy', 0),
(1100, 'information_id=5', 'terms', 0),
(898, 'information/contact', 'contacts', 1),
(899, 'information/sitemap', 'sitemaps', 1),
(900, 'product/manufacturer', 'brands', 1),
(901, 'product/special', 'specials', 1),
(903, 'blog_category_id=71', 'anonsi', 0),
(904, 'blog_category_id=70', 'obzori', 0),
(905, 'article_id=123', 'obzor-pervij', 0),
(908, 'article_id=124', 'vazhnaja-statja', 0),
(907, 'article_id=125', 'pervaja-novost', 0),
(920, 'category_id=20', 'chasi', 0),
(936, 'category_id=62', 'zhenskie-chasi', 0),
(938, 'category_id=63', 'portmone', 0),
(939, 'category_id=64', 'sumki', 0),
(940, 'category_id=65', 'remni', 0),
(1084, 'product_id=52', 'rolex-explorer', 0),
(1103, 'product_id=53', 'richard-mille-watches-rm-058', 0),
(1082, 'product_id=54', 'boatprecious', 0),
(1052, 'product_id=55', 'audemars-piguet-royal-oak', 0),
(1054, 'product_id=56', 'cartier-ballon-bleu-de-cartier', 0),
(1053, 'product_id=57', 'breguet-classique-grande', 0),
(1083, 'product_id=58', 'zenith-pilot', 0),
(1102, 'product_id=59', 'franck-muller-double-mystery', 0),
(1042, 'product_id=60', 'rolex-day-date', 0),
(1107, 'product_id=61', 'versace', 0),
(1065, 'product_id=62', 'montblanc', 0),
(1066, 'product_id=63', 'montblanc1', 0),
(1063, 'product_id=64', 'louis-vuitton-', 0),
(1064, 'product_id=65', 'louis-vuitton-2', 0),
(1060, 'product_id=66', 'gucci-1', 0),
(1061, 'product_id=67', 'gucci-2', 0),
(1057, 'product_id=68', 'bvlgari-wlgqb02017', 0),
(1095, 'product_id=69', 'chopard-415-566-4156', 0),
(1094, 'product_id=70', 'gucci-018sl0119tnd', 0),
(1101, 'product_id=71', 'chopard2', 0),
(1092, 'product_id=72', 'omega', 0),
(1067, 'product_id=73', 'penmontblanc', 0),
(1068, 'product_id=74', 'penmontblanc2', 0),
(1069, 'product_id=75', 'montblancpen3', 0),
(1070, 'product_id=76', 'penmontblanc4', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_user`
--

CREATE TABLE IF NOT EXISTS `oc_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', '77469607b9d1155b797762aa4c9601d59fcc7692', 'uCDo3Jw9Y', 'John', 'Doe', 'sadykovkayrat81@gmail.com', '', '', '5.76.247.22', 1, '2017-08-18 09:22:34'),
(2, 1, 'dinara', 'e85a4444dcf6dffcb4257b41f3a80738021aa531', 'EgG1TNH1V', 'Dinara', 'Dinara', '', '', '', '178.88.2.55', 1, '2017-08-23 09:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `oc_user_group`
--

CREATE TABLE IF NOT EXISTS `oc_user_group` (
  `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Администратор', '{"access":["analytics\\/google_analytics","blog\\/article","blog\\/category","blog\\/review","blog\\/setting","captcha\\/basic_captcha","captcha\\/google_captcha","catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","common\\/column_left","common\\/filemanager","common\\/menu","common\\/profile","common\\/stats","customer\\/custom_field","customer\\/customer","customer\\/customer_group","design\\/banner","design\\/layout","design\\/menu","extension\\/analytics","extension\\/captcha","extension\\/feed","extension\\/fraud","extension\\/installer","extension\\/modification","extension\\/module","extension\\/openbay","extension\\/payment","extension\\/shipping","extension\\/total","feed\\/google_base","feed\\/google_sitemap","feed\\/openbaypro","feed\\/yandex_market","fraud\\/fraudlabspro","fraud\\/ip","fraud\\/maxmind","howto\\/blogun","howto\\/gogettop","howto\\/howtosms","howto\\/seopult","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","marketing\\/affiliate","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","module\\/account","module\\/affiliate","module\\/amazon_login","module\\/amazon_pay","module\\/banner","module\\/bestseller","module\\/blog_category","module\\/blog_featured","module\\/blog_latest","module\\/cachemanager","module\\/carousel","module\\/category","module\\/category4level","module\\/ebay_listing","module\\/featured","module\\/filter","module\\/google_hangouts","module\\/html","module\\/imgcategory","module\\/information","module\\/latest","module\\/manufacturer","module\\/popular","module\\/pp_button","module\\/pp_login","module\\/product_tab","module\\/slideshow","module\\/special","module\\/store","openbay\\/amazon","openbay\\/amazon_listing","openbay\\/amazon_product","openbay\\/amazonus","openbay\\/amazonus_listing","openbay\\/amazonus_product","openbay\\/ebay","openbay\\/ebay_profile","openbay\\/ebay_template","openbay\\/etsy","openbay\\/etsy_product","openbay\\/etsy_shipping","openbay\\/etsy_shop","payment\\/amazon_login_pay","payment\\/authorizenet_aim","payment\\/authorizenet_sim","payment\\/bank_transfer","payment\\/bluepay_hosted","payment\\/bluepay_redirect","payment\\/cheque","payment\\/cod","payment\\/firstdata","payment\\/firstdata_remote","payment\\/free_checkout","payment\\/g2apay","payment\\/globalpay","payment\\/globalpay_remote","payment\\/klarna_account","payment\\/klarna_invoice","payment\\/liqpay","payment\\/nochex","payment\\/paymate","payment\\/paypoint","payment\\/payza","payment\\/perpetual_payments","payment\\/pp_express","payment\\/pp_payflow","payment\\/pp_payflow_iframe","payment\\/pp_pro","payment\\/pp_pro_iframe","payment\\/pp_standard","payment\\/realex","payment\\/realex_remote","payment\\/sagepay_direct","payment\\/sagepay_server","payment\\/sagepay_us","payment\\/securetrading_pp","payment\\/securetrading_ws","payment\\/skrill","payment\\/twocheckout","payment\\/web_payment_software","payment\\/worldpay","report\\/affiliate","report\\/affiliate_activity","report\\/affiliate_login","report\\/customer_activity","report\\/customer_credit","report\\/customer_login","report\\/customer_online","report\\/customer_order","report\\/customer_reward","report\\/marketing","report\\/product_purchased","report\\/product_viewed","report\\/sale_coupon","report\\/sale_order","report\\/sale_return","report\\/sale_shipping","report\\/sale_tax","sale\\/order","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","search\\/search","setting\\/setting","setting\\/store","shipping\\/auspost","shipping\\/citylink","shipping\\/fedex","shipping\\/flat","shipping\\/free","shipping\\/item","shipping\\/parcelforce_48","shipping\\/pickup","shipping\\/royal_mail","shipping\\/ups","shipping\\/usps","shipping\\/weight","tool\\/backup","tool\\/error_log","tool\\/seomanager","tool\\/upload","total\\/coupon","total\\/credit","total\\/handling","total\\/klarna_fee","total\\/low_order_fee","total\\/reward","total\\/shipping","total\\/sub_total","total\\/tax","total\\/total","total\\/voucher","user\\/api","user\\/user","user\\/user_permission","payment\\/bank_transfer","captcha\\/google_captcha","module\\/filter","module\\/manufacturerpercategory","module\\/manufacturer_category","catalog\\/category_mgr_lite","payment\\/w1","module\\/mega_filter","module\\/tg_kspro_settings","module\\/tg_kspro_revolution_slider","module\\/tg_kspro_megamenu","module\\/tg_kspro_megamenu_vertical","module\\/tg_kspro_filter_product","module\\/html","module\\/ncategory","module\\/news","module\\/newsletter","catalog\\/nauthor","catalog\\/ncategory","catalog\\/ncomments","catalog\\/news","common\\/newspanel","extension\\/newsletter","feed\\/articles_google_base","feed\\/articles_google_sitemap","module\\/mega_filter","module\\/d_quickcheckout","module\\/d_quickcheckout","module\\/buy","module\\/buy","module\\/buy","shipping\\/pickup","shipping\\/fedex","module\\/pim","payment\\/liqpay","payment\\/liqpay","payment\\/skrill","payment\\/w1","payment\\/okpay","payment\\/shoputils_ik"],"modify":["analytics\\/google_analytics","blog\\/article","blog\\/category","blog\\/review","blog\\/setting","captcha\\/basic_captcha","captcha\\/google_captcha","catalog\\/attribute","catalog\\/attribute_group","catalog\\/category","catalog\\/download","catalog\\/filter","catalog\\/information","catalog\\/manufacturer","catalog\\/option","catalog\\/product","catalog\\/recurring","catalog\\/review","common\\/column_left","common\\/filemanager","common\\/menu","common\\/profile","common\\/stats","customer\\/custom_field","customer\\/customer","customer\\/customer_group","design\\/banner","design\\/layout","design\\/menu","extension\\/analytics","extension\\/captcha","extension\\/feed","extension\\/fraud","extension\\/installer","extension\\/modification","extension\\/module","extension\\/openbay","extension\\/payment","extension\\/shipping","extension\\/total","feed\\/google_base","feed\\/google_sitemap","feed\\/openbaypro","feed\\/yandex_market","fraud\\/fraudlabspro","fraud\\/ip","fraud\\/maxmind","howto\\/blogun","howto\\/gogettop","howto\\/howtosms","howto\\/seopult","localisation\\/country","localisation\\/currency","localisation\\/geo_zone","localisation\\/language","localisation\\/length_class","localisation\\/location","localisation\\/order_status","localisation\\/return_action","localisation\\/return_reason","localisation\\/return_status","localisation\\/stock_status","localisation\\/tax_class","localisation\\/tax_rate","localisation\\/weight_class","localisation\\/zone","marketing\\/affiliate","marketing\\/contact","marketing\\/coupon","marketing\\/marketing","module\\/account","module\\/affiliate","module\\/amazon_login","module\\/amazon_pay","module\\/banner","module\\/bestseller","module\\/blog_category","module\\/blog_featured","module\\/blog_latest","module\\/cachemanager","module\\/carousel","module\\/category","module\\/category4level","module\\/ebay_listing","module\\/featured","module\\/filter","module\\/google_hangouts","module\\/html","module\\/imgcategory","module\\/information","module\\/latest","module\\/manufacturer","module\\/popular","module\\/pp_button","module\\/pp_login","module\\/product_tab","module\\/slideshow","module\\/special","module\\/store","openbay\\/amazon","openbay\\/amazon_listing","openbay\\/amazon_product","openbay\\/amazonus","openbay\\/amazonus_listing","openbay\\/amazonus_product","openbay\\/ebay","openbay\\/ebay_profile","openbay\\/ebay_template","openbay\\/etsy","openbay\\/etsy_product","openbay\\/etsy_shipping","openbay\\/etsy_shop","payment\\/amazon_login_pay","payment\\/authorizenet_aim","payment\\/authorizenet_sim","payment\\/bank_transfer","payment\\/bluepay_hosted","payment\\/bluepay_redirect","payment\\/cheque","payment\\/cod","payment\\/firstdata","payment\\/firstdata_remote","payment\\/free_checkout","payment\\/g2apay","payment\\/globalpay","payment\\/globalpay_remote","payment\\/klarna_account","payment\\/klarna_invoice","payment\\/liqpay","payment\\/nochex","payment\\/paymate","payment\\/paypoint","payment\\/payza","payment\\/perpetual_payments","payment\\/pp_express","payment\\/pp_payflow","payment\\/pp_payflow_iframe","payment\\/pp_pro","payment\\/pp_pro_iframe","payment\\/pp_standard","payment\\/realex","payment\\/realex_remote","payment\\/sagepay_direct","payment\\/sagepay_server","payment\\/sagepay_us","payment\\/securetrading_pp","payment\\/securetrading_ws","payment\\/skrill","payment\\/twocheckout","payment\\/web_payment_software","payment\\/worldpay","report\\/affiliate","report\\/affiliate_activity","report\\/affiliate_login","report\\/customer_activity","report\\/customer_credit","report\\/customer_login","report\\/customer_online","report\\/customer_order","report\\/customer_reward","report\\/marketing","report\\/product_purchased","report\\/product_viewed","report\\/sale_coupon","report\\/sale_order","report\\/sale_return","report\\/sale_shipping","report\\/sale_tax","sale\\/order","sale\\/recurring","sale\\/return","sale\\/voucher","sale\\/voucher_theme","search\\/search","setting\\/setting","setting\\/store","shipping\\/auspost","shipping\\/citylink","shipping\\/fedex","shipping\\/flat","shipping\\/free","shipping\\/item","shipping\\/parcelforce_48","shipping\\/pickup","shipping\\/royal_mail","shipping\\/ups","shipping\\/usps","shipping\\/weight","tool\\/backup","tool\\/error_log","tool\\/seomanager","tool\\/upload","total\\/coupon","total\\/credit","total\\/handling","total\\/klarna_fee","total\\/low_order_fee","total\\/reward","total\\/shipping","total\\/sub_total","total\\/tax","total\\/total","total\\/voucher","user\\/api","user\\/user","user\\/user_permission","payment\\/bank_transfer","captcha\\/google_captcha","module\\/filter","module\\/manufacturerpercategory","module\\/manufacturer_category","catalog\\/category_mgr_lite","payment\\/w1","module\\/mega_filter","module\\/tg_kspro_settings","module\\/tg_kspro_revolution_slider","module\\/tg_kspro_megamenu","module\\/tg_kspro_megamenu_vertical","module\\/tg_kspro_filter_product","module\\/html","module\\/ncategory","module\\/news","module\\/newsletter","catalog\\/nauthor","catalog\\/ncategory","catalog\\/ncomments","catalog\\/news","common\\/newspanel","extension\\/newsletter","feed\\/articles_google_base","feed\\/articles_google_sitemap","module\\/mega_filter","module\\/d_quickcheckout","module\\/d_quickcheckout","module\\/buy","module\\/buy","module\\/buy","shipping\\/pickup","shipping\\/fedex","module\\/pim","payment\\/liqpay","payment\\/liqpay","payment\\/skrill","payment\\/w1","payment\\/okpay","payment\\/shoputils_ik"]}'),
(10, 'Демонстрация', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher`
--

CREATE TABLE IF NOT EXISTS `oc_voucher` (
  `voucher_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`voucher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_history`
--

CREATE TABLE IF NOT EXISTS `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`voucher_history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme`
--

CREATE TABLE IF NOT EXISTS `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`voucher_theme_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme_description`
--

CREATE TABLE IF NOT EXISTS `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`voucher_theme_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Рождество'),
(7, 1, 'День рождения'),
(8, 1, 'Общий');

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class`
--

CREATE TABLE IF NOT EXISTS `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  PRIMARY KEY (`weight_class_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class_description`
--

CREATE TABLE IF NOT EXISTS `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  PRIMARY KEY (`weight_class_id`,`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Килограммы', 'кг'),
(2, 2, 'Kilogram', 'kg'),
(3, 1, 'Граммы', 'г'),
(4, 2, 'Gram', 'g'),
(5, 1, 'Фунты', 'lb'),
(6, 2, 'Pound', 'lb'),
(7, 1, 'Унции', 'oz'),
(7, 2, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone`
--

CREATE TABLE IF NOT EXISTS `oc_zone` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4232 ;

--
-- Dumping data for table `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M''Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu''a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Арагацотн', 'AGT', 1),
(181, 11, 'Арарат', 'ARR', 1),
(182, 11, 'Армавир', 'ARM', 1),
(183, 11, 'Гегаркуник', 'GEG', 1),
(184, 11, 'Котайк', 'KOT', 1),
(185, 11, 'Лори', 'LOR', 1),
(186, 11, 'Ширак', 'SHI', 1),
(187, 11, 'Сюник', 'SYU', 1),
(188, 11, 'Тавуш', 'TAV', 1),
(189, 11, 'Вайоц Дзор', 'VAY', 1),
(190, 11, 'Ереван', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', 1),
(202, 14, 'Ober&ouml;sterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore''s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Брест', 'BR', 1),
(338, 20, 'Гомель', 'HO', 1),
(339, 20, 'Минск', 'HM', 1),
(340, 20, 'Гродно', 'HR', 1),
(341, 20, 'Могилев', 'MA', 1),
(342, 20, 'Минская область', 'MI', 1),
(343, 20, 'Витебск', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George''s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith''s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M''Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O''Higgins', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1),
(848, 53, 'Grad Zagreb', 'GZ', 1),
(849, 53, 'Dubrovačko-neretvanska', 'DN', 1),
(850, 53, 'Istarska', 'IS', 1),
(851, 53, 'Karlovačka', 'KA', 1),
(852, 53, 'Koprivničko-križevačka', 'KK', 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1),
(854, 53, 'Ličko-senjska', 'LS', 1),
(855, 53, 'Međimurska', 'ME', 1),
(856, 53, 'Osječko-baranjska', 'OB', 1),
(857, 53, 'Požeško-slavonska', 'PS', 1),
(858, 53, 'Primorsko-goranska', 'PG', 1),
(859, 53, 'Šibensko-kninska', 'SK', 1),
(860, 53, 'Sisačko-moslavačka', 'SM', 1),
(861, 53, 'Brodsko-posavska', 'BP', 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1),
(863, 53, 'Varaždinska', 'VA', 1),
(864, 53, 'Virovitičko-podravska', 'VP', 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1),
(866, 53, 'Zadarska', 'ZA', 1),
(867, 53, 'Zagrebačka', 'ZG', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '''Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma''iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa''id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina''', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina''', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan lääni', 'AL', 1),
(1086, 72, 'Etelä-Suomen lääni', 'ES', 1),
(1087, 72, 'Itä-Suomen lääni', 'IS', 1),
(1088, 72, 'Länsi-Suomen lääni', 'LS', 1),
(1089, 72, 'Lapin lääni', 'LA', 1),
(1090, 72, 'Oulun lääni', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d''Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Th&uuml;ringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand''Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Puducherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'BoDeTaBek', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta Raya', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta''mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1),
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be''er Sheva', 'BS', 1),
(1613, 104, 'Bika''at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '''Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al ''Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa''', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa''', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma''an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Алматинская область', 'AL', 1),
(1717, 109, 'Алматы - город республ-го значения', 'AC', 1),
(1718, 109, 'Акмолинская область', 'AM', 1),
(1719, 109, 'Актюбинская область', 'AQ', 1),
(1720, 109, 'Астана - город республ-го значения', 'AS', 1),
(1721, 109, 'Атырауская область', 'AT', 1),
(1722, 109, 'Западно-Казахстанская область', 'BA', 1),
(1723, 109, 'Байконур - город республ-го значения', 'BY', 1),
(1724, 109, 'Мангистауская область', 'MA', 1),
(1725, 109, 'Южно-Казахстанская область', 'ON', 1),
(1726, 109, 'Павлодарская область', 'PA', 1),
(1727, 109, 'Карагандинская область', 'QA', 1),
(1728, 109, 'Костанайская область', 'QO', 1),
(1729, 109, 'Кызылординская область', 'QY', 1),
(1730, 109, 'Восточно-Казахстанская область', 'SH', 1),
(1731, 109, 'Северо-Казахстанская область', 'SO', 1),
(1732, 109, 'Жамбылская область', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P''yongan-bukto', 'PYB', 1),
(1769, 112, 'P''yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P''yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch''ungch''ong-bukto', 'CO', 1),
(1774, 113, 'Ch''ungch''ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch''on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t''ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al ''Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra''', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale''s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha''s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al ''Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati''', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'MY-01', 1),
(1972, 129, 'Kedah', 'MY-02', 1),
(1973, 129, 'Kelantan', 'MY-03', 1),
(1974, 129, 'Labuan', 'MY-15', 1),
(1975, 129, 'Melaka', 'MY-04', 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1),
(1977, 129, 'Pahang', 'MY-06', 1),
(1978, 129, 'Perak', 'MY-08', 1),
(1979, 129, 'Perlis', 'MY-09', 1),
(1980, 129, 'Pulau Pinang', 'MY-07', 1),
(1981, 129, 'Sabah', 'MY-12', 1),
(1982, 129, 'Sarawak', 'MY-13', 1),
(1983, 129, 'Selangor', 'MY-10', 1),
(1984, 129, 'Terengganu', 'MY-11', 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1),
(4035, 129, 'Putrajaya', 'MY-16', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord Brabant', 'NB', 1),
(2336, 150, 'Noord Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke''s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa''id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Абакан', 'AB', 1),
(2722, 176, 'Агинское', 'AG', 1),
(2723, 176, 'Анадырь', 'AN', 1),
(2724, 176, 'Архангельск', 'AR', 1),
(2725, 176, 'Астрахань', 'AS', 1),
(2726, 176, 'Барнаул', 'BA', 1),
(2727, 176, 'Белгород', 'BE', 1),
(2728, 176, 'Биробиджан', 'BI', 1),
(2729, 176, 'Благовещенск', 'BL', 1),
(2730, 176, 'Брянск', 'BR', 1),
(2731, 176, 'Чебоксары', 'CH', 1),
(2732, 176, 'Челябинск', 'CL', 1),
(2733, 176, 'Черкесск', 'CR', 1),
(2734, 176, 'Чита', 'CI', 1),
(2735, 176, 'Дудинка', 'DU', 1),
(2736, 176, 'Элиста', 'EL', 1),
(2738, 176, 'Горно-Алтайск', 'GA', 1),
(2739, 176, 'Грозный', 'GR', 1),
(2740, 176, 'Иркутск', 'IR', 1),
(2741, 176, 'Иваново', 'IV', 1),
(2742, 176, 'Ижевск', 'IZ', 1),
(2743, 176, 'Калининград', 'KA', 1),
(2744, 176, 'Калуга', 'KL', 1),
(2745, 176, 'Краснодар', 'KS', 1),
(2746, 176, 'Казань', 'KZ', 1),
(2747, 176, 'Кемерово', 'KE', 1),
(2748, 176, 'Хабаровск', 'KH', 1),
(2749, 176, 'Ханты-Мансийск', 'KM', 1),
(2750, 176, 'Кострома', 'KO', 1),
(2751, 176, 'Краснодар', 'KR', 1),
(2752, 176, 'Красноярск', 'KN', 1),
(2753, 176, 'Кудымкар', 'KU', 1),
(2754, 176, 'Курган', 'KG', 1),
(2755, 176, 'Курск', 'KK', 1),
(2756, 176, 'Кызыл', 'KY', 1),
(2757, 176, 'Липецк', 'LI', 1),
(2758, 176, 'Магадан', 'MA', 1),
(2759, 176, 'Махачкала', 'MK', 1),
(2760, 176, 'Майкоп', 'MY', 1),
(2761, 176, 'Москва', 'MO', 1),
(2762, 176, 'Мурманск', 'MU', 1),
(2763, 176, 'Нальчик', 'NA', 1),
(2764, 176, 'Нарьян-Мар', 'NR', 1),
(2765, 176, 'Назрань', 'NZ', 1),
(2766, 176, 'Нижний Новгород', 'NI', 1),
(2767, 176, 'Великий Новгород', 'NO', 1),
(2768, 176, 'Новосибирск', 'NV', 1),
(2769, 176, 'Омск', 'OM', 1),
(2770, 176, 'Орёл', 'OR', 1),
(2771, 176, 'Оренбург', 'OE', 1),
(2772, 176, 'Палана', 'PA', 1),
(2773, 176, 'Пенза', 'PE', 1),
(2774, 176, 'Пермь', 'PR', 1),
(2775, 176, 'Петропавловск-Камчатский', 'PK', 1),
(2776, 176, 'Петрозаводск', 'PT', 1),
(2777, 176, 'Псков', 'PS', 1),
(2778, 176, 'Ростов-на-Дону', 'RO', 1),
(2779, 176, 'Рязань', 'RY', 1),
(2780, 176, 'Салехард', 'SL', 1),
(2781, 176, 'Самара', 'SA', 1),
(2782, 176, 'Саранск', 'SR', 1),
(2783, 176, 'Саратов', 'SV', 1),
(2784, 176, 'Смоленск', 'SM', 1),
(2785, 176, 'Санкт-Петербург', 'SP', 1),
(2786, 176, 'Ставрополь', 'ST', 1),
(2787, 176, 'Сыктывкар', 'SY', 1),
(2788, 176, 'Тамбов', 'TA', 1),
(2789, 176, 'Томск', 'TO', 1),
(2790, 176, 'Тула', 'TU', 1),
(2791, 176, 'Тура', 'TR', 1),
(2792, 176, 'Тверь', 'TV', 1),
(2793, 176, 'Тюмень', 'TY', 1),
(2794, 176, 'Уфа', 'UF', 1),
(2795, 176, 'Ульяновск', 'UL', 1),
(2796, 176, 'Улан-Удэ', 'UU', 1),
(2797, 176, 'Усть-Ордынский', 'US', 1),
(2798, 176, 'Владикавказ', 'VL', 1),
(2799, 176, 'Владимир', 'VA', 1),
(2800, 176, 'Владивосток', 'VV', 1),
(2801, 176, 'Волгоград', 'VG', 1),
(2802, 176, 'Вологда', 'VD', 1),
(2803, 176, 'Воронеж', 'VO', 1),
(2804, 176, 'ВЯТКА', 'VY', 1),
(2805, 176, 'Якутск', 'YA', 1),
(2806, 176, 'Ярославль', 'YR', 1),
(2807, 176, 'Екатеринбург', 'YE', 1),
(2808, 176, 'Йошкар-Ола', 'YO', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A''ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa''asaleleaga', 'FA', 1),
(2856, 181, 'Gaga''emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa''itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va''a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '''Asir', 'AS', 1),
(2882, 184, 'Ha''il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand'' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand'' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3034, 199, 'A''ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa''iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa''iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'G&auml;vleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'J&auml;mtland', 'Z', 1),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, '&Ouml;rebro', 'T', 1),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'S&ouml;dermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'V&auml;rmland', 'S', 1),
(3091, 203, 'V&auml;sterbotten', 'AC', 1),
(3092, 203, 'V&auml;sternorrland', 'Y', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3093, 203, 'V&auml;stmanland', 'U', 1),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graub&uuml;nden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Z&uuml;rich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1),
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t''ou', 'NT', 1),
(3145, 206, 'P''eng-hu', 'PH', 1),
(3146, 206, 'P''ing-tung', 'PT', 1),
(3147, 206, 'T''ai-chung', 'TG', 1),
(3148, 206, 'T''ai-nan', 'TA', 1),
(3149, 206, 'T''ai-pei county', 'TP', 1),
(3150, 206, 'T''ai-tung', 'TT', 1),
(3151, 206, 'T''ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T''ai-chung', 'TH', 1),
(3157, 206, 'T''ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T''ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha''apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava''u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakır', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Черкассы', '71', 1),
(3481, 220, 'Чернигов', '74', 1),
(3482, 220, 'Черновцы', '77', 1),
(3483, 220, 'Крым', '43', 1),
(3484, 220, 'Днепропетровск', '12', 1),
(3485, 220, 'Донецк', '14', 1),
(3486, 220, 'Ивано-Франковск', '26', 1),
(3487, 220, 'Херсон', '65', 1),
(3488, 220, 'Хмельницкий', '68', 1),
(3489, 220, 'Кировоград', '35', 1),
(3490, 220, 'Киев', '30', 1),
(3491, 220, 'Киевская область', '32', 1),
(3492, 220, 'Луганск', '09', 1),
(3493, 220, 'Львов', '46', 1),
(3494, 220, 'Николаев', '48', 1),
(3495, 220, 'Одесса', '51', 1),
(3496, 220, 'Полтава', '53', 1),
(3497, 220, 'Ровно', '56', 1),
(3498, 220, 'Севастополь', '40', 1),
(3499, 220, 'Сумы', '59', 1),
(3500, 220, 'Тернополь', '61', 1),
(3501, 220, 'Винница', '05', 1),
(3502, 220, 'Волынь', '07', 1),
(3503, 220, 'Ужгород', '21', 1),
(3504, 220, 'Запорожье', '23', 1),
(3505, 220, 'Житомир', '18', 1),
(3506, 221, 'Abu Zaby', 'AZ', 1),
(3507, 221, '''Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubai', 'DU', 1),
(3511, 221, 'R''as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg''ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog''iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma''rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa''dah', 'SD', 1),
(3805, 235, 'San''a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta''izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3895, 105, 'Medio Campidano', 'VS', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3902, 105, 'Ogliastra', 'OG', 1),
(3903, 105, 'Olbia-Tempio', 'OT', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', 1),
(4038, 117, 'Aizkraukles novads', '0320200', 1),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', 1),
(4040, 117, 'Aizputes novads', '0640600', 1),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', 1),
(4042, 117, 'Aknīstes novads', '0560800', 1),
(4043, 117, 'Aloja, Alojas novads', '0661007', 1),
(4044, 117, 'Alojas novads', '0661000', 1),
(4045, 117, 'Alsungas novads', '0624200', 1),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', 1),
(4047, 117, 'Alūksnes novads', '0360200', 1),
(4048, 117, 'Amatas novads', '0424701', 1),
(4049, 117, 'Ape, Apes novads', '0360805', 1),
(4050, 117, 'Apes novads', '0360800', 1),
(4051, 117, 'Auce, Auces novads', '0460805', 1),
(4052, 117, 'Auces novads', '0460800', 1),
(4053, 117, 'Ādažu novads', '0804400', 1),
(4054, 117, 'Babītes novads', '0804900', 1),
(4055, 117, 'Baldone, Baldones novads', '0800605', 1),
(4056, 117, 'Baldones novads', '0800600', 1),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', 1),
(4058, 117, 'Baltinavas novads', '0384400', 1),
(4059, 117, 'Balvi, Balvu novads', '0380201', 1),
(4060, 117, 'Balvu novads', '0380200', 1),
(4061, 117, 'Bauska, Bauskas novads', '0400201', 1),
(4062, 117, 'Bauskas novads', '0400200', 1),
(4063, 117, 'Beverīnas novads', '0964700', 1),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', 1),
(4065, 117, 'Brocēnu novads', '0840601', 1),
(4066, 117, 'Burtnieku novads', '0967101', 1),
(4067, 117, 'Carnikavas novads', '0805200', 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', 1),
(4069, 117, 'Cesvaines novads', '0700800', 1),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', 1),
(4071, 117, 'Cēsu novads', '0420200', 1),
(4072, 117, 'Ciblas novads', '0684901', 1),
(4073, 117, 'Dagda, Dagdas novads', '0601009', 1),
(4074, 117, 'Dagdas novads', '0601000', 1),
(4075, 117, 'Daugavpils', '0050000', 1),
(4076, 117, 'Daugavpils novads', '0440200', 1),
(4077, 117, 'Dobele, Dobeles novads', '0460201', 1),
(4078, 117, 'Dobeles novads', '0460200', 1),
(4079, 117, 'Dundagas novads', '0885100', 1),
(4080, 117, 'Durbe, Durbes novads', '0640807', 1),
(4081, 117, 'Durbes novads', '0640801', 1),
(4082, 117, 'Engures novads', '0905100', 1),
(4083, 117, 'Ērgļu novads', '0705500', 1),
(4084, 117, 'Garkalnes novads', '0806000', 1),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', 1),
(4086, 117, 'Grobiņas novads', '0641000', 1),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', 1),
(4088, 117, 'Gulbenes novads', '0500200', 1),
(4089, 117, 'Iecavas novads', '0406400', 1),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', 1),
(4091, 117, 'Ikšķiles novads', '0740600', 1),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', 1),
(4093, 117, 'Ilūkstes novads', '0440801', 1),
(4094, 117, 'Inčukalna novads', '0801800', 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', 1),
(4096, 117, 'Jaunjelgavas novads', '0321000', 1),
(4097, 117, 'Jaunpiebalgas novads', '0425700', 1),
(4098, 117, 'Jaunpils novads', '0905700', 1),
(4099, 117, 'Jelgava', '0090000', 1),
(4100, 117, 'Jelgavas novads', '0540200', 1),
(4101, 117, 'Jēkabpils', '0110000', 1),
(4102, 117, 'Jēkabpils novads', '0560200', 1),
(4103, 117, 'Jūrmala', '0130000', 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', 1),
(4105, 117, 'Kandava, Kandavas novads', '0901211', 1),
(4106, 117, 'Kandavas novads', '0901201', 1),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', 1),
(4108, 117, 'Kārsavas novads', '0681000', 1),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', 1),
(4110, 117, 'Kokneses novads', '0326100', 1),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', 1),
(4112, 117, 'Krāslavas novads', '0600202', 1),
(4113, 117, 'Krimuldas novads', '0806900', 1),
(4114, 117, 'Krustpils novads', '0566900', 1),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', 1),
(4116, 117, 'Kuldīgas novads', '0620200', 1),
(4117, 117, 'Ķeguma novads', '0741001', 1),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', 1),
(4119, 117, 'Ķekavas novads', '0800800', 1),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', 1),
(4121, 117, 'Lielvārdes novads', '0741401', 1),
(4122, 117, 'Liepāja', '0170000', 1),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', 1),
(4124, 117, 'Limbažu novads', '0660200', 1),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', 1),
(4126, 117, 'Līgatnes novads', '0421200', 1),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', 1),
(4128, 117, 'Līvānu novads', '0761201', 1),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', 1),
(4130, 117, 'Lubānas novads', '0701400', 1),
(4131, 117, 'Ludza, Ludzas novads', '0680201', 1),
(4132, 117, 'Ludzas novads', '0680200', 1),
(4133, 117, 'Madona, Madonas novads', '0700201', 1),
(4134, 117, 'Madonas novads', '0700200', 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', 1),
(4136, 117, 'Mazsalacas novads', '0961000', 1),
(4137, 117, 'Mālpils novads', '0807400', 1),
(4138, 117, 'Mārupes novads', '0807600', 1),
(4139, 117, 'Mērsraga novads', '0887600', 1),
(4140, 117, 'Naukšēnu novads', '0967300', 1),
(4141, 117, 'Neretas novads', '0327100', 1),
(4142, 117, 'Nīcas novads', '0647900', 1),
(4143, 117, 'Ogre, Ogres novads', '0740201', 1),
(4144, 117, 'Ogres novads', '0740202', 1),
(4145, 117, 'Olaine, Olaines novads', '0801009', 1),
(4146, 117, 'Olaines novads', '0801000', 1),
(4147, 117, 'Ozolnieku novads', '0546701', 1),
(4148, 117, 'Pārgaujas novads', '0427500', 1),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', 1),
(4150, 117, 'Pāvilostas novads', '0641401', 1),
(4151, 117, 'Piltene, Ventspils novads', '0980213', 1),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', 1),
(4153, 117, 'Pļaviņu novads', '0321400', 1),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', 1),
(4155, 117, 'Preiļu novads', '0760202', 1),
(4156, 117, 'Priekule, Priekules novads', '0641615', 1),
(4157, 117, 'Priekules novads', '0641600', 1),
(4158, 117, 'Priekuļu novads', '0427300', 1),
(4159, 117, 'Raunas novads', '0427700', 1),
(4160, 117, 'Rēzekne', '0210000', 1),
(4161, 117, 'Rēzeknes novads', '0780200', 1),
(4162, 117, 'Riebiņu novads', '0766300', 1),
(4163, 117, 'Rīga', '0010000', 1),
(4164, 117, 'Rojas novads', '0888300', 1),
(4165, 117, 'Ropažu novads', '0808400', 1),
(4166, 117, 'Rucavas novads', '0648500', 1),
(4167, 117, 'Rugāju novads', '0387500', 1),
(4168, 117, 'Rundāles novads', '0407700', 1),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', 1),
(4170, 117, 'Rūjienas novads', '0961600', 1),
(4171, 117, 'Sabile, Talsu novads', '0880213', 1),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', 1),
(4173, 117, 'Salacgrīvas novads', '0661400', 1),
(4174, 117, 'Salas novads', '0568700', 1),
(4175, 117, 'Salaspils novads', '0801200', 1),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', 1),
(4177, 117, 'Saldus novads', '0840200', 1),
(4178, 117, 'Saldus, Saldus novads', '0840201', 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', 1),
(4180, 117, 'Saulkrastu novads', '0801400', 1),
(4181, 117, 'Seda, Strenču novads', '0941813', 1),
(4182, 117, 'Sējas novads', '0809200', 1),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', 1),
(4184, 117, 'Siguldas novads', '0801601', 1),
(4185, 117, 'Skrīveru novads', '0328200', 1),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', 1),
(4187, 117, 'Skrundas novads', '0621200', 1),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', 1),
(4189, 117, 'Smiltenes novads', '0941600', 1),
(4190, 117, 'Staicele, Alojas novads', '0661017', 1),
(4191, 117, 'Stende, Talsu novads', '0880215', 1),
(4192, 117, 'Stopiņu novads', '0809600', 1),
(4193, 117, 'Strenči, Strenču novads', '0941817', 1),
(4194, 117, 'Strenču novads', '0941800', 1),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', 1),
(4196, 117, 'Talsi, Talsu novads', '0880201', 1),
(4197, 117, 'Talsu novads', '0880200', 1),
(4198, 117, 'Tērvetes novads', '0468900', 1),
(4199, 117, 'Tukuma novads', '0900200', 1),
(4200, 117, 'Tukums, Tukuma novads', '0900201', 1),
(4201, 117, 'Vaiņodes novads', '0649300', 1),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', 1),
(4203, 117, 'Valka, Valkas novads', '0940201', 1),
(4204, 117, 'Valkas novads', '0940200', 1),
(4205, 117, 'Valmiera', '0250000', 1),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', 1),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', 1),
(4208, 117, 'Varakļānu novads', '0701800', 1),
(4209, 117, 'Vārkavas novads', '0769101', 1),
(4210, 117, 'Vecpiebalgas novads', '0429300', 1),
(4211, 117, 'Vecumnieku novads', '0409500', 1),
(4212, 117, 'Ventspils', '0270000', 1),
(4213, 117, 'Ventspils novads', '0980200', 1),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', 1),
(4215, 117, 'Viesītes novads', '0561800', 1),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', 1),
(4217, 117, 'Viļakas novads', '0381600', 1),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', 1),
(4219, 117, 'Viļānu novads', '0781800', 1),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', 1),
(4221, 117, 'Zilupes novads', '0681801', 1),
(4222, 43, 'Arica y Parinacota', 'AP', 1),
(4223, 43, 'Los Rios', 'LR', 1),
(4224, 220, 'Харьков', '63', 1),
(4225, 118, 'Beirut', 'LB-BR', 1),
(4226, 118, 'Bekaa', 'LB-BE', 1),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1),
(4228, 118, 'Nabatieh', 'LB-NB', 1),
(4229, 118, 'North', 'LB-NR', 1),
(4230, 118, 'South', 'LB-ST', 1),
(4231, 99, 'Telangana', 'TS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone_to_geo_zone`
--

CREATE TABLE IF NOT EXISTS `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`zone_to_geo_zone_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=113 ;

--
-- Dumping data for table `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(111, 109, 1717, 4, '2017-08-23 07:08:07', '0000-00-00 00:00:00'),
(112, 109, 0, 3, '2017-08-23 07:10:27', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
