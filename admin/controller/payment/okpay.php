<?php 
class ControllerPaymentOkpay extends Controller {
	private $error = array(); 

	public function index() {
		$this->load->language('payment/okpay');

		//$this->document->title = $this->language->get('heading_title');
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting('okpay', $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['entry_receiver'] = $this->language->get('entry_receiver');

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');

		$data['entry_order_status'] = $this->language->get('entry_order_status');	
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
 		if (isset($this->error['receiver'])) {
			$data['error_receiver'] = $this->error['receiver'];
		} else {
			$data['error_receiver'] = '';
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payment'),
			'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL')
		);

   		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/okpay', 'token=' . $this->session->data['token'], 'SSL')
		);
		
		$data['action'] = $this->url->link('payment/okpay', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		
		
		// Receiver Email, Phone Number or Wallet Designation ID
		if (isset($this->request->post['okpay_receiver'])) {
			$data['okpay_receiver'] = $this->request->post['okpay_receiver'];
		} else {
			$data['okpay_receiver'] = $this->config->get('okpay_receiver');
		}
		
		if (isset($this->request->post['okpay_order_status_id'])) {
			$data['okpay_order_status_id'] = $this->request->post['okpay_order_status_id'];
		} else {
			$data['okpay_order_status_id'] = $this->config->get('okpay_order_status_id'); 
		} 

		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		

		if (isset($this->request->post['okpay_geo_zone_id'])) {
			$data['okpay_geo_zone_id'] = $this->request->post['okpay_geo_zone_id'];
		} else {
			$data['okpay_geo_zone_id'] = $this->config->get('okpay_geo_zone_id'); 
		} 
		
		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		
		if (isset($this->request->post['okpay_status'])) {
			$data['okpay_status'] = $this->request->post['okpay_status'];
		} else {
			$data['okpay_status'] = $this->config->get('okpay_status');
		}

		
		if (isset($this->request->post['okpay_sort_order'])) {
			$data['okpay_sort_order'] = $this->request->post['okpay_sort_order'];
		} else {
			$data['okpay_sort_order'] = $this->config->get('okpay_sort_order');
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('payment/okpay.tpl', $data));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/okpay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->request->post['okpay_receiver']) {
			$this->error['receiver'] = $this->language->get('error_receiver');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}
?>