<?php
class ModelCatalogManufacturerCategory extends Model {
	public function addCategory($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_category SET parent_id = '" . (int)$data['parent_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

		$category_id = $this->db->getLastId();

		foreach ($data['category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_category_description SET manufacturer_category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}

	}

	public function editCategory($category_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "manufacturer_category SET parent_id = '" . (int)$data['parent_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE manufacturer_category_id = '" . (int)$category_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_category_description WHERE manufacturer_category_id = '" . (int)$category_id . "'");
		foreach ($data['category_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_category_description SET manufacturer_category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}

	}

	public function deleteCategory($category_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_category WHERE manufacturer_category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_category_description WHERE manufacturer_category_id = '" . (int)$category_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_to_category WHERE manufacturer_category_id = '" . (int)$category_id . "'");

		$query = $this->db->query("SELECT manufacturer_category_id FROM " . DB_PREFIX . "manufacturer_category WHERE parent_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$this->deleteCategory($result['manufacturer_category_id']);
		}

	}

	public function getCategory($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "manufacturer_category WHERE manufacturer_category_id = '" . (int)$category_id . "'");

		return $query->row;
	}

	public function getCategories($parent_id = 0) {
		$category_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer_category c LEFT JOIN " . DB_PREFIX . "manufacturer_category_description cd ON (c.manufacturer_category_id = cd.manufacturer_category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");
		foreach ($query->rows as $result) {
			$category_data[] = array(
				'manufacturer_category_id' => $result['manufacturer_category_id'],
				'name'        => $this->getPath($result['manufacturer_category_id'], $this->config->get('config_language_id')),
				'status'  	  => $result['status'],
				'sort_order'  => $result['sort_order']
			);

			$category_data = array_merge($category_data, $this->getCategories($result['manufacturer_category_id']));
		}

		return $category_data;
	}

	public function getPath($category_id) {
		$query = $this->db->query("SELECT name, parent_id FROM " . DB_PREFIX . "manufacturer_category c LEFT JOIN " . DB_PREFIX . "manufacturer_category_description cd ON (c.manufacturer_category_id = cd.manufacturer_category_id) WHERE c.manufacturer_category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.sort_order, cd.name ASC");

		if ($query->row['parent_id']) {
			return $this->getPath($query->row['parent_id'], $this->config->get('config_language_id')) . $this->language->get('text_separator') . $query->row['name'];
		} else {
			return $query->row['name'];
		}
	}

	public function getCategoryDescriptions($category_id) {
		$category_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer_category_description WHERE manufacturer_category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$category_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
			);
		}

		return $category_description_data;
	}

	public function getTotalCategories() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "manufacturer_category");

		return $query->row['total'];
	}

	public function uninstall() {
		$this->db->query("DROP TABLE `" . DB_PREFIX . "manufacturer_category`");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "manufacturer_category_description`");
		$this->db->query("DROP TABLE `" . DB_PREFIX . "manufacturer_to_category`");
	}

	public function install() {
		$query = $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "manufacturer_category` (
									`manufacturer_category_id` int(11) NOT NULL AUTO_INCREMENT,
									`parent_id` int(11) NOT NULL DEFAULT '0',
									`sort_order` int(11) NOT NULL DEFAULT '0',
									`status` int(11) NOT NULL DEFAULT '1',
									PRIMARY KEY (`manufacturer_category_id`)
									)");

		$query = $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "manufacturer_category_description` (
									`manufacturer_category_id` int(11) NOT NULL,
									`language_id` int(11) NOT NULL,
									`name` varchar(255) NOT NULL
		)");

		$query = $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "manufacturer_to_category` (
			`manufacturer_category_id` int(11) NOT NULL,
			`manufacturer_id` int(11) NOT NULL
		)");
	}

}
?>