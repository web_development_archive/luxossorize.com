<?php
// Heading
$_['heading_title']          = 'Manufacturer\'s Categories';

// Text
$_['text_success']           = 'List of manufacturer\'s categories was updated!';

// Column
$_['column_name']            = 'Category name';
$_['column_seo_keyword']     = 'SEO Keyword';
$_['column_sort_order']      = 'Sort order';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Category name:';
$_['entry_parent']         	 = 'Parent category:';
$_['entry_keyword']          = 'SEO Keyword:<br/><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_sort_order']       = 'Sort order:';
$_['entry_status']           = 'Status:';

$_['text_help']				= '<p>If you have any questions or suggestions for improving the operation of the module, you can contact me:</p>
								<p>E-mail: <a href="mailto:mods@nikita-sp.com.ua">mods@nikita-sp.com.ua</a><br>
								Website: <a href="http://nikita-sp.com.ua/" target="_blank">nikita-sp.com.ua</a></p>


								<p>Support module, and donate a coin. Thanks.</p>
								<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
								<input type="hidden" name="cmd" value="_s-xclick">
								<input type="hidden" name="hosted_button_id" value="UGLKSM4UVN9S2">
								<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
								<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
								</form>
								<br><br>
								WMZ: Z320581374524 (USD)<br/>
								WMR: R227380004010 (RUB)<br/>
								WMU: U237282944947 (UAH)<br/>';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Category Name must be between 2 and 32 characters!';
?>