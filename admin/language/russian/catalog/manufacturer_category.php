<?php
// Heading
$_['heading_title']          = 'Категории производителей';

// Text
$_['text_success']           = 'Список категорий производителей обновлен!';

// Column
$_['column_name']            = 'Название категории';
$_['column_seo_keyword']     = 'SEO URL';
$_['column_sort_order']      = 'Порядок сортировки';
$_['column_action']          = 'Действие';

// Entry
$_['entry_name']             = 'Название категории:';
$_['entry_parent']         	 = 'Родительская категория:';
$_['entry_keyword']          = 'SEO URL:<br/><span class="help">Должно быть уникальным на всю систему.</span>';
$_['entry_sort_order']       = 'Порядок сортировки:';
$_['entry_status']           = 'Статус:';

$_['text_help']				= '<p>Если у вас возникли вопросы или предложения по улучшению работы модуля, вы можете связаться со мной:</p>
								<p>E-mail: <a href="mailto:mods@nikita-sp.com.ua">mods@nikita-sp.com.ua</a><br>
								Сайт: <a href="http://nikita-sp.com.ua/" target="_blank">nikita-sp.com.ua</a></p>


								<p>Поддержите модуль, и пожертвуйте копеечку. Спасибо.</p>
								<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
								<input type="hidden" name="cmd" value="_s-xclick">
								<input type="hidden" name="hosted_button_id" value="UGLKSM4UVN9S2">
								<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
								<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
								</form>
								<br><br>
								WMZ: Z320581374524 (USD)<br/>
								WMR: R227380004010 (RUB)<br/>
								WMU: U237282944947 (UAH)<br/>';

// Error
$_['error_warning']          = 'Внимательно проверьте форму на ошибки!';
$_['error_permission']       = 'У Вас нет прав для изменения категорий производителей!';
$_['error_name']             = 'Название категории должно быть от 2 до 32 символов!';
?>