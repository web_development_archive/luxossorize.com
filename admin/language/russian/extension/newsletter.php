<?php
// Heading 
$_['heading_title']     = 'Подписчики';

// Text
$_['column_email']		= 'Почта';

$_['text_empty']		= 'Нет подписчиков.';
$_['text_success']		= 'Вы успешно удалили подписчиков!';

$_['error_warning']		= 'У вас нет разрешения на изменение подписчиков!';