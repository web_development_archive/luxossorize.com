<?php

// Heading Goes here:
$_['heading_title']    = '<b>TG KINGSTORE LITE Настройки</b>';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Успех: Вы изменили модуль TG KingStore Lite Settings!';
$_['text_warning']     = 'Предупреждение: Изменения не сохранены! Проверьте заполненость всех полей.';
$_['text_warning2']    = "Предупреждение: Skin не удален. Возможная причина:<br>- Skin не существует<br> - Skin используется";

// Error
$_['error_permission'] = 'Предупреждение: У вас нету доступа изменять модуль TG KingStore Lite Settings!';

?>
