<?php

// Heading Goes here:
$_['heading_title']    = '<b>TG KingStore Lite Filter Product</b>';

// Text
$_['text_success']     = 'Успех: Вы изменили модуль TG KingStore Lite Filter Product!';

// Error
$_['error_permission'] = 'Предупреждение: У вас нету доступа изменять модуль TG KingStore Lite Filter Product!';

?>
