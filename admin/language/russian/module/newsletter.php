<?php
// Heading
$_['heading_title']    			= '<b>TG KingStore Pro Подписки <a href="http://themeglobal.com/" style="color:#00badc;"> (Купите Pro версию для пользования)</a></b>';

// Text
$_['text_module']      			= 'Модули';
$_['text_success']     			= 'Успех: Вы изменили модуль TG KingStore Pro Подписки!';
$_['text_edit']					= 'Изменить модуль TG KingStore Pro Подписка';

// Entry
$_['entry_status']   			= 'Статус'; 

// Error
$_['error_permission'] 			= 'Предупреждение: У вас нету доступа изменять модуль TG KingStore Pro Подписка!';