<?php

// Heading Goes here:
$_['heading_title']    = '<b>TG KingStore Pro MegaMenu<a href="http://themeglobal.com/" style="color:#00badc;"> (Купите Pro версию для пользования)</a></b>';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Успех: Вы изменили модуль TG KingStore Pro MegaMenu!';
$_['text_warning']     = 'Предупреждение: Изменения не сохранены! Проверьте заполненость всех полей.';

// Error
$_['error_permission'] = 'Предупреждение: У вас нету доступа изменять модуль TG KingStore Pro MegaMenu!';

?>
