<?php
// Heading
$_['heading_title']             = 'OKPAY';

// Text 
$_['text_payment']              = 'Оплата';
$_['text_success']              = 'Настройки модуля обновлены!';
$_['text_okpay']                = '<a onclick="window.open(\'https://www.okpay.com\');"><img src="../image/payment/okpay.gif" alt="OKPAY.com" title="OKPAY.com" style="border: 1px solid #EEEEEE;" height="25px" /></a>';

// Entry
$_['entry_receiver']            = 'Получатель (Эл.почта/Телефон/ID кошелька)';

$_['entry_order_status'] 	= 'Статус заказа';
$_['entry_geo_zone']     	= 'Гео. зона';
$_['entry_status']       	= 'Состояние';
$_['entry_sort_order']   	= 'Сортировка';

// Error
$_['error_permission']   	= 'Ошибка: У вас нет прав для изменения настроек модуля OKPAY!';
$_['error_receiver']        	= 'Неверные данные получателя в системе OKPAY!';
 
?>